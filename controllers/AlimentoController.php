<?php

namespace app\controllers;

use Yii;
use app\models\Alimento;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AlimentoController implements the CRUD actions for Alimento model.
 */
class AlimentoController extends Controller
{
    /**
     * @inheritdoc
     */
	public $layout = 'leftmenu';
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Alimento models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Alimento::find(),
        ]);

        return $this->render('@app/views/admin/alimento/index', [
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Creates a new Alimento model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Alimento();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			$dataProvider = new ActiveDataProvider(['query' => Alimento::find(),]);
			        return $this->render('@app/views/admin/alimento/index', ['dataProvider' => $dataProvider,]);
        } else {
            return $this->render('@app/views/admin/alimento/create', ['model' => $model,]);
        }
    }

    /**
     * Updates an existing Alimento model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $dataProvider = new ActiveDataProvider(['query' => Alimento::find(),]);
			 return $this->render('@app/views/admin/alimento/index', ['dataProvider' => $dataProvider,]);
        } else {
            return $this->render('@app/views/admin/alimento/update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Alimento model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['@app/views/admin/alimento/index']);
    }

    /**
     * Finds the Alimento model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Alimento the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Alimento::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
