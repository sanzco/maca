<?php

namespace app\controllers;

use Yii;
use app\models\Usuario;
use app\models\Persona;
use app\models\Perfil;
use app\models\UsuarioQuery;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UsuarioController implements the CRUD actions for Usuario model.
 */
class UsuarioController extends Controller
{
    /**
     * @inheritdoc
     */
	public $layout = 'leftmenu';
	public $tipoPersonaMedico = 2;
	
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Usuario models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsuarioQuery();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('@app/views/admin/users/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Usuario model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('@app/views/admin/users/view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Usuario model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Usuario();
        $modelPersona = new Persona();
        $checkUsuario = false;
        if($modelPersona->load(Yii::$app->request->post())){
        	$persona = Persona::find()->where(['identificacion' => $modelPersona->identificacion])->one();
        	if(!isset($persona)){
        		$modelPersona->tipo_persona_id = $this->tipoPersonaMedico;
        		$modelPersona->save();
        	}else{
        		$modelPersona = $persona;
        		$checkUsuario = true;
        	}
        }else{
        	 $modelPersona = new Persona();
        }
       	if ($model->load(Yii::$app->request->post())&&$model->validate()) {
       		$model->persona_id = $modelPersona->id;
       		$user = Usuario::findOne($model->persona_id);
       		if(!isset($user)){
       			$user = Usuario::find()->where(['username' => $model->username])->one();
       			if(!isset($user)){
	       			$model->save();
		       		foreach($model->tempperfiles as $perfil){
		        		$perfilTemp = Perfil::findOne($perfil);
		        		$model->link('perfiles',$perfilTemp);
		        	}
	       			$searchModel = new UsuarioQuery();
			        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
			        return $this->render('@app/views/admin/users/index', [
			            'searchModel' => $searchModel,
			            'dataProvider' => $dataProvider,
			        ]);
       			}else{
       				$model->addError('username','Ya existe el nombre de usuario escogido');
	       			 return $this->render('@app/views/admin/users/create', [
		                'model' => $model,
	       			 	'modelPersona'=> $modelPersona
		            ]);
       			}
			        
       		}else{
       			$model->addError('username','Esta persona ya cuenta con un usuario');
       			$modelPersona->addError('apellido','Esta persona ya cuenta con un usuario');
       			 return $this->render('@app/views/admin/users/create', [
	                'model' => $model,
       			 	'modelPersona'=> $modelPersona
	            ]);
       		}
       		
		} else {
	            return $this->render('@app/views/admin/users/create', [
	                'model' => $model,
	            	'modelPersona'=> new Persona(),
	            ]);
	        }
        
    }

    /**
     * Updates an existing Usuario model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
        	$model->unlinkAll('perfiles', true);
        	foreach($model->tempperfiles as $perfil){
        		$perfilTemp = Perfil::findOne($perfil);
        		$model->link('perfiles',$perfilTemp);
        	}
            $searchModel = new UsuarioQuery();
			        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
			        return $this->render('@app/views/admin/users/index', [
			            'searchModel' => $searchModel,
			            'dataProvider' => $dataProvider,
			        ]);
        } else {
        	
            return $this->render('@app/views/admin/users/update', [
                'model' => $model,
            	'modelPersona'=>$model->persona,
            ]);
        }
    }

    /**
     * Deletes an existing Usuario model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        $searchModel = new UsuarioQuery();
			        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
			        return $this->render('@app/views/admin/users/index', [
			            'searchModel' => $searchModel,
			            'dataProvider' => $dataProvider,
			        ]);
    }

    /**
     * Finds the Usuario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Usuario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Usuario::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    
}
