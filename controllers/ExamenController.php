<?php

namespace app\controllers;

use Yii;
use app\models\ExamenReferencia;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ExamenController implements the CRUD actions for ExamenReferencia model.
 */
class ExamenController extends Controller
{
	public $layout = 'leftmenu';
	
	/**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ExamenReferencia models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ExamenReferencia::find(),
        ]);

        return $this->render('@app/views/admin/examen/index', [
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Creates a new ExamenReferencia model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ExamenReferencia();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $dataProvider = new ActiveDataProvider(['query' => ExamenReferencia::find(),]);
	        return $this->render('@app/views/admin/examen/index', ['dataProvider' => $dataProvider,]);
        } else {
            return $this->render('@app/views/admin/examen/create', ['model' => $model,]);
        }
    }

    /**
     * Updates an existing ExamenReferencia model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $dataProvider = new ActiveDataProvider(['query' => ExamenReferencia::find(),]);
	        return $this->render('@app/views/admin/examen/index', ['dataProvider' => $dataProvider,]);
        } else {
            return $this->render('@app/views/admin/examen/update', [
                'model' => $model,
            ]);
        }
    }

    

    /**
     * Finds the ExamenReferencia model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ExamenReferencia the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ExamenReferencia::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
