<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\base\Model;

use app\models\PersonaForm;
use app\models\Persona;
use app\models\TipoAntecedente;
use app\models\Antecedentes;
use app\models\Antropometria;
use yii\helpers\ArrayHelper;
use app\models\Anamnesis;
use app\models\Recordatorio;
use app\models\Farmacos;
use app\models\Habitos;
use app\models\Cita;
use app\models\InterpretacionCircunferencia;
use app\models\Examen;
use app\models\ExamenReferencia;
use app\models\RecordatorioPreparacion;
use app\models\EstadoPsicosocial;
use app\models\ConductaCita;
use app\models\Menu;
use app\models\IngestaAnamnesis;
use yii\helpers\Html;
use kartik\mpdf\Pdf;
use mPDF;

class CitaController extends Controller
{
	public $enableCsrfValidation = false;
	
	public function actionIndex(){
		$model = new PersonaForm();
		$modelPersona = new Persona();
		$accion = 'NUEVO_PACIENTE';
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
        	$modelPersona = Persona::findPersonaById($model->idType,$model->id);
        	if($modelPersona === null){
        		$modelPersona = new Persona();
        		$modelPersona->identificacion = $model->id;
        		$modelPersona->tipo_identificacion_id = $model->idType;
        	}
        }
        
		return $this->render('cita',['modelPersona' =>$modelPersona,'accion'=>$accion]);
	}
    
	public function actionNewpatient(){
		Yii::trace('http://localhost'.Yii::$app->view->theme->baseUrl.'/css/materialize.css');
		$idPost = Yii::$app->request->post('Persona');
		$id = $idPost['id']; 
		$model = Persona::findOne($id);
		$modelCita = null;
		if($model===null){
			$model = new Persona();
		}
		if (Yii::$app->request->post('history-button')==='history') {
	    	return $this->render('listacitas',['modelPersona' =>$model]);
		}else{
			$savePersona = false;
			$ultimacita = Cita::lastDate($model->identificacion);
			Yii::trace($ultimacita);
			Yii::trace(Yii::$app->user->id);
			if(isset($ultimacita)&& !empty($ultimacita)){
				if(Yii::$app->user->id!=$ultimacita['persona_medico']){
					$accion = 'NUEVO_PACIENTE';
					$model->addError('*', 'El paciente pertenece a otro medico');
					return $this->render('cita',['modelPersona' =>$model,'accion'=>$accion]);
				}
			}
				try{
					if ($model->load(Yii::$app->request->post()) && $model->validate()) {
							$model->save();
							$savePersona = true;
					}else{
							$model->update();
							$savePersona = true;
					}
					if($savePersona){
						$modelCita = new Cita;
						$modelCita->persona_id = $model->id;
						$modelCita->persona_medico = Yii::$app->user->id;
						$modelCita->save();
					}
				}catch (ErrorException $e) {
				    Yii::warning("No se pudo gurdar en db");
				}
			return $this->render('motivos',
								[
									'modelCita'=>$modelCita,
								]);	
				
		}
	}

		public function actionIniciar(){
			$idCita = Yii::$app->request->post('Cita');
			$id = $idCita['cita_id']; 
			Yii::trace($idCita);
			$modelCita = Cita::findOne($id);	
			if ($modelCita->load(Yii::$app->request->post()) && $modelCita->validate()) {
				$modelCita->save();
			}
			$antecedentes = Yii::$app->request->post('Antecedente');
			foreach($antecedentes as $key =>$antecedente){
				if($antecedente!=null && !empty($antecedente)){ 
					$modelAntecedente = Antecedentes::find()->where(['persona_id'=>$modelCita->persona->id,'tipo_antecedente_id'=>$key])->one();
					if($modelAntecedente === null){
						$modelAntecedente = new Antecedentes;
					}
					$modelAntecedente->persona_id = $modelCita->persona->id;
					$modelAntecedente->tipo_antecedente_id = $key;
					$modelAntecedente->descripcion = $antecedente;
					$modelAntecedente->save();
				}
			}
			$ultimacita = Cita::lastDateById($modelCita->persona->id,$modelCita->cita_id);
			//Yii::trace($ultimacita);
			if(isset($ultimacita) && !empty($ultimacita)){
				$modelCita = Cita::findOne($ultimacita['cita_id']);
			}
			if(empty($modelCita->antropometria)){$modelCita->link('antropometria',new Antropometria); }
			if(empty($modelCita->anamnesis)){$modelCita->link('anamnesis' , new Anamnesis); }
			if(empty($modelCita->recordatorios)){ $recordatorio = new Recordatorio;   $modelCita->link('recordatorios' ,$recordatorio ); }
			if(empty($modelCita->farmacos)){$modelCita->link('farmacos' , new Farmacos); }
			
			//if(empty($modelCita->interpretacionCircunferencias)){$modelCita->link('interpretacionCircunferencias', new InterpretacionCircunferencia); }
			if(empty($modelCita->examenes)){$modelCita->link('examenes', new Examen);}
			if(empty($modelCita->habitos)){$modelCita->link('habitos',new Habitos);}
			if(empty($modelCita->estadoPsicosocial)){$modelCita->link('estadoPsicosocial', new EstadoPsicosocial); }
			
			return $this->render('editcita',[
				'modelCita'=>$modelCita, 
				'citaId' =>$id,
			]);
			
			/*
			
			$modelAntropo = new Antropometria;
			$modelAnamnesis = new Anamnesis;
			$modelRecordatorio = [new Recordatorio];
			$modelRecordatorioPreparacion = [[new  RecordatorioPreparacion]];
			$modelFarmacos =  [new Farmacos];
			$modelHabitos = new Habitos;
			$modelInterpretacion = new InterpretacionCircunferencia;
			$modelExamen =[new Examen];
			$modelEstadoPsicosocial = new EstadoPsicosocial;
			$accion = 'INICIAR_CITA';
			return $this->render('cita',['modelPersona' =>$modelCita->persona,'accion'=>$accion,
								'modelCita'=>$modelCita,
								'modelAntropo'=>$modelAntropo,
								'modelAnamnesis'=>$modelAnamnesis,
								'modelsRecordatorio' => (empty($modelRecordatorio)) ? [new Recordatorio] : $modelRecordatorio,
								'modelRecordatorioPreparacion' => (empty($modelRecordatorioPreparacion)) ? [[new RecordatorioPreparacion]] : $modelRecordatorioPreparacion,
								'modelsFarmacos' =>(empty($modelFarmacos)) ? [new Farmacos] : $modelFarmacos,
								'modelHabitos' => $modelHabitos,
								'modelInterpretacion'=>$modelInterpretacion,
								'modelsExamen' =>(empty($modelExamen)) ? [new Examen] : $modelExamen,
								'modelPsicosocial'=>$modelEstadoPsicosocial,
							]);
				*/
			}
			
	
	public function actionDiagnostico(){
		$newCita = Yii::$app->request->post('citaId');
		$oldCita = Yii::$app->request->post('Cita');
		$update = Yii::$app->request->post('update');
		$modelCita = Cita::findOne($newCita);
		if(isset($oldCita)){
			$modelCita->examen_fisico = $oldCita['examen_fisico'];
			$modelCita->diagnostico = $oldCita['diagnostico'];
		}else{
			$modelCita->examen_fisico = '';
			$modelCita->diagnostico = '';
		}
		
		$modelCita->save();
		
		/*$modelAntropoPost = Yii::$app->request->post('Antropometria');
		if(!empty($modelAntropoPost['peso']) && !empty($modelAntropoPost['talla'])){
			$modelCita->antropometria->attributes = $modelAntropoPost;
			$modelCita->antropometria->save();
		}*/
		
		$persona = Yii::$app->request->post('Persona');
		$modelPersona = Persona::findOne($persona['id']);
		//Yii::trace($modelPersona);
		
		$this->updateCita($modelCita, Yii::$app->request->post());
		$this->saveCita($modelCita, Yii::$app->request->post());
		
		
		return $this->render('review',[
			'modelCita'=>$modelCita,
			
		]);
	}
	
	public function actionReview(){
		$oldCita = Yii::$app->request->post('id');
		//Yii::trace('Cita'.$oldCita);
		$modelCita = Cita::findOne($oldCita);
		return $this->render('review',[
			'modelCita'=>$modelCita,
		]);
	}
	
	public function actionUpdatevalor($id){
		$examen  = ExamenReferencia::findOne($id);
         echo Html::tag('span',$examen->valor_referencia_superior.' - '.$examen->valor_referencia_bajo);
	}
	
	public function actionEditcita(){
		$citaEdit = Yii::$app->request->post('Cita');
		$cita = $citaEdit['cita_id'];
		if(empty($cita))
			$cita = Yii::$app->request->get('id');
		$modelCita = Cita::findOne($cita);
		$origen = Yii::$app->request->get('print'); 
		if(isset($origen) && $origen =='1'){
			$pdf = $this->printReview($modelCita);
			return $pdf->render();
		}else{
			
			if(empty($modelCita->examenes)){$modelCita->link('examenes', new Examen);}
			if(empty($modelCita->habitos)){$modelCita->link('habitos',new Habitos);}
		    if(empty($modelCita->antropometria)){$modelCita->link('antropometria',new Antropometria);}
			if(empty($modelCita->estadoPsicosocial)){$modelCita->link('estadoPsicosocial', new EstadoPsicosocial); }
			
			return $this->render('editcita',[
				'modelCita'=>$modelCita,
				'citaId'=>$cita,
				'update'=>true,
			]);
		}
	}
	
	public function actionUpdate(){
		$oldCita = Yii::$app->request->post('Cita');
		$modelCita = Cita::findOne($oldCita['cita_id']);
		$modelAntropoPost = Yii::$app->request->post('Antropometria');
		$modelCita->load(Yii::$app->request->post());
		$modelCita->antropometria->attributes = $modelAntropoPost;
		//Yii::trace($modelCita->antropometria);
		$modelCita->save();
		$modelCita->antropometria->save();
		return $this->render('review',[
			'modelCita'=>$modelCita,
		]);
	}
	
	public function actionRecomendaciones(){
		$this->layout = 'reports';
		$provider = Cita::datesWithoutRecommendations();
		return $this->render('recomendaciones',[
			'dataProvider'=>$provider,
		]);
	}
	public function actionRecomendar(){
		$id = Yii::$app->request->get('id');
		if(!isset($id) || empty($id)){
			$id = Yii::$app->request->post('id');
		}
		$modelCita = Cita::findOne($id);
		return $this->render('recomendar',[
			'modelCita'=>$modelCita,
		]);
	}
	
	public function actionSavereco(){
		$origen = Yii::$app->request->post('save-button');
		$print = Yii::$app->request->post('print-button');
		if(isset($origen)|| isset($print)){
			$post = Yii::$app->request->post('Cita');
			$modelCita = Cita::findOne($post['cita_id']);
			$modelCita->load(Yii::$app->request->post());
			$modelCita->save();
		}
		$refresh = Yii::$app->request->post('refresh-button');
		if(isset($refresh)){
			$post = Yii::$app->request->post('Cita');
			$modelCita = Cita::findOne($post['cita_id']);
			$modelCita->f_actividad = $post['f_actividad'];
			return $this->render('recomendar',[
			'modelCita'=>$modelCita,
		]);
		}
		$print = Yii::$app->request->post('print-button');
		if(isset($print)){
			$pdf = $this->printForm($modelCita);
			return $pdf->render();
		}
		$this->layout = 'reports';
		$provider = Cita::datesWithoutRecommendations();
		return $this->render('recomendaciones',[
			'dataProvider'=>$provider,
		]);
	}
	
	
	private function saveCita($modelCita, $post){
		
		$modelAntropo = new Antropometria;
		$modelAntropo->load($post);
		$modelAntropo->cita_id = $modelCita->cita_id;
		if(!empty($modelAntropo->peso) && !empty($modelAntropo->talla)){
			$modelAntropo->save();
		}
		
		//Yii::trace($modelAntropo);
		
		//Circunferencias
		$circunferencias = Yii::$app->request->post('InterpretacionCircunferencia');
		if(isset($circunferencias) && !empty($circunferencias)){
			foreach($circunferencias as $key => $circunferencia){
				$modelInterpretacion = new InterpretacionCircunferencia;
				$modelInterpretacion->cita_id = $modelCita->cita_id;
				$modelInterpretacion->circunferencia_id = $key;
				$modelInterpretacion->valor = $circunferencia['valor'];
				$modelInterpretacion->interpretacion = $circunferencia['interpretacion'];
				if(!empty($modelInterpretacion->valor)){
					$modelInterpretacion->save();
				}
			}
		}
		
		//Examenes
		$examenes = [new Examen];
		$examenes = Yii::$app->request->post('Examen');
		if(isset($examenes) && !empty($examenes)){
			foreach($examenes as $examen){
				//Yii::trace($examen);
				if(isset($examen['valor']) && !empty($examen['valor'])){
					$examenT = new Examen;
					$examenT->cita_id = $modelCita->cita_id;
					$examenT->examen_id =  $examen['examen_id'];
					$existe = $examenT->duplicateResult();
					//Yii::trace($existe);
					if(!isset($existe) || empty($existe)){
						$examenT->valor = $examen['valor'];
						$examenT->interpretacion = $examen['interpretacion'];
						$examenT->fecha = $examen['fecha'];
						if(!$examenT->save()){
							Yii::error($examenT->errors);
						}
					}
				}
			}
		}
		
		//Recordatorio
		$modelsRecordatorio = [new Recordatorio];
        $modelsRecordatorio = Yii::$app->request->post('Recordatorio');
        $modelsRecordatorioPreparacion = [new RecordatorioPreparacion];
        $modelsRecordatorioPreparacion = Yii::$app->request->post('RecordatorioPreparacion');
        
        if(isset($modelsRecordatorio) && !empty($modelsRecordatorio)){
        	foreach($modelsRecordatorio as $keyR => $recordatorio){
        		$modelRecordatorio = new Recordatorio();
        		$modelRecordatorio->attributes =$recordatorio;
        		$modelRecordatorio->cita_id = $modelCita->cita_id;
        		if(!empty($modelRecordatorio->preparacion)){
	        		if(!$modelRecordatorio->save()){
	        			Yii::error($modelRecordatorio->errors);
	        		}
	        		foreach($modelsRecordatorioPreparacion[$keyR] as $keyPreparacion => $preparacion){
	        			$prepare = new RecordatorioPreparacion();
	        			$prepare->attributes = $preparacion;
	        			$prepare->recordatorio_id = $modelRecordatorio->id;
	        			if(!$prepare->save()){
	        				Yii::error($prepare->errors);
	        			}
	        		}
        		}
        	}
        }
		
        $modelMenus = [new Menu];
        $modelMenus = Yii::$app->request->post('Menu');
        
		if(isset($modelMenus) && !empty($modelMenus)){
			foreach($modelMenus as $key => $menu){
				$modelMenu = new Menu;
				$modelMenu->attributes = $menu;
				$modelMenu->alimento_id = $key;
				$modelMenu->cita_id = $modelCita->cita_id;
				if(!empty($modelMenu->valor)){
					$modelMenu->save();
				}
			}
		}
        
		//anamnesis
		$modelAnamnesis = new Anamnesis;
		$modelAnamnesis->load($post);
		$modelAnamnesis->cita_id = $modelCita->cita_id;
		$modelAnamnesis->save();
		
		$modelsIngesta = Yii::$app->request->post('IngestaAnamnesis');
		Yii::trace($modelsIngesta);
		if(isset($modelsIngesta) && !empty($modelsIngesta)){
			foreach($modelsIngesta as $key => $ingesta){
				$modelIngesta = new IngestaAnamnesis;
				$modelIngesta->attributes = $ingesta;
				$modelIngesta->anamnesis_id = $modelAnamnesis->anamnesis_id;
				$modelIngesta->tipo_ingesta_id = $key;
				if(!empty($modelIngesta->texto_descriptivo)){
					$modelIngesta->save();
				}
			}	
		}else{
			Yii::trace("no guarda ingesta");
		}
		
		//Farmacos
		$modelsFarmacos = [new Farmacos];
		$modelsFarmacos = Yii::$app->request->post('Farmacos');
		if(isset($modelsFarmacos) && !empty($modelsFarmacos)){
			foreach($modelsFarmacos as $farmaco){	
				$modelFarmaco = new Farmacos();
				$modelFarmaco->attributes = $farmaco;
				$modelFarmaco->cita_id = $modelCita->cita_id;
				if(!empty($modelFarmaco->nombre)){
					$modelFarmaco->save();
				}
			}
		}
		
		//Habitos
		$modelHabitos = new Habitos;
		$modelHabitos->load($post);
		$modelHabitos->cita_id = $modelCita->cita_id;
		$modelHabitos->save();
		//Yii::trace($modelHabitos);
		
		//PsicoSocial
		$modelPsicosocial = new EstadoPsicosocial;
		$modelPsicosocial->load($post);
		$modelPsicosocial->cita_id = $modelCita->cita_id;
		$modelPsicosocial->save();
		//Yii::trace($modelHabitos);
		
		//Conductas Compensatorias
		$modelConductas = [new ConductaCita];
		$modelConductas = Yii::$app->request->post('ConductaCita');
		if(isset($modelConductas) && !empty($modelConductas)){
			foreach($modelConductas as $keyConducta => $conducta){
				$modelConducta = new ConductaCita;
				$modelConducta->attributes = $conducta;
				$modelConducta->conducta_id = $keyConducta;
				$modelConducta->cita_id = $modelCita->cita_id;
				$modelConducta->save();
			}
		}
		
	}

	private function updateCita($modelCita, $post){
		Antropometria::deleteAll('cita_id ='.$modelCita->cita_id);
		InterpretacionCircunferencia::deleteAll('cita_id ='.$modelCita->cita_id);
		Examen::deleteAll('cita_id ='.$modelCita->cita_id);
		
		
		//Recordatorio
		$records = Recordatorio::find()->where(['cita_id'=>$modelCita->cita_id])->all();
		Yii::trace($records);
		foreach($records as $k => $record){
			RecordatorioPreparacion::deleteAll('recordatorio_id ='.$record->id);
		}
		Recordatorio::deleteAll('cita_id ='.$modelCita->cita_id);
		
		
		Menu::deleteAll('cita_id ='.$modelCita->cita_id);
        
		$records = Anamnesis::find()->where(['cita_id'=>$modelCita->cita_id])->all();
		foreach($records as  $j => $record){
			IngestaAnamnesis::deleteAll('anamnesis_id ='.$record->anamnesis_id);
		}
		Anamnesis::deleteAll('cita_id ='.$modelCita->cita_id);
		
		Farmacos::deleteAll('cita_id ='.$modelCita->cita_id);
		Habitos::deleteAll('cita_id ='.$modelCita->cita_id);
		EstadoPsicosocial::deleteAll('cita_id ='.$modelCita->cita_id);
		ConductaCita::deleteAll('cita_id ='.$modelCita->cita_id);
		
	}
	private function printForm($modelCita){
		
		$mpdf = new mPDF();
		//$stylesheet = file_get_contents('http://localhost'.Yii::$app->view->theme->baseUrl.'/css/materialize.css');
		$stylesheet = file_get_contents(Yii::$app->params['domain'].Yii::$app->view->theme->baseUrl.'/css/materialize.css');
		$mpdf->WriteHTML($stylesheet, 1);
		$mpdf->WriteHTML($this->renderPartial('printrecommendations',[
			'modelCita'=>$modelCita,
		]));
        $mpdf->Output();
        exit;
		
	}
	
	private function printReview($modelCita){
		
		$mpdf = new mPDF();
		$stylesheet = file_get_contents(Yii::$app->params['domain'].Yii::$app->view->theme->baseUrl.'/css/materialize.css');
		$mpdf->WriteHTML($stylesheet, 1);
		$mpdf->WriteHTML($this->renderPartial('printreview',[
			'modelCita'=>$modelCita,
		]));
        $mpdf->Output();
        exit;
		
	}
	
	public function actionAutosave(){
		
		$data = Yii::$app->request->post();
		$newCita = Yii::$app->request->post('citaId');
		$oldCita = Yii::$app->request->post('Cita');
		$modelCita = Cita::findOne($newCita);
		
		if(isset($oldCita)){
			$modelCita->examen_fisico = $oldCita['examen_fisico'];
			$modelCita->diagnostico = $oldCita['diagnostico'];
		}else{
			$modelCita->examen_fisico = '';
			$modelCita->diagnostico = '';
		}
		Yii::trace($oldCita);
		$this->updateCita($modelCita, $data);
		$this->saveCita($modelCita, $data);
		
		return '';
	}
}