<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use app\models\Usuario;

class AdminController extends Controller
{
	public $layout = 'leftmenu';
	
	public function actionIndex(){
		$this->layout = 'leftmenu';
		return $this->render('index');
	}
	
	public function actionUsers(){
		$dataProvider = new ActiveDataProvider([
            'query' => Usuario::find(),
        ]);
		return $this->render('users',[
            'dataProvider' => $dataProvider,
        ]);
	}
}
