	<?php
	
	/* @var $this yii\web\View */
use yii\helpers\Html;
use app\models\TipoIdentificacion;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use janisto\timepicker\TimePicker;


use app\models\Apetito;
use app\models\TipoCambioIngesta;
use app\models\IngestaAnamnesis;
use app\models\Circunferencias;
use app\models\ExamenReferencia;
use app\models\Alimento;
use app\models\EstadoCivil;
use app\models\Frecuencia;
use app\models\Menu;
use app\models\VolumenConsumido;
use app\models\ConductasCompensatorias;
use app\models\ConductaCita;
use app\models\TiempoComida;
use wbraganca\dynamicform\DynamicFormWidget;
	
	$this->title = 'MACA';
	?>
	
	    
	     <?php 
	     if(isset($accion)&& $accion=='NUEVO_PACIENTE'){
		     $form = ActiveForm::begin([
			         'action' =>['newpatient'],
			        'layout' => 'horizontal',
			        'fieldConfig' => [
				        'options' => [
				            'tag' => false,
				        ],
				    ],
				    'class' => 'form-horizontal',
			        
			    ]);  ?>
		    <div class="jumbotron">
			    <div class="row">
		        	<?php
		        	    
			    		echo $form->field($modelPersona, 'identificacion')->textInput(['style'=>'width:100px']);
			    		$tipoIdItems = ArrayHelper::map(TipoIdentificacion::find()->all(),'id','nombre');
			   			echo $form->field($modelPersona,'tipo_identificacion_id')->dropDownList($tipoIdItems)->label('Tipo Identificacion'); 
			   			echo Html::a('Nuevo',['site/index'], ['class' => 'btn btn-primary btn_query', 'name' => 'query-button']) 
			    
			   		?>
			   		<span style="padding-right: 50px">&nbsp;</span>
			   	</div>
		   		<div class="row">	
		   		<?php 
		   			echo $form->field($modelPersona, 'nombre')->textInput(['autofocus' => true,'style'=>'width:150px']);
		   			echo $form->field($modelPersona, 'apellido')->textInput(['style'=>'width:250px']);
		   		?>
		   		</div>
		   		<div class="row">	
		   		
		   		<?php 	
		   			echo $form->field($modelPersona, 'fecha_nacimiento')->widget(
		   										DatePicker::className(),[
		   											'dateFormat' =>'yyyy-MM-dd',
												    'clientOptions' => [
		   												'yearRange' => '-80:+0',
	           											'changeYear' => true
		   											],
		   											
											])->textInput(['placeholder' =>'Seleccione..' , 'style'=>'width:100px']);
											
					$estadoCivil = ArrayHelper::map(EstadoCivil::find()->all(),'id','nombre');
		   			echo $form->field($modelPersona,'estado_civil_id')->dropDownList($estadoCivil); 
		   		?>
		   		</div>
		   		<div class="row">
		   		<?php 
		   			echo $form->field($modelPersona,'genero')->dropDownList(['M' => 'Masculino','F' => 'Femenino']);
					echo $form->field($modelPersona, 'ocupacion')->textInput(['style'=>'width:250px']);
				?>
				</div>
		   		<div class="row">
				<?php 
		   			echo $form->field($modelPersona, 'telefono')->textInput(['style'=>'width:100px']);
		   			echo $form->field($modelPersona, 'email')->input('email',['style'=>'width:250px']);
		   			 echo $form->field($modelPersona, 'id')->hiddenInput()->label(false);
		   			?>
		    	</div>
		    	<div  class="row" style="text-align: right;" >
		    	 <?= Html::submitButton('Iniciar Cita', ['class' => 'btn btn-primary btn_query', 'name' => 'query-button']) ?>
		 		</div>
		 		<div class="row" style="text-align: right;">
		   			<div class="error" style="width: 290px;float:left">
		   			<?= $form->errorSummary($modelPersona); ?>
		   			</div>
		   		</div>
		    </div> <!-- Fin div jumbotron -->
		    
		    <div class="jumbotron">
		   		<div class="box_header">Historial de Citas</div>
		   		<?php 
		   		if(isset($modelPersona)){
			    	if(empty($modelPersona->citas)){
			    		echo 'La pesona no tiene citas asociadas';
			   		}else{
			   			
			    		?>
			    <div class="row">
			    	<div class="col-sm-1"><label class="negrita">id</label></div>
			    	<div class="col-sm-3"><label class="negrita">Paciente</label></div>
			    	<div class="col-sm-6"><label class="negrita">Fecha y hora</label></div>
			    	<div class="col-sm-6"><label class="negrita">Detalles</label></div>
			    	<div class="col-sm-6"><label class="negrita">Recomendaciones</label></div>
			    	
			    </div>
		   		
		   		<?php 
		    		foreach($modelPersona->citas as $cita){
		    			?>
		    			<div class="row">
		    				<div class="col-sm-1"><label><?=$cita->cita_id?></label></div>
		    				<div class="col-sm-3"><label><?=$modelPersona->nombre?>&nbsp;<?=$modelPersona->apellido?></label></div>
		    				<div class="col-sm-6"><label><?=$cita->fecha_hora?></label></div>
		    				<?php if($cita->personaMedico->id == Yii::$app->user->id){?>
		    				<div class="col-sm-6"> <?= Html::a('ver', ['/cita/review'], 
		    								 [
											    'data'=>[
											        'method' => 'post',
											        'params'=>['id'=>$cita->cita_id],
											    ],
		    									'class'=>'btn btn-primary'
											]) ; ?></div>
											
							<div class="col-sm-6"> <?= Html::a('Recomendación', ['/cita/recomendar'], 
		    								 [
											    'data'=>[
											        'method' => 'post',
											        'params'=>['id'=>$cita->cita_id],
											    ],
		    									'class'=>'btn btn-primary'
											]) ; ?></div>
		    			
		    			<?php }else{?>
		    					<div class="col-sm-6"><label>ND</label></div>
		    					<div class="col-sm-6"><label>ND</label></div>
		    			<?php
		    			}
		    			 ?></div><?php 
		    		}
		    		
			   	}
		    		?>
		    <?php 
		   		}
		    ?>
		    </div>
		   <?php 
		    ActiveForm::end(); 
		    ?>
		    <?php 
	     }else if($accion=='INICIAR_CITA'){
	     	
				$url = Yii::$app->basePath.'/views/cita/formulas.php';
				//Yii::trace('RUTA:'.$url);
				include $url;	     	
    ?>
	     	
	     	
	     	<div class="jumbotron">
	     	 <div class="row">
	        	<?php
	        	$form = ActiveForm::begin([
			         'action' =>['newpatient'],
			        'layout' => 'horizontal',
			        'fieldConfig' => [
	        			'horizontalCssClasses' => [
				            'label' => 'col-sm-2',
				            'offset' => 'col-sm-offset-2',
				        ],
				        'options' => [
				            'tag' => false,
				        ],
				    ],
				    'class' => 'form-horizontal',
			        
			    ]);   
		    		echo $form->field($modelPersona, 'identificacion')->textInput(['style'=>'width:100px','readonly'=>'true']);
		    		$tipoIdItems = ArrayHelper::map(TipoIdentificacion::find()->all(),'id','nombre');
		   			echo $form->field($modelPersona,'tipo_identificacion_id')->dropDownList($tipoIdItems)->label('Tipo Identificacion'); 
		   			echo Html::a('Nuevo',['site/index'], ['class' => 'btn btn-primary btn_query', 'name' => 'query-button']) ;
		     ActiveForm::end(); 
		   		?>
		   		<span style="padding-right: 30px">&nbsp;</span>
		   	</div>
		   	</div>
		   	<div><span style="padding-top: 30px">&nbsp;</span></div>
		   	<div class="jumbotron">
	     		<div class="row">
		     		<div class="col-sm-3"><label class="col-sm-3 negrita">Paciente: </label></div>
		     		<div class="col-sm-3"><label class="col-sm-6"><?php echo $modelPersona->nombre.' '.$modelPersona->apellido?></label></div>
		     		<div class="col-sm-3"><label class="col-sm-3 negrita">Ocupación: </label></div>
		     		<div class="col-sm-6"><label class="col-sm-6"><?php echo $modelPersona->ocupacion?></label></div>
	     			<div class="col-sm-3"><label class="col-sm-3 negrita">Genero: </label></div>
		     		<div class="col-sm-1"><label class="col-sm-3"><?php echo $modelPersona->genero?></label></div>
	     		
	     		</div>
	     		<div class="row">
		     		<div class="col-sm-3"><label class="col-sm-3 negrita">Telefono: </label></div>
		     		<div class="col-sm-6"><label class="col-sm-6"><?php echo $modelPersona->telefono?></label></div>
		     		<div class="col-sm-3"><label class="col-sm-3 negrita">Edad: </label></div>
		     		<div class="col-sm-6"><label class="col-sm-6"><?php echo $modelPersona->edad?> años</label></div>
	     		</div>
	     		<div class="row">
	     			<span style="width: 4px">&nbsp;</span>
	     		</div>
	     	</div>
	     	<span style="width: 4px">&nbsp;</span>
	     	
	     	<?php 
	     	$formCita = ActiveForm::begin([
	     			'id'=>'formCita',
		         'action' =>['diagnostico'],
		        'layout' => 'horizontal',
		        'fieldConfig' => [
			        'options' => [
			            'tag' => false,
			        ],
			    ],
			    'class' => 'form-horizontal',
		        
		    ]);
		    
		    ?>
		    
		    <div class="jumbotron">
		    
	     	<div class="box_header">Datos Atropométricos</div>
	     	<?= $formCita->field($modelCita,'cita_id')->hiddenInput()->label(false);?>
		      <?= $formCita->field($modelPersona,'id')->hiddenInput()->label(false);?>
		       <?= $formCita->field($modelPersona,'genero')->hiddenInput()->label(false);?>
		       <?= $formCita->field($modelPersona,'edad')->hiddenInput()->label(false);?>
	     	<?php 
	     	if(isset($modelAntropo)){?>
		     		<div>
			     		<table>
			     			<tr>
			     				<td  ><?= $formCita->field($modelAntropo, 'peso', ['horizontalCssClasses' => ['wrapper' => 'col-sm-2']])->textInput(['style'=>'width:50px'])->label(null,['class'=>'col-sm-3',]); ?></td>
							    <td width="40%"><?= $formCita->field($modelAntropo,'pgraso', ['horizontalCssClasses' => ['wrapper' => 'col-sm-2']])->textInput(['style'=>'width:50px','readonly'=>'true'])->label('Peso Graso',['class'=>'col-sm-3',]); ?></td>
							    <td ><?= $formCita->field($modelAntropo, 'cintura', ['horizontalCssClasses' => ['wrapper' => 'col-sm-2']])->textInput(['style'=>'width:50px'])->label('C. Cintura',['class'=>'col-sm-1']);?></td>
							</tr>
						 	<tr>
						 		<td ><?= $formCita->field($modelAntropo, 'talla', ['horizontalCssClasses' => ['wrapper' => 'col-sm-2']])->textInput(['style'=>'width:50px'])->label(null,['class'=>'col-sm-3']);?></td>
						 		<td ><?= $formCita->field($modelAntropo, 'plgraso', ['horizontalCssClasses' => ['wrapper' => 'col-sm-2']])->textInput(['style'=>'width:50px','readonly'=>'true'])->label('Peso Libre de grasa (kg)',['class'=>'col-sm-3']); ?></td>
						 		<td ><?= $formCita->field($modelAntropo, 'cadera', ['horizontalCssClasses' => ['wrapper' => 'col-sm-2']])->textInput(['style'=>'width:50px'])->label(null,['class'=>'col-sm-1']);?></td>
					    	  	 </tr>
			     			 <tr> 
								<td ><?= $formCita->field($modelAntropo,'imc', ['horizontalCssClasses' => ['wrapper' => 'col-sm-2']])->textInput(['style'=>'width:50px','readonly'=>'true'])->label('IMC (kg/mt2)',['class'=>'col-sm-3']); ?></td>
					    	  	<td ><?= $formCita->field($modelAntropo, 'grasa_objetivo', ['horizontalCssClasses' => ['wrapper' => 'col-sm-2']])->textInput(['style'=>'width:50px'])->label(null,['class'=>'col-sm-3']);?></td>
					    	  	<td ><?= $formCita->field($modelAntropo, 'carpo', ['horizontalCssClasses' => ['wrapper' => 'col-sm-2']])->textInput(['style'=>'width:50px'])->label(null,['class'=>'col-sm-1']);?></td>
							</tr>
							<tr> 
								<td ><?= $formCita->field($modelAntropo,'grasa_actual', ['horizontalCssClasses' => ['wrapper' => 'col-sm-2']])->textInput(['style'=>'width:50px'])->label('% Grasa Actual',['class'=>'col-sm-3']); ?></td>
					    		<td ><?= $formCita->field($modelAntropo, 'triceps', ['horizontalCssClasses' => ['wrapper' => 'col-sm-2']])->textInput(['style'=>'width:50px'])->label(null,['class'=>'col-sm-3']);?></td>
					    		<td ><?= $formCita->field($modelAntropo, 'peso_ideal_estructura_osea', ['horizontalCssClasses' => ['wrapper' => 'col-sm-2']])->textInput(['style'=>'width:50px','readonly'=>'true'])->label(null,['class'=>'col-sm-3']);?></td>
					    		
					    	</tr>
							
							<tr><td colspan="3" ><div class="error"><?= $formCita->errorSummary($modelAntropo); ?></div></tr>
						  </table>
			     	</div>
		     		<div >
		     			<center>
			     			<table class="small_table">
			     				<tr>
			     					<th>Interpretación Circunferencias</th><th></th><th>Interpretación</th>
			     				</tr>
			     				<?php 
			     				 $tiposCirc = Circunferencias::find()->all();
		    					 foreach($tiposCirc as $tipoCirc){
		    					 	
		    					 	$settings = array('style'=>'width:50px','readonly'=>'readonly');
		    					 	$settings150 = array('style'=>'width:150px','readonly'=>'readonly');
		    					 	if($tipoCirc->readonly ===0){unset($settings['readonly']); unset($settings150['readonly']);};
		    					 	//echo var_dump($settings);
			     					?>
		    					 	<tr>
		    					 		<td nowrap="nowrap"><?=$tipoCirc->nombre?><?php  ?></td> 
		    					 		<td><?=$formCita->field($modelInterpretacion, "[{$tipoCirc->id}]valor")->textInput($settings)->label(false);?> </td>
		    					 		<td><?=$formCita->field($modelInterpretacion, "[{$tipoCirc->id}]interpretacion")->textInput($settings150)->label(false);?> </td>
		    					 	</tr>	
		    					 <?php }
			     				?>
			     				
			     			</table>
		     			<center>
		     			<span style="width: 10px">&nbsp;</span>
		     		</div>
	     	</div>
	     		<?php 
	     		}//Fin if Antropometria
	     		?>
	     		<div class="jumbotron">
	     			<div class="box_header">Examenes de Laboratorio</div> 
	     				<?php DynamicFormWidget::begin([
					        'widgetContainer' => 'dynamicform_examen', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
					        'widgetBody' => '.container-items-examen', // required: css class selector
					        'widgetItem' => '.item-examen', // required: css class
					        'limit' => 50, // the maximum times, an element can be added (default 999)
					        'min' => 1, // 0 or 1 (default 1)
					        'insertButton' => '.add-item-examen', // css class
					        'deleteButton' => '.remove-item-examen', // css class
					        'model' => $modelsExamen[0],
					        'formId' => 'formCita',
					        'formFields' => [
					            'examen_id',
					            'valor',
					            'interpretacion',
					        ],
	    ]); ?>
	    
			    <table class="table table-bordered">
			    <thead>
		            <tr>
		            	<th class="table-bordered center">Parametro</th>		
		            	<th class="table-bordered center" >Valor</th>
		            	<th class="table-bordered center">Valor Referencia</th>
		            	<th class="table-bordered center">Fecha</th>
		            	<th class="table-bordered center">Interpretación</th>
		            	<th class="text-center table-bordered" style="width: 70px;">
		                    <button type="button" class="add-item-examen btn btn-success btn-xs">+</button>
		                </th>
		            </tr>
			    </thead>
			     <tbody class="container-items-examen">
	     
				            <?php 
				            $tipoExamen = ArrayHelper::map(ExamenReferencia::find()->all(),'id','nombre');
				            foreach ($modelsExamen as $i => $modelExamen): 
				            	
				            ?>
				            
				             <tr class="item-examen panel panel-default ">
				              
				                        <?php
				                            // necessary for update action.
				                            $valorReferencia = 'valorReferencia'."{$i}";
				                            if (! $modelExamen->isNewRecord) {
				                                echo Html::activeHiddenInput($modelFarmaco, "[{$i}]id");
				                            }
				                          
				                        ?>
				                        <td  class="table-bordered center">
				                        <?= $formCita->field($modelExamen,"[{$i}]examen_id",['wrapperOptions' =>false])
				                        			->dropDownList(
				                        				$tipoExamen,
				                        				[
				                        					'id' =>'Referencia'."{$i}",
				                        					'prompt'=>'Examen..',
				                        					'onchange'=>'
				                        						$.post("index.php?r=cita/updatevalor&id='.'"+$(this).val(),function( data ) {
							                  $( "#valor"+id ).html( data );
							                });'
				                        				])
				                        			->label(false) ?>
				                        </td>
				                         <td  class="table-bordered center" >
				                        <?= $formCita->field($modelExamen, "[{$i}]valor")->textInput(['style'=>'width:50px'])->label(false) ?>
				                        </td>
				                        <td  class="table-bordered center" >
											<div id="<?=$valorReferencia?>"></div>
				                        </td>
				                        <td  class="table-bordered center">
				                        <?= $formCita->field($modelExamen, "[{$i}]fecha")->textInput(['placeholder' =>'Seleccione..' , 'style'=>'width:150px','type'=>'date'])->label(false);
				                        ?>
				                        </td>
				                        <td  class="table-bordered center">
				                        <?= $formCita->field($modelExamen, "[{$i}]interpretacion")->textInput(['style'=>'width:150px'])->label(false) ?>
				                        </td>
				                         <td class="table-bordered">
			                            	<button type="button" class="remove-item-examen btn btn-danger btn-xs" >-</button>
			                            </td>
	    						</tr>
	    					 <?php endforeach; ?>
	    			</tbody>
	    		</table>		
	    <?php DynamicFormWidget::end(); ?>
	     			</div>
	     			
	     			
	     			
	     		<div class="jumbotron">
	     			<div class="box_header">Examen Físico</div> 
	     			<div class="row">
	     				<?= $formCita->field($modelCita, 'examen_fisico')->textarea(['rows' => '6', 'cols'=>'70']); ?>
	     			</div>
	     		</div>
	     		<div class="jumbotron">
	     			<div class="box_header">Recordatorio de 24 horas</div>
	     			
					    <?php DynamicFormWidget::begin([
					        'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
					        'widgetBody' => '.container-items', // required: css class selector
					        'widgetItem' => '.item', // required: css class
					        'limit' => 10, // the maximum times, an element can be added (default 999)
					        'min' => 1, // 0 or 1 (default 1)
					        'insertButton' => '.add-item', // css class
					        'deleteButton' => '.remove-item', // css class
					        'model' => $modelsRecordatorio[0],
					        'formId' => 'formCita',
					        'formFields' => [
					            'hora',
					            'preparacion',
					            'alimentos',
					            'cantidad',
					        ],
	    ]); ?>
	    <table class="table table-bordered">
	    <thead>
            <tr>
            	<th class="table-bordered center">Hora</th>		
            	<th class="table-bordered center">Tiempo</th>
            	<th class="table-bordered center">Preparación</th>
            	<th class="table-bordered center">Alimentos/Cantidad</th>
            	<th class="text-center table-bordered" style="width: 70px;">
                    <button type="button" class="add-item btn btn-success btn-xs">+</button>
                </th>
            </tr>
	    </thead>
	     <tbody class="container-items">
	        	         
	            <?php 
	             $tiempoComida = ArrayHelper::map(TiempoComida::find()->all(),'tiempo_id','nombre');
	            foreach ($modelsRecordatorio as $i => $modelRecordatorio): ?>
	            <tr class="item panel panel-default ">
	                        <?php
	                            // necessary for update action.
	                            if (! $modelRecordatorio->isNewRecord) {
	                                echo Html::activeHiddenInput($modelRecordatorio, "[{$i}]id");
	                            }
	                        ?>
	                        <td class="table-bordered center">
	                        	<?= $formCita->field($modelRecordatorio, "[{$i}]hora",['wrapperOptions' =>false])->textInput(['style'=>'width:40px'])->label(false) ?>
	                        </td>
	                        <td class="table-bordered center">
	                        	<div style="margin-top: -8px">
	                        	<?= $formCita->field($modelRecordatorio, "[{$i}]tiempo_id",['wrapperOptions' =>false])->dropDownList($tiempoComida,['style'=>'margin-top:-15px'])->label(false) ?>
	                        	</div>
	                        </td>
                            <td class="table-bordered center">
                                <?= $formCita->field($modelRecordatorio, "[{$i}]preparacion",['wrapperOptions' =>false])->textInput(['style'=>'width:150px'])->label(false) ?>
                            </td>
                            <td class="table-bordered center">
	                            <?= $this->render('_alimentos', [
				                        'form' => $formCita,
				                        'indexPreparacion' => $i,
				                        'modelsAlimento' => $modelRecordatorioPreparacion[$i],
				                    ]) ?>
                            </td>
                            <td>
                            	<button type="button" class="remove-item btn btn-danger btn-xs" >-</button>
                            </td>
	                        
	                </tr>
	            <?php endforeach; ?>
	           </tbody>
	          </table>
	            <!--  </div>
	        </div>
	    </div><!-- .panel -->
	    
	    
	    
	     			<?php DynamicFormWidget::end(); ?>
	     			</div>
	     		<div class="jumbotron">
	     			<div class="box_header">Frecuencia de consumo</div>	
	     		<table>
	     			<tr><th>Alimento</th><th colspan="2" style="text-align: center;">Frecuencia</th><th>Observacion</th></tr>
	     			<?php 
		     			$tiposAlimento = Alimento::find()->all();
		     			$frecuencias = ArrayHelper::map(Frecuencia::find()->all(),'id','frecuencia');
		     			$modelMenu = new Menu;
			   			foreach($tiposAlimento as $tipoAlimento){
			   			?>
			   				<tr>
			   					<td><?= $tipoAlimento->nombre?></td>
			   					<td><?= $formCita->field($modelMenu,"[{$tipoAlimento->alimento_id}]frecuencia_id",['wrapperOptions' =>false])->dropDownList($frecuencias)->label(false);  ?></td>
			   					<td><?= $formCita->field($modelMenu, "[{$tipoAlimento->alimento_id}]valor",['wrapperOptions' =>false])->textInput(['style'=>'width:50px'])->label(false) ?></td>
				                <td><?= $formCita->field($modelMenu, "[{$tipoAlimento->alimento_id}]observacion",['wrapperOptions' =>false])->textInput(['style'=>'width:200px'])->label(false) ?></td>
			   				</tr>	
			   				<?php 
			   			}?>
	     		</table>	
	     			
	     		</div>
	     		
	     		<?php 
		     	if(isset($modelAnamnesis)){
		     	?>
		     		<div class="jumbotron">
		     			<div class="box_header">Analisis de ingesta</div>
		     			<div class="row">
			     			<?php 
			     				echo $formCita->field($modelAnamnesis, 'analisis_ingesta')->textarea(['rows' => '6', 'cols'=>'70']);
			     			?>
		     			</div>
		     		</div>
		     		<div class="jumbotron">
		     			<div class="box_header">Datos Anamnesis alimentaria</div>
		     			<div class="row">
		     			<table>
		     				<tr>
		     					<td>
		     						<?php 
											$tipoApetito = ArrayHelper::map(Apetito::find()->all(),'id','nombre');
						   				    echo $formCita->field($modelAnamnesis,'apetito_id')->dropDownList($tipoApetito)->label('Apetito'); 
									?>
		     					</td>
		     					<td>
		     						<?=	 $formCita->field($modelAnamnesis,'cambios_ingesta')->radioList(array('S'=>'Si','N'=>'No'));
		   				 			 ?>
		     					</td>
		     				</tr>
		     				<?php 
		     					 $tiposCambioIngesta = TipoCambioIngesta::find()->all();
		   				    	foreach($tiposCambioIngesta as $key => $tipoCambio){
			   				    	$modelCambio = new IngestaAnamnesis;
			   				    	$modelCambio->tipo_ingesta_id = $tipoCambio->id;
		     				?>
		     				<tr>
		     				<td colspan="2">
		     					<?= $formCita->field($modelCambio, "[{$modelCambio->tipo_ingesta_id}]texto_descriptivo")->textarea(['rows' => '6', 'cols'=>'70'])->label($tipoCambio->nombre);
	    						 ?>
		     				</td>
		     				</tr>
		     				<?php }?>
		     				<tr>
		     					<td><?=  $formCita->field($modelAnamnesis, 'alimentos_preferidos')->textarea(['rows' => '6', 'cols'=>'70']);
			     			?></td>
			     			</tr>
			     			
			     			<tr>
		     					<td><?=  $formCita->field($modelAnamnesis, 'alimentos_rechazados')->textarea(['rows' => '6', 'cols'=>'70']);
			     			?></td>
			     			</tr>
			     			
			     			<tr>
		     					<td><?= $formCita->field($modelAnamnesis, 'intolerancias')->textarea(['rows' => '6', 'cols'=>'70']);
			     			?></td>
			     			</tr>
			     			<tr>
			     				<td>
			     				<?php 
			     					$volumen = ArrayHelper::map(VolumenConsumido::find()->all(),'id','nombre');
						   			echo $formCita->field($modelAnamnesis,'volumen_consumido_id')->dropDownList($volumen); 
									?>
								</td>
			     			</tr>
			     			<tr>
			     				<td><?=$formCita->field($modelAnamnesis, 'tiempo_empleado')->textarea(['rows' => '6', 'cols'=>'70']);?></td>
			     			</tr>
			     			<tr>
			     				<td><?=$formCita->field($modelAnamnesis, 'uso_sal')->radioList(array('1'=>'Si','0'=>'No'))?></td>
			     			</tr>
		     			</table>
						  			
		     			</div>
					</div>
	    <?php 
		     	}//Fin if Ananmnesis
		     	//Inicio Farmacos
		     	?>
		     	
		     	<div class="jumbotron">
		     		<div class="box_header">Farmacos</div>
	     			
					    <?php DynamicFormWidget::begin([
					        'widgetContainer' => 'dynamicform_inner', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
					        'widgetBody' => '.container-items2', // required: css class selector
					        'widgetItem' => '.item2', // required: css class
					        'limit' => 10, // the maximum times, an element can be added (default 999)
					        'min' => 1, // 0 or 1 (default 1)
					        'insertButton' => '.add-item2', // css class
					        'deleteButton' => '.remove-item2', // css class
					        'model' => $modelsFarmacos[0],
					        'formId' => 'formCita',
					        'formFields' => [
					            'nombre',
					            'dosis_frecuencia',
					            'objetivo',
					    		'prescripcion',
					        ],
	    ]); ?>
			    
			     	<div class="panel panel-default">
			                <button type="button" class="add-item2 btn btn-success btn-xs pull-right">+</button>
				        <div class="panel-body">
				            <div class="container-items2"><!-- widgetBody -->
				             <div class="row">
				             	<div class="col-sm-6" style="padding-right: 40px">Nombre</div>
				             	<div class="col-sm-3">Frecuencia</div>
				             	<div class="col-sm-6" style="padding-right: 40px">Objetivo</div>
				             	<div class="col-sm-4">Prescripción Médica</div>
				             </div>
				            <?php foreach ($modelsFarmacos as $i => $modelFarmaco): ?>
				                <div class="item2 panel panel-default"><!-- widgetItem -->
				                    <div class="row">
				                        <?php
				                            // necessary for update action.
				                            if (! $modelFarmaco->isNewRecord) {
				                                echo Html::activeHiddenInput($modelFarmaco, "[{$i}]id");
				                            }
				                        ?>
				                        <div class="col-sm-7" style="padding-right: 40px">
				                        <?= $formCita->field($modelFarmaco, "[{$i}]nombre",['wrapperOptions' =>false])->textInput(['style'=>'width:150px'])->label(false) ?>
				                        </div>
				                        	
				                            <div class="col-sm-7">
				                                <?= $formCita->field($modelFarmaco, "[{$i}]dosis_frecuencia",['wrapperOptions' =>false])->textInput(['style'=>'width:50px'])->label(false) ?>
				                            </div>
				                            <div class="col-sm-7" style="padding-right: 40px"> 
				                                <?= $formCita->field($modelFarmaco, "[{$i}]objetivo",['wrapperOptions' =>false])->textInput()->label(false) ?>
				                            </div>
				                             <div class="col-sm-7">
				                                <?= $formCita->field($modelFarmaco, "[{$i}]prescripcion",['wrapperOptions' =>false])->dropDownList(['0' => 'NO','1' => 'SI'])->label(false) ?>
				                            </div>
				                            <div class="col-sm-3">
				                             <button type="button" class="remove-item2 btn btn-danger btn-xs" >-</button>
				                            </div>
				                     </div><!-- .row -->
				                        
				                    </div>
				            <?php endforeach; ?>
				            </div>
				       	 </div>
			    	</div><!-- .panel -->
	     			<?php DynamicFormWidget::end(); ?>
	     		</div>
	     		<div class="jumbotron">
	     			<div class="box_header">Hábitos de vida</div>
	     			<table>
	     				<tr>
	     					<td >
	     						<?=  $formCita->field($modelHabitos, 'lugar_consumo')->textarea(['rows' => '6','cols'=>'70'])->label(null,['class'=>'col-sm-7']);?>
	     					</td>
	     				</tr>
	     				<tr>
			     				<td><?=$formCita->field($modelHabitos, 'compania')->radioList(array('1'=>'Si','0'=>'No'))->label(null,['class'=>'col-sm-7']);?></td>
			     			</tr>
	     				<tr>
	     					<td >
	     						<?=  $formCita->field($modelHabitos, 'quien_prepara')->textInput(['style'=>'width:300px;'])->label(null,['class'=>'col-sm-7']);?>
	     					</td>
	     				</tr>
	     				<tr>
	     					<td>
	     						<?=  $formCita->field($modelHabitos, 'licor')->textInput(['style'=>'width:250px;'])	?>
	     					</td>
	     				</tr>
	     				<tr>
	     					<td>
	     						<?=  $formCita->field($modelHabitos, 'cigarrillo')->textInput(['style'=>'width:250px;']);?>
	     					</td>
	     				</tr>
	     				<tr>
	     					<td >
	     						<?=  $formCita->field($modelHabitos, 'actividad_fisica')->textarea(['rows' => '6','cols'=>'70'])->label(null,['class'=>'col-sm-7']);?>
	     					</td>
	     				</tr>
	     				<tr>
	     					<td >
	     						<?=  $formCita->field($modelHabitos, 'deporte')->textInput(['style'=>'width:300px;'])->label(null,['class'=>'col-sm-7']);?>
	     					</td>
	     				</tr>
	     				<tr>
	     					<td >
	     						<?=  $formCita->field($modelHabitos, 'habito_intestinal')->textInput(['style'=>'width:300px;'])->label(null,['class'=>'col-sm-7']);?>
	     					</td>
	     				</tr>
	     				<tr>
	     					<td >
	     						<?=  $formCita->field($modelHabitos, 'horas_sueno')->textInput(['style'=>'width:50px;'])->label(null,['class'=>'col-sm-7']);?>
	     					</td>
	     				</tr>
	     			</table>
	     		</div>
	     		<div class="jumbotron">
	     			<div class="box_header">Estado Psicosocial</div>
	     			<table>
	     				<tr>
	     					<td>
				         		<?= $formCita->field($modelPsicosocial, 'quien_vive')->textInput()->label(null,['class'=>'col-sm-7']); ?>
				         	</td>
				         </tr>
				         <tr>
				         	<td>
	     						<?= $formCita->field($modelPsicosocial, 'relaciones_amistad')->radioList(array('1'=>'Si','0'=>'No'))->label(null,['class'=>'col-sm-7'])?>
	     					</td>
	     				</tr>
	     			</table>
	     		</div>
	     		<div class="jumbotron">
	     			<div class="box_header">Conductas compensatorias asociadas a la enfermedad</div>
	     			<table>
	     				
	     			<?php 
	     				$tiposConducta = ConductasCompensatorias::find()->all();
			   			foreach($tiposConducta as $conducta){
			   					$modelConducta = new ConductaCita;
			   					
			   			?>
			   				<tr>
			   					<td width="180px"><label class="negrita"><?= $conducta->conducta?></label></td>
			   					<td class="col-sm-7"><?= $formCita->field($modelConducta,"[{$conducta->id}]posee_conducta")->dropDownList(['0' => 'NO','1' => 'SI'])->label(false);  ?></td>
			   					<td class="col-sm-9" ><?= $formCita->field($modelConducta, "[{$conducta->id}]frecuencia")->textInput(['style'=>'width:150px'])->label(null);?></td>
			   				</tr>	
			   				<?php 
			   			}?>
	     			</table>
	     		</div>
	     		
	     		<div class="jumbotron">
	     			<div class="box_header">Diagnostico Nutricional</div>
	     			<div class="row">
	    				<?php 
	    				echo $formCita->field($modelCita, 'diagnostico')->textarea(['rows' => '6','cols'=>'70']);
	    				?>
		    		</div>
		    		<div class="row">
		    			<?= Html::submitButton('Guardar', ['class' => 'btn btn-primary btn_query', 'name' => 'save-button']) ?>
		    		</div>
		    		
	     		</div>
	     		
	     		<?php
	     		 ActiveForm::end(); 
	     	} //fin else nueva cita
	     ?>
	    
	
	<div class="body-content">
	
	       
	
	</div>