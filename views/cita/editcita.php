	<?php
	
	/* @var $this yii\web\View */
use yii\helpers\Html;
use app\models\TipoIdentificacion;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use janisto\timepicker\TimePicker;
use app\models\TipoAntecedente;
use app\models\Antecedentes;
use app\models\Apetito;
use app\models\TipoCambioIngesta;
use app\models\IngestaAnamnesis;
use app\models\Circunferencias;
use app\models\ExamenReferencia;
use app\models\Alimento;
use app\models\EstadoCivil;
use app\models\Frecuencia;
use app\models\Menu;
use app\models\Habitos;
use app\models\VolumenConsumido;
use app\models\ConductasCompensatorias;
use app\models\ConductaCita;
use app\models\TiempoComida;
use app\models\InterpretacionCircunferencia;

use wbraganca\dynamicform\DynamicFormWidget;
	
	$this->title = 'MACA';
	?>
	
	    <?php 
	     	
				$url = Yii::$app->basePath.'/views/cita/formulasedit.php';
				//Yii::trace('RUTA:'.$url);
				include $url;	     	
    ?>
	     	
	     	
	     	<div class="jumbotron">
	     	<div class="box_header">Paciente</div>
	     	 <div class="row">
	        	<?php
	        	$form = ActiveForm::begin([
			         'action' =>['newpatient'],
			        'layout' => 'horizontal',
			        'fieldConfig' => [
	        			'horizontalCssClasses' => [
				            'label' => 'col-sm-2',
				            'offset' => 'col-sm-offset-2',
				            'wrapper' => 'col-sm-6',
				        ],
				        'options' => [
				            'tag' => false,
				        ],
				    ],
				    'class' => 'form-horizontal',
			        
			    ]);   
		    		echo $form->field($modelCita->persona, 'identificacion')->textInput(['style'=>'width:100px','readonly'=>'true']);
		    		$tipoIdItems = ArrayHelper::map(TipoIdentificacion::find()->all(),'id','nombre');
		   			echo $form->field($modelCita->persona,'tipo_identificacion_id')->dropDownList($tipoIdItems)->label('Tipo Identificacion'); 
		   			echo Html::a('Nuevo',['site/index'], ['class' => 'btn btn-primary btn_query', 'name' => 'query-button']) ;
		     ActiveForm::end(); 
		   		?>
		   		<span style="padding-right: 30px">&nbsp;</span>
		   	</div>
		   	</div>
		   	<div><span style="padding-top: 30px">&nbsp;</span></div>
		   	<div class="jumbotron">
	     		<div class="row">
		     		<div class="col-sm-3"><label class="col-sm-3 negrita">Paciente: </label></div>
		     		<div class="col-sm-3"><label class="col-sm-6"><?php echo $modelCita->persona->nombre.' '.$modelCita->persona->apellido?></label></div>
		     		<div class="col-sm-3"><label class="col-sm-3 negrita">Ocupación: </label></div>
		     		<div class="col-sm-6"><label class="col-sm-6"><?php echo $modelCita->persona->ocupacion?></label></div>
	     			<div class="col-sm-3"><label class="col-sm-3 negrita">Genero: </label></div>
		     		<div class="col-sm-1"><label class="col-sm-3"><?php echo $modelCita->persona->genero?></label></div>
	     		
	     		</div>
	     		<div class="row">
		     		<div class="col-sm-3"><label class="col-sm-3 negrita">Telefono: </label></div>
		     		<div class="col-sm-6"><label class="col-sm-6"><?php echo $modelCita->persona->telefono?></label></div>
		     		<div class="col-sm-3"><label class="col-sm-3 negrita">Edad: </label></div>
		     		<div class="col-sm-6"><label class="col-sm-6"><?php echo $modelCita->persona->edad?> años</label></div>
	     		</div>
	     		<div class="row">
	     			<span style="width: 4px">&nbsp;</span>
	     		</div>
	     	</div>
	     	<span style="width: 4px">&nbsp;</span>
	     	
	     	<?php 
	     	$formCita = ActiveForm::begin([
	     			'id'=>'formCita',
		         'action' =>['diagnostico'],
		        'layout' => 'horizontal',
		        'fieldConfig' => [
			        'options' => [
			            'tag' => false,
			        ],
			    ],
			    'class' => 'form-horizontal',
		        
		    ]);
		    
		    if(isset($modelCita->antropometria)){?>
		    
				    <div class="jumbotron">
				    
			     	<div class="box_header">Datos Atropométricos</div>
			     	<?php if(isset($citaId)&& !empty($citaId)){
			     		echo Html::hiddenInput('citaId', $citaId);
		    		}
		    		if(isset($update)){
		    			echo Html::hiddenInput('update', $update);
		    		}
		    		?>
			     	<?= $formCita->field($modelCita,'cita_id')->hiddenInput()->label(false);?>
				      <?= $formCita->field($modelCita->persona,'id')->hiddenInput()->label(false);?>
				       <?= $formCita->field($modelCita->persona,'genero')->hiddenInput()->label(false);?>
				       <?= $formCita->field($modelCita->persona,'edad')->hiddenInput()->label(false);?>
			     	
				     		<div>
					     		<table>
			     			<tr>
			     				<td  ><?= $formCita->field($modelCita->antropometria, 'peso', ['horizontalCssClasses' => ['wrapper' => 'col-sm-2']])->textInput(['style'=>'width:50px'])->label(null,['class'=>'col-sm-3',]); ?></td>
							    <td width="40%"><?= $formCita->field($modelCita->antropometria,'pgraso', ['horizontalCssClasses' => ['wrapper' => 'col-sm-2']])->textInput(['style'=>'width:50px','readonly'=>'true'])->label('Peso Graso (kg)',['class'=>'col-sm-3',]); ?></td>
							    <td ><?= $formCita->field($modelCita->antropometria, 'cintura', ['horizontalCssClasses' => ['wrapper' => 'col-sm-2']])->textInput(['style'=>'width:50px'])->label('C. Cintura (cm)',['class'=>'col-sm-3']);?></td>
							</tr>
						 	<tr>
						 		<td ><?= $formCita->field($modelCita->antropometria, 'talla', ['horizontalCssClasses' => ['wrapper' => 'col-sm-2']])->textInput(['style'=>'width:50px'])->label(null,['class'=>'col-sm-3']);?></td>
						 		<td ><?= $formCita->field($modelCita->antropometria, 'plgraso', ['horizontalCssClasses' => ['wrapper' => 'col-sm-2']])->textInput(['style'=>'width:50px','readonly'=>'true'])->label('Peso Libre de grasa (kg)',['class'=>'col-sm-3']); ?></td>
						 		<td ><?= $formCita->field($modelCita->antropometria, 'cadera', ['horizontalCssClasses' => ['wrapper' => 'col-sm-2']])->textInput(['style'=>'width:50px'])->label(null,['class'=>'col-sm-3']);?></td>
					    	  	 </tr>
			     			 <tr> 
								<td ><?= $formCita->field($modelCita->antropometria,'imc', ['horizontalCssClasses' => ['wrapper' => 'col-sm-2']])->textInput(['style'=>'width:50px','readonly'=>'true'])->label('IMC (kg/mt2)',['class'=>'col-sm-3']); ?></td>
					    	  	<td ><?= $formCita->field($modelCita->antropometria, 'grasa_objetivo', ['horizontalCssClasses' => ['wrapper' => 'col-sm-2']])->textInput(['style'=>'width:50px'])->label(null,['class'=>'col-sm-3']);?></td>
					    	  	<td ><?= $formCita->field($modelCita->antropometria, 'carpo', ['horizontalCssClasses' => ['wrapper' => 'col-sm-2']])->textInput(['style'=>'width:50px'])->label(null,['class'=>'col-sm-3']);?></td>
							</tr>
							<tr> 
								<td ><?= $formCita->field($modelCita->antropometria,'grasa_actual', ['horizontalCssClasses' => ['wrapper' => 'col-sm-2']])->textInput(['style'=>'width:50px'])->label('% Grasa Actual',['class'=>'col-sm-3']); ?></td>
					    		<td ><?= $formCita->field($modelCita->antropometria, 'triceps', ['horizontalCssClasses' => ['wrapper' => 'col-sm-2']])->textInput(['style'=>'width:50px'])->label(null,['class'=>'col-sm-3']);?></td>
					    		<td ><?= $formCita->field($modelCita->antropometria, 'peso_ideal_estructura_osea', ['horizontalCssClasses' => ['wrapper' => 'col-sm-2']])->textInput(['style'=>'width:50px','readonly'=>'true'])->label(null,['class'=>'col-sm-3']);?></td>
					    	</tr>
							<tr> 
								<td colspan="4" ><?= $formCita->field($modelCita->antropometria,'interpretacion_imc', ['horizontalCssClasses' => ['wrapper' => 'col-sm-2']])->textInput(['style'=>'width:350px'])->label(null,['class'=>'col-sm-3']); ?></td>
					    	</tr>		
							<tr><td colspan="3" ><div class="error"><?= $formCita->errorSummary($modelCita->antropometria); ?></div></tr>
						  </table>
					     	</div>
					     	<div style="visibility: hidden;">
					     		<table><tr><td><?php echo Html::Button('guardar',array('onclick'=>'send();','id'=>'sendButton')); ?></td></tr></table>
					     	</div>
				     		<div >
				     		
				     			<center>
					     			<table class="small_table">
					     				<tr>
					     					<th>Interpretación Circunferencias</th><th>Valor</th><th>Interpretación</th>
					     				</tr>
					     				<?php 
					     				 $tiposCirc = Circunferencias::find()->all();
				    					 foreach($tiposCirc as $tipoCirc){
				    					 	$settings = array('style'=>'width:50px','readonly'=>'readonly');
				    					 	$settings150 = array('style'=>'width:150px','readonly'=>'readonly');
				    					 	if($tipoCirc->readonly ===0){unset($settings['readonly']); unset($settings150['readonly']);};
				    					 	$encontro = false;
				    					 	foreach($modelCita->interpretacionCircunferencias as $circunferencia){
				    					 		if($circunferencia->circunferencia_id ===$tipoCirc->id){
				    					 			$encontro = true;
				    					 			?>
						    					 	<tr>
						    					 		<td nowrap="nowrap"><?=$tipoCirc->nombre?><?php  ?></td> 
						    					 		<td><?=$formCita->field($circunferencia, "[{$tipoCirc->id}]valor")->textInput($settings)->label(false);?> </td>
						    					 		<td><?=$formCita->field($circunferencia, "[{$tipoCirc->id}]interpretacion")->textInput($settings150)->label(false);?> </td>
						    					 	</tr>	
						    					 	<?php 
				    					 		}
				    					 	}
				    					 	if(!$encontro){
				    					 		$modelInterpretacion = new InterpretacionCircunferencia;
				    					 	?>
				    					 	<tr>
				    					 		<td nowrap="nowrap"><?=$tipoCirc->nombre?><?php  ?></td> 
				    					 		<td><?=$formCita->field($modelInterpretacion, "[{$tipoCirc->id}]valor")->textInput($settings)->label(false);?> </td>
				    					 		<td><?=$formCita->field($modelInterpretacion, "[{$tipoCirc->id}]interpretacion")->textInput($settings150)->label(false);?> </td>
				    					 	</tr>
				    					 	<?php }
				    					 	
				    					 	//echo var_dump($settings);
					     					?>
				    					 		
				    					 <?php }
					     				?>
					     				
					     			</table>
				     			<center>
				     			<span style="width: 10px">&nbsp;</span>
				     		</div>
			     	</div>
	     		<?php
	     		}//Fin if Antropometria
	     		 if(isset($modelCita->examenes)&& !empty($modelCita->examenes)){
	     		?>
	     		<div class="jumbotron">
	     			<div class="box_header">Examenes de Laboratorio</div> 
	     				<?php DynamicFormWidget::begin([
					        'widgetContainer' => 'dynamicform_examen', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
					        'widgetBody' => '.container-items-examen', // required: css class selector
					        'widgetItem' => '.item-examen', // required: css class
					        'limit' => 30, // the maximum times, an element can be added (default 999)
					        'min' => 1, // 0 or 1 (default 1)
					        'insertButton' => '.add-item-examen', // css class
					        'deleteButton' => '.remove-item-examen', // css class
					        'model' => $modelCita->examenes[0],
					        'formId' => 'formCita',
					        'formFields' => [
					            'examen_id',
					            'valor',
					            'interpretacion',
					        ],
	    ]); ?>
	    
			    <table class="table table-bordered">
			    <thead>
		            <tr>
		            	<th class="table-bordered center">Parametro</th>		
		            	<th class="table-bordered center" >Valor</th>
		            	<th class="table-bordered center">Valor Referencia</th>
		            	<th class="table-bordered center">Fecha</th>
		            	<th class="table-bordered center">Interpretación</th>
		            	<th class="text-center table-bordered" style="width: 70px;">
		                    <button type="button" class="add-item-examen btn btn-success btn-xs">+</button>
		                </th>
		            </tr>
			    </thead>
			     <tbody class="container-items-examen">
	     
				            <?php 
				            $tipoExamen = ArrayHelper::map(ExamenReferencia::find()->all(),'id','nombre');
				            foreach ($modelCita->examenes as $i => $modelExamen): 
				            	
				            ?>
				            
				             <tr class="item-examen panel panel-default ">
				              
				                        <?php
				                            // necessary for update action.
				                            $valorReferencia = 'valorReferencia'."{$i}";
				                            if (! $modelExamen->isNewRecord) {
				                                echo Html::activeHiddenInput($modelExamen, "[{$i}]examen_id");
				                            }
				                          
				                        ?>
				                        <td  class="table-bordered center">
				                        <?= $formCita->field($modelExamen,"[{$i}]examen_id",['wrapperOptions' =>false])
				                        			->dropDownList(
				                        				$tipoExamen,
				                        				[
				                        					'id' =>'Referencia'."{$i}",
				                        					'prompt'=>'Examen..',
				                        					'onchange'=>'
				                        						$.post("index.php?r=cita/updatevalor&id='.'"+$(this).val(),function( data ) {
							                  $( "#valor"+id ).html( data );
							                });'
				                        				])
				                        			->label(false) ?>
				                        </td>
				                         <td  class="table-bordered center" >
				                        <?= $formCita->field($modelExamen, "[{$i}]valor")->textInput(['style'=>'width:50px'])->label(false) ?>
				                        </td>
				                        <td  class="table-bordered center" >
											<div id="<?=$valorReferencia?>"></div>
				                        </td>
				                        <td  class="table-bordered center">
				                        <?= $formCita->field($modelExamen, "[{$i}]fecha")->textInput(['placeholder' =>'Seleccione..' , 'style'=>'width:150px','type'=>'date'])->label(false);
				                        ?>
				                        </td>
				                        <td  class="table-bordered center">
				                        <?= $formCita->field($modelExamen, "[{$i}]interpretacion")->textInput(['style'=>'width:150px'])->label(false) ?>
				                        </td>
				                         <td class="table-bordered">
			                            	<button type="button" class="remove-item-examen btn btn-danger btn-xs" >-</button>
			                            </td>
	    						</tr>
	    					 <?php endforeach; ?>
	    			</tbody>
	    		</table>		
	    <?php DynamicFormWidget::end(); 
	    
	     		?>
	     			</div>
	     		<?php  }?>	
	     			
	     			
	     		<div class="jumbotron">
	     			<div class="box_header">Examen Físico</div> 
	     			<div class="row">
	     				<?= $formCita->field($modelCita, 'examen_fisico')->textarea(['rows' => '6', 'cols'=>'70']); ?>
	     			</div>
	     		</div>
	     		<?php 
	     		
	     		if(isset($modelCita->recordatorios)&& !empty($modelCita->recordatorios)){
	     		?>
	     		
	     		<div class="jumbotron">
	     			<div class="box_header">Recordatorio de 24 horas</div>
	     			
					    <?php DynamicFormWidget::begin([
					        'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
					        'widgetBody' => '.container-items', // required: css class selector
					        'widgetItem' => '.item', // required: css class
					        'limit' => 20, // the maximum times, an element can be added (default 999)
					        'min' => 1, // 0 or 1 (default 1)
					        'insertButton' => '.add-item', // css class
					        'deleteButton' => '.remove-item', // css class
					        'model' => $modelCita->recordatorios[0],
					        'formId' => 'formCita',
					        'formFields' => [
					            'hora',
					            'preparacion',
					            'alimentos',
					            'cantidad',
					        ],
	   					 ]); ?>
					    <table class="table table-bordered">
					    <thead>
				            <tr>
				            	<th class="table-bordered center">Hora</th>		
				            	<th class="table-bordered center">Tiempo</th>
				            	<th class="table-bordered center">Preparación</th>
				            	<th class="table-bordered center">Alimentos/Cantidad</th>
				            	<th class="text-center table-bordered" style="width: 70px;">
				                    <button type="button" class="add-item btn btn-success btn-xs">+</button>
				                </th>
				            </tr>
					    </thead>
					     <tbody class="container-items">
					        	         
					            <?php 
					             $tiempoComida = ArrayHelper::map(TiempoComida::find()->all(),'tiempo_id','nombre');
					            foreach ($modelCita->recordatorios as $i => $modelRecordatorio): ?>
					            <tr class="item panel panel-default ">
					                        <?php
					                            // necessary for update action.
					                            if (!$modelRecordatorio->isNewRecord) {
					                                echo Html::activeHiddenInput($modelRecordatorio, "[{$i}]id");
					                            }
					                        ?>
					                        <td class="table-bordered center">
					                        	<?= $formCita->field($modelRecordatorio, "[{$i}]hora",['wrapperOptions' =>false])->textInput(['style'=>'width:40px'])->label(false) ?>
					                        </td>
					                        <td class="table-bordered center">
					                        	<div style="margin-top: -8px">
					                        	<?= $formCita->field($modelRecordatorio, "[{$i}]tiempo_id",['wrapperOptions' =>false])->dropDownList($tiempoComida,['style'=>'margin-top:-15px'])->label(false) ?>
					                        	</div>
					                        </td>
				                            <td class="table-bordered center">
				                                <?= $formCita->field($modelRecordatorio, "[{$i}]preparacion",['wrapperOptions' =>false])->textInput(['style'=>'width:350px'])->label(false) ?>
				                            </td>
				                            <td class="table-bordered center">
					                            <?= $this->render('_alimentos', [
								                        'form' => $formCita,
								                        'indexPreparacion' => $i,
								                        'modelsAlimento' => $modelRecordatorio->recordatorioPreparaciones,
								                    ]) ?>
				                            </td>
				                            <td>
				                            	<button type="button" class="remove-item btn btn-danger btn-xs" >-</button>
				                            </td>
					                        
					                </tr>
					            <?php endforeach; ?>
					           </tbody>
					          </table>
	            <!--  </div>
	        </div>
	    </div><!-- .panel -->
	    
	    
	    
	     			<?php DynamicFormWidget::end();?>
	     		</div>
	     		<div class="jumbotron">
	     			<div class="box_header">Frecuencia de Consumo</div>	
	     		<table>
	     			<tr><th>Alimento</th><th colspan="2">Frecuencia</th><th>Observacion</th></tr>
	     			<?php 
		     			$tiposAlimento = Alimento::find()->orderBy(['orden'=>SORT_ASC])->all();
		     			$frecuencias = ArrayHelper::map(Frecuencia::find()->all(),'id','frecuencia');
		     			$modelMenu = new Menu;
			   			foreach($tiposAlimento as $tipoAlimento){
			   				$encontro = false;
			   				foreach($modelCita->menus as $menu){
			   					if($tipoAlimento->alimento_id === $menu->alimento_id){
			   						$encontro = true;
			   					?>
			   						<tr>
					   					<td><?= $tipoAlimento->nombre?></td>
					   					<td><?= $formCita->field($menu,"[{$menu->alimento_id}]frecuencia_id",['wrapperOptions' =>false])->dropDownList($frecuencias)->label(false);  ?></td>
					   					<td><?= $formCita->field($menu, "[{$menu->alimento_id}]valor",['wrapperOptions' =>false])->textInput(['style'=>'width:50px'])->label(false) ?></td>
						                <td><?= $formCita->field($menu, "[{$menu->alimento_id}]observacion",['wrapperOptions' =>false])->textInput(['style'=>'width:150px'])->label(false) ?></td>
					   				</tr>	
			   					<?php 	
			   					}
			   				}
			   				if(!$encontro){
			   					$modelMenu = new Menu;
			   			?>
			   				<tr>
			   					<td><?= $tipoAlimento->nombre?></td>
			   					<td><?= $formCita->field($modelMenu,"[{$tipoAlimento->alimento_id}]frecuencia_id",['wrapperOptions' =>false])->dropDownList($frecuencias)->label(false);  ?></td>
			   					<td><?= $formCita->field($modelMenu, "[{$tipoAlimento->alimento_id}]valor",['wrapperOptions' =>false])->textInput(['style'=>'width:50px'])->label(false) ?></td>
				                <td><?= $formCita->field($modelMenu, "[{$tipoAlimento->alimento_id}]observacion",['wrapperOptions' =>false])->textInput(['style'=>'width:150px'])->label(false) ?></td>
			   				</tr>	
			   				<?php 
			   				}
			   			}?>
	     		</table>	
	     			
	     			
	     		</div>
	     		<?php }?>
	     		<?php 
	     		
		     	if(isset($modelCita->anamnesis)){
		     	?>
		     		<div class="jumbotron">
		     			<div class="box_header">Analisis de ingesta</div>
		     			<div class="row">
			     			<?php 
			     				echo $formCita->field($modelCita->anamnesis, 'analisis_ingesta')->textarea(['rows' => '6', 'cols'=>'70']);
			     			?>
		     			</div>
		     		</div>
		     		<div class="jumbotron">
		     			<div class="box_header">Datos Anamnesis alimentaria</div>
		     			<div class="row">
		     			<table>
		     				<tr>
		     					<td>
		     						<?php 
											$tipoApetito = ArrayHelper::map(Apetito::find()->all(),'id','nombre');
						   				    echo $formCita->field($modelCita->anamnesis,'apetito_id')->dropDownList($tipoApetito)->label('Apetito'); 
									?>
		     					</td>
		     					<td>
		     						<?=	 $formCita->field($modelCita->anamnesis,'cambios_ingesta')->radioList(array('S'=>'Si','N'=>'No'));
		   				 			 ?>
		     					</td>
		     				</tr>
		     				<?php 
		     					 $tiposCambioIngesta = TipoCambioIngesta::find()->all();
		   				    	foreach($tiposCambioIngesta as $tipoCambio){
			   				    	$modelCambio = new IngestaAnamnesis;
			   				    	$modelCambio->tipo_ingesta_id = $tipoCambio->id;
			   				    	$encontro = false;
			   				    	foreach($modelCita->anamnesis->ingestaAnamneses as $anamneses){
			   				    		if($tipoCambio->id === $anamneses->tipo_ingesta_id){
			   				    			$encontro = true;
			   				    		?>
			   				    			<tr>
							     				<td colspan="2">
							     					<?= $formCita->field($anamneses, 'texto_descriptivo')->textarea(['rows' => '6', 'cols'=>'70','name'=>'IngestaAnamnesis['.$anamneses->tipo_ingesta_id.']','id'=>'IngestaAnamnesis['.$anamneses->tipo_ingesta_id.']'])->label($tipoCambio->nombre);
						    						 ?>
							     				</td>
						     				</tr>
			   				    		<?php 
			   				    		}
			   				    	}
			   				    	if(!$encontro){
			   				    		?>
				     				<tr>
				     				<td colspan="2">
				     					<?= $formCita->field($modelCambio, 'texto_descriptivo')->textarea(['rows' => '6', 'cols'=>'70','name'=>'IngestaAnamnesis['.$modelCambio->tipo_ingesta_id.']','id'=>'IngestaAnamnesis['.$modelCambio->tipo_ingesta_id.']'])->label($tipoCambio->nombre);
			    						 ?>
				     				</td>
				     				</tr>
				     				<?php
			   				    	}
		     				
		     				 }?>
		     				<tr>
		     					<td><?=  $formCita->field($modelCita->anamnesis, 'alimentos_preferidos')->textarea(['rows' => '6', 'cols'=>'70']);
			     			?></td>
			     			</tr>
			     			
			     			<tr>
		     					<td><?=  $formCita->field($modelCita->anamnesis, 'alimentos_rechazados')->textarea(['rows' => '6', 'cols'=>'70']);
			     			?></td>
			     			</tr>
			     			
			     			<tr>
		     					<td><?= $formCita->field($modelCita->anamnesis, 'intolerancias')->textarea(['rows' => '6', 'cols'=>'70']);
			     			?></td>
			     			</tr>
			     			<tr>
			     				<td>
			     				<?php 
			     					$volumen = ArrayHelper::map(VolumenConsumido::find()->all(),'id','nombre');
						   			echo $formCita->field($modelCita->anamnesis,'volumen_consumido_id')->dropDownList($volumen); 
									?>
								</td>
			     			</tr>
			     			<tr>
			     				<td><?=$formCita->field($modelCita->anamnesis, 'tiempo_empleado')->textarea(['rows' => '6', 'cols'=>'70']);?></td>
			     			</tr>
			     			<tr>
			     				<td><?=$formCita->field($modelCita->anamnesis, 'uso_sal')->radioList(array('1'=>'Si','0'=>'No'))?></td>
			     			</tr>
		     			</table>
						  			
		     			</div>
					</div>
	    <?php 
		     	}//Fin if Ananmnesis
		     	if(isset($modelCita->farmacos)&&!empty($modelCita->farmacos))
		     	{
		     	//Inicio Farmacos
		     	?>
		     	
		     	<div class="jumbotron">
		     		<div class="box_header">Farmacos y/o suplementos</div>
	     			
					    <?php DynamicFormWidget::begin([
					        'widgetContainer' => 'dynamicform_inner', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
					        'widgetBody' => '.container-items2', // required: css class selector
					        'widgetItem' => '.item2', // required: css class
					        'limit' => 10, // the maximum times, an element can be added (default 999)
					        'min' => 1, // 0 or 1 (default 1)
					        'insertButton' => '.add-item2', // css class
					        'deleteButton' => '.remove-item2', // css class
					        'model' => $modelCita->farmacos[0],
					        'formId' => 'formCita',
					        'formFields' => [
					            'nombre',
					            'dosis_frecuencia',
					            'objetivo',
					    		'prescripcion'
					        ],
	    ]); ?>
			    
			     	<div class="panel panel-default">
			                <button type="button" class="add-item2 btn btn-success btn-xs pull-right">+</button>
				        <div class="panel-body">
				            <div class="container-items2"><!-- widgetBody -->
				             <div class="row">
				             	<div class="col-sm-6" style="padding-right: 40px">Nombre</div>
				             	<div class="col-sm-3">Frecuencia</div>
				             	<div class="col-sm-6" style="padding-right: 40px">Objetivo</div>
				             	<div class="col-sm-4">Prescripción Médica</div>
				             </div>
				            <?php foreach ($modelCita->farmacos as $i => $modelFarmaco): ?>
				                <div class="item2 panel panel-default"><!-- widgetItem -->
				                    <div class="row">
				                        <?php
				                            // necessary for update action.
				                            if (! $modelFarmaco->isNewRecord) {
				                                echo Html::activeHiddenInput($modelFarmaco, "[{$i}]id");
				                            }
				                        ?>
				                        <div class="col-sm-7" style="padding-right: 40px">
				                        <?= $formCita->field($modelFarmaco, "[{$i}]nombre",['wrapperOptions' =>false])->textInput(['style'=>'width:150px'])->label(false) ?>
				                        </div>
				                        	
				                            <div class="col-sm-7">
				                                <?= $formCita->field($modelFarmaco, "[{$i}]dosis_frecuencia",['wrapperOptions' =>false])->textInput(['style'=>'width:50px'])->label(false) ?>
				                            </div>
				                            <div class="col-sm-7" style="padding-right: 40px">
				                                <?= $formCita->field($modelFarmaco, "[{$i}]objetivo",['wrapperOptions' =>false])->textInput()->label(false) ?>
				                            </div>
				                             <div class="col-sm-7">
				                                <?= $formCita->field($modelFarmaco, "[{$i}]prescripcion",['wrapperOptions' =>false])->dropDownList(['0' => 'NO','1' => 'SI'])->label(false) ?>
				                            </div>
				                            <div class="col-sm-3">
				                             <button type="button" class="remove-item2 btn btn-danger btn-xs" >-</button>
				                            </div>
				                     </div><!-- .row -->
				                        
				                    </div>
				            <?php endforeach; ?>
				            </div>
				       	 </div>
			    	</div><!-- .panel -->
	     			<?php DynamicFormWidget::end(); 
		     	?>
	     		</div>
	     	<?php }
	 	     	?>
	     		<div class="jumbotron">
	     			<div class="box_header">Hábitos de vida</div>
	     			<table>
	     				<tr>
	     					<td >
	     						<?=  $formCita->field($modelCita->habitos, 'lugar_consumo')->textarea(['rows' => '6','cols'=>'70'])->label(null,['class'=>'col-sm-7']);?>
	     					</td>
	     				</tr>
	     				<tr>
			     				<td><?=$formCita->field($modelCita->habitos, 'compania')->radioList(array('1'=>'Si','0'=>'No'))->label(null,['class'=>'col-sm-7']);?></td>
			     			</tr>
	     				<tr>
	     					<td >
	     						<?=  $formCita->field($modelCita->habitos, 'quien_prepara')->textInput(['style'=>'width:300px;'])->label(null,['class'=>'col-sm-7']);?>
	     					</td>
	     				</tr>
	     				<tr>
	     					<td>
	     						<?=  $formCita->field($modelCita->habitos, 'licor')->textInput(['style'=>'width:250px;'])	?>
	     					</td>
	     				</tr>
	     				<tr>
	     					<td>
	     						<?=  $formCita->field($modelCita->habitos, 'cigarrillo')->textInput(['style'=>'width:250px;']);?>
	     					</td>
	     				</tr>
	     				<tr>
	     					<td >
	     						<?=  $formCita->field($modelCita->habitos, 'actividad_fisica')->textarea(['rows' => '6','cols'=>'70'])->label(null,['class'=>'col-sm-7']);?>
	     					</td>
	     				</tr>
	     				<tr>
	     					<td >
	     						<?=  $formCita->field($modelCita->habitos, 'deporte')->textInput(['style'=>'width:300px;'])->label(null,['class'=>'col-sm-7']);?>
	     					</td>
	     				</tr>
	     				<tr>
	     					<td >
	     						<?=  $formCita->field($modelCita->habitos, 'habito_intestinal')->textInput(['style'=>'width:300px;'])->label(null,['class'=>'col-sm-7']);?>
	     					</td>
	     				</tr>
	     				<tr>
	     					<td >
	     						<?=  $formCita->field($modelCita->habitos, 'horas_sueno')->textInput(['style'=>'width:50px;'])->label(null,['class'=>'col-sm-7']);?>
	     					</td>
	     				</tr>
	     				<tr>
	     					<td >
	     						<?=  $formCita->field($modelCita->habitos, 'percepcion_sueno')->textInput(['style'=>'width:300px;'])->label(null,['class'=>'col-sm-7']);?>
	     					</td>
	     				</tr>
	     			</table>
	     		</div>
	     		<div class="jumbotron">
	     			<div class="box_header">Estado Psicosocial</div>
	     			<table>
	     				<tr>
	     					<td>
				         		<?= $formCita->field($modelCita->estadoPsicosocial, 'quien_vive')->textInput()->label(null,['class'=>'col-sm-7']); ?>
				         	</td>
				         </tr>
				         <tr>
				         	<td>
	     						<?= $formCita->field($modelCita->estadoPsicosocial, 'relaciones_amistad')->radioList(array('1'=>'Si','0'=>'No'))->label(null,['class'=>'col-sm-7'])?>
	     					</td>
	     				</tr>
	     			</table>
	     		</div>
	     		<div class="jumbotron">
	     			<div class="box_header">Conductas compensatorias asociadas a la enfermedad</div>
	     			<table>
	     				
	     			<?php 
	     				$tiposConducta = ConductasCompensatorias::find()->all();
			   			foreach($tiposConducta as $conducta){
			   				$encontro  = false;
		   					foreach($modelCita->conductaCitas as $conductaCita){
		   						if($conductaCita->conducta_id === $conducta->id){
		   							$encontro  = true;
		   							?>
		   							<tr>
					   					<td width="180px"><label class="negrita"><?= $conducta->conducta?></label></td>
					   					<td class="col-sm-7"><?= $formCita->field($conductaCita,"[{$conducta->id}]posee_conducta")->dropDownList(['0' => 'NO','1' => 'SI'])->label(false);  ?></td>
					   					<td class="col-sm-9" ><?= $formCita->field($conductaCita, "[{$conducta->id}]frecuencia")->textInput(['style'=>'width:150px'])->label(null);?></td>
					   				</tr>
		   							<?php 
		   						}
		   					}
		   					if(!$encontro){
			   					$modelConducta = new ConductaCita;
				   				?>
				   				<tr>
				   					<td width="180px"><label class="negrita"><?= $conducta->conducta?></label></td>
				   					<td class="col-sm-7"><?= $formCita->field($modelConducta,"[{$conducta->id}]posee_conducta")->dropDownList(['0' => 'NO','1' => 'SI'])->label(false);  ?></td>
				   					<td class="col-sm-9" ><?= $formCita->field($modelConducta, "[{$conducta->id}]frecuencia")->textInput(['style'=>'width:150px'])->label(null);?></td>
				   				</tr>	
				   				<?php 
		   					}
			   			}?>
	     			</table>
	     		</div>
	     		
	     		<div class="jumbotron">
	     			<div class="box_header">Concepto y Diagnostico Nutricional</div>
	     			<div class="row">
	    				<?php 
	    				echo $formCita->field($modelCita, 'diagnostico')->textarea(['rows' => '6','cols'=>'70']);
	    				?>
		    		</div>
		    		<div class="row right">
		    			<?= Html::submitButton('Guardar', ['class' => 'btn btn-primary btn_query', 'name' => 'save-button']) ?>
		    		</div>
		    		
	     		</div>
	     		
	     		<?php
	     		 ActiveForm::end(); 
	     ?>
	    <script type="text/javascript">

function send()
 {
   
   var data=$("#formCita").serialize();


  $.ajax({
   type: 'POST',
   url: '<?php echo Yii::$app->urlManager->createUrl("cita/autosave"); ?>',
   data:data,
	success:function(response){
                //alert(response); 
              },
    error: function(data) { // if error occured
         alert("Error almacenando informacion");
         alert(data);
    },

  dataType:'html'
  });

}

</script>
	
	<div class="body-content">
	
	       
	
	</div>