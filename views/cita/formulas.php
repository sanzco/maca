<?php
use yii\helpers\Html;

$this->RegisterJs ("
    $('document').ready(function(){
        $('#".Html::getInputId($modelAntropo, 'peso')."').on('change',function(){ 
                     var total = this.value;
                     var talla = $('#".Html::getInputId($modelAntropo, 'talla')."').val();
                     var grasa = $('#".Html::getInputId($modelAntropo, 'grasa_actual')."').val();
                     var imc = (total)/(talla * talla);
                     var pgraso = (grasa*total)/100;
                     var plgraso = total - pgraso;
                    $('#".Html::getInputId($modelAntropo, 'imc')."').val(imc.toFixed(2));  
                    $('#".Html::getInputId($modelAntropo, 'pgraso')."').val(pgraso.toFixed(2)); 
                     $('#".Html::getInputId($modelAntropo, 'plgraso')."').val(plgraso.toFixed(2)); 
                 });
    });

    ", 3);
    
     $this->RegisterJs ("
    $('document').ready(function(){
        $('#".Html::getInputId($modelAntropo, 'talla')."').on('change',function(){ 
                     var talla = this.value;
                     var peso = $('#".Html::getInputId($modelAntropo, 'peso')."').val();
                     var imc = (peso)/(talla * talla);
                    $('#".Html::getInputId($modelAntropo, 'imc')."').val(imc.toFixed(2));
                 });
    });
    ", 3);
     
     $this->RegisterJs ("
    $('document').ready(function(){
        $('#".Html::getInputId($modelAntropo, 'cintura')."').on('change',function(){ 
                     var cintura = this.value;
                     var cadera = $('#".Html::getInputId($modelAntropo, 'cadera')."').val();
                     var relacion = (cintura)/(cadera);
                    $('#interpretacioncircunferencia-1-valor').val(relacion.toFixed(2));
                     var genero = $('#".Html::getInputId($modelPersona, 'genero')."').val();
                     var interpretacion = '';
                     //alert(relacion);
                     if(genero == 'F'){
                     	if(relacion > 0.85){
                     		interpretacion = 'Riesgo Alto';
                     	}else{
                     		interpretacion = '';
                     	}
                     }else{
                     	if(relacion > 0.9){
                     		interpretacion = 'Riesgo Alto';
                     	}else{
                     		interpretacion = '';
                     	}
                     }
                    $('#interpretacioncircunferencia-1-interpretacion').val(interpretacion);
                    
                 });
    });
    ", 3);
     
     $this->RegisterJs ("
    $('document').ready(function(){
        $('#".Html::getInputId($modelAntropo, 'cadera')."').on('change',function(){ 
                     var cadera = this.value;
                     var cintura = $('#".Html::getInputId($modelAntropo, 'cintura')."').val();
                     var relacion = (cintura)/(cadera);
                    $('#interpretacioncircunferencia-1-valor').val(relacion.toFixed(2));
                    var genero = $('#".Html::getInputId($modelPersona, 'genero')."').val();
                    var interpretacion = '';
                     //alert(relacion);
                     if(genero == 'F'){
                     	if(relacion > 0.85){
                     		interpretacion = 'Riesgo Alto';
                     	}
                     }else{
                     	if(relacion > 0.9){
                     		interpretacion = 'Riesgo Alto';
                     	}
                     }
                    $('#interpretacioncircunferencia-1-interpretacion').val(interpretacion);
                    
                 });
    });
    ", 3);
     
     
     $this->RegisterJs ("
    $('document').ready(function(){
        $('#".Html::getInputId($modelAntropo, 'talla')."').on('change',function(){ 
                     var talla = this.value;
                     var carpo = $('#".Html::getInputId($modelAntropo, 'carpo')."').val();
                     var relacion = ((talla)*100)/(carpo);
                     var genero = $('#".Html::getInputId($modelPersona, 'genero')."').val();
                     var edad = $('#".Html::getInputId($modelPersona, 'edad')."').val();
                     var interpretacion = '';
                     var factor = '';
                     var peso_ideal = 0;
                     
                     if(genero === 'F'){
                     	if(relacion < 9.9){
                     		interpretacion = 'Grande';
                     		factor = 'G';
                     	}else if(relacion >9.8 && relacion < 11){
                     		interpretacion = 'Mediana';
                     		factor = 'M';
                     	}else if(relacion >10.9){
                     		interpretacion = 'Pequeña';
                     		factor = 'P';
                     	}
                     }else{
                     //alert('por masculino');
                     	if(relacion < 9.6){
                     		interpretacion = 'Grande';
                     		factor = 'G';
                     	}else if(relacion >9.6 && relacion < 10.5){
                     		interpretacion = 'Mediana';
                     		factor = 'M';
                     	}else if(relacion >10.5){
                     		interpretacion = 'Pequeña';
                     		factor = 'P';
                     	}
                     }
                     //alert('genero:'+genero+' factor'+factor + ' relacion'+relacion);
                     
                     var factorValor = 0;
                     if(edad<65){
                     	if(factor =='P'){
                     		factorValor = 20;
                     	}else if(factor =='M'){
                     		factorValor = 22.5;
                     	}else if(factor =='G'){
                     		factorValor =25;
                     	}
                     }else{
                     	if(factor =='P'){
                     		factorValor = 22;
                     	}else if(factor =='M'){
                     		factorValor = 25;
                     	}else if(factor =='G'){
                     		factorValor =27;
                     	}
                     }
                     
                    peso_ideal = (talla*talla)*factorValor;
                     
                    $('#interpretacioncircunferencia-2-valor').val(relacion.toFixed(2));
                    $('#interpretacioncircunferencia-2-interpretacion').val(interpretacion);
                     $('#".Html::getInputId($modelAntropo, 'peso_ideal_estructura_osea')."').val(peso_ideal.toFixed(2)); 
                 });
    });
    ", 3); 
     
    $this->RegisterJs ("
    $('document').ready(function(){
        $('#".Html::getInputId($modelAntropo, 'carpo')."').on('change',function(){ 
                     var carpo = this.value;
                     var talla = $('#".Html::getInputId($modelAntropo, 'talla')."').val();
                     var relacion = ((talla)*100)/(carpo);
                     var genero = $('#".Html::getInputId($modelPersona, 'genero')."').val();
                     var edad = $('#".Html::getInputId($modelPersona, 'edad')."').val();
                     var interpretacion = '';
                     //alert(relacion);
                      if(genero === 'F'){
                     	if(relacion < 9.9){
                     		interpretacion = 'Grande';
                     		factor = 'G';
                     	}else if(relacion >9.8 && relacion < 11){
                     		interpretacion = 'Mediana';
                     		factor = 'M';
                     	}else if(relacion >10.9){
                     		interpretacion = 'Pequeña';
                     		factor = 'P';
                     	}
                     }else{
                     	//alert('por masculino');
                     	if(relacion < 9.6){
                     		interpretacion = 'Grande';
                     		factor = 'G';
                     	}else if(relacion >9.6 && relacion < 10.5){
                     		interpretacion = 'Mediana';
                     		factor = 'M';
                     	}else if(relacion >10.5){
                     		interpretacion = 'Pequeña';
                     		factor = 'P';
                     	}
                     }
                     //alert('genero:'+genero+' factor'+factor + ' relacion'+relacion);
                     
                     var factorValor = 0;
                     if(edad<65){
                     	if(factor =='P'){
                     		factorValor = 20;
                     	}else if(factor =='M'){
                     		factorValor = 22.5;
                     	}else if(factor =='G'){
                     		factorValor =25;
                     	}
                     }else{
                     	if(factor =='P'){
                     		factorValor = 22;
                     	}else if(factor =='M'){
                     		factorValor = 25;
                     	}else if(factor =='G'){
                     		factorValor =27;
                     	}
                     }
                     
                    peso_ideal = (talla*talla)*factorValor;
                     //  alert('peso'+peso_ideal+' factor:'+factorValor);
                    $('#interpretacioncircunferencia-2-valor').val(relacion.toFixed(2));
                    $('#interpretacioncircunferencia-2-interpretacion').val(interpretacion);
                    $('#".Html::getInputId($modelAntropo, 'peso_ideal_estructura_osea')."').val(peso_ideal.toFixed(2)); 
                 
                 });
    });
    ", 3);  
     
    
    $this->RegisterJs ("
    $('document').ready(function(){
        $('#".Html::getInputId($modelAntropo, 'grasa_actual')."').on('change',function(){ 
                     var grasa_actual = this.value;
                     var peso = $('#".Html::getInputId($modelAntropo, 'peso')."').val();
                     var pgraso = (peso * grasa_actual)/100;
                      var plgraso = peso - pgraso;
                    $('#".Html::getInputId($modelAntropo, 'pgraso')."').val(pgraso.toFixed(2));   
                      $('#".Html::getInputId($modelAntropo, 'plgraso')."').val(plgraso.toFixed(2)); 
                 });
    });

    ", 3);
    
    $this->RegisterJs ("
    $('document').ready(function(){
        $('#interpretacioncircunferencia-3-valor').on('change',function(){
                     var pantorrilla = this.value;
                     var edad = $('#".Html::getInputId($modelPersona, 'edad')."').val();
                     var interpretacion  = '';
					 if(edad>60){ 
					 	if(pantorrilla>30){
					 		interpretacion = 'Normal';
					 	}else{
					 		interpretacion = 'Depleción';
					 	}
					 }
					 $('#interpretacioncircunferencia-3-interpretacion').val(interpretacion);
                 });
    });

    ", 3);

    $this->RegisterJs ("
    $('document').ready(function(){
        $('#".Html::getInputId($modelAntropo, 'grasa_actual')."').on('change',function(){ 
                     var grasa_actual = this.value;
                     var peso = $('#".Html::getInputId($modelAntropo, 'peso')."').val();
                     var pgraso = (peso * grasa_actual)/100;
                      var plgraso = peso - pgraso;
                    $('#".Html::getInputId($modelAntropo, 'pgraso')."').val(pgraso.toFixed(2));   
                      $('#".Html::getInputId($modelAntropo, 'plgraso')."').val(plgraso.toFixed(2)); 
                 });
    });

    ", 3);
    
     $this->RegisterJs ("
    $('document').ready(function(){
        $('#interpretacioncircunferencia-6-valor').on('change',function(){
                     var cintura = this.value;
                     var genero = $('#".Html::getInputId($modelPersona, 'genero')."').val();
                     var interpretacion  = '';
					 if(genero == 'F'){
					 	if(cintura>79){
					 		interpretacion = 'Riego de complicaciones';
					 	}
					 }else{
						if(cintura>89){
					 		interpretacion = 'Riego de complicaciones';
					 	}
					 }
					 $('#interpretacioncircunferencia-6-interpretacion').val(interpretacion);
                 });
    });
     ", 3);
     
     
      $this->RegisterJs ("
    $('document').ready(function(){
        $('#interpretacioncircunferencia-4-valor').on('change',function(){
                     var cbrazo = this.value;
                     var triceps = $('#".Html::getInputId($modelAntropo, 'triceps')."').val();
                     var valor  =cbrazo-(3.1416-triceps) ;
					  $('#interpretacioncircunferencia-5-valor').val(valor.toFixed(2));
                 });
    });
     ", 3);
     
     
      
      $this->RegisterJs ("
    $('document').ready(function(){
        $('#".Html::getInputId($modelAntropo, 'triceps')."').on('change',function(){ 
                     var triceps = this.value;
                     var cbrazo = $('#interpretacioncircunferencia-4-valor').val();
                      var valor  =cbrazo-(3.1416-triceps) ;
                     $('#interpretacioncircunferencia-5-valor').val(valor.toFixed(2));
                 });
    });

    ", 3);
      
  ?>