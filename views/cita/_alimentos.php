<?php

use yii\helpers\Html;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\RecordatorioPreparacion;

if(empty($modelsAlimento)){
	$modelsAlimento = [new RecordatorioPreparacion];
}
?>

<?php DynamicFormWidget::begin([
    'widgetContainer' => 'dynamicform_inner',
    'widgetBody' => '.container-rooms',
    'widgetItem' => '.room-item',
    'limit' => 20,
    'min' => 1,
    'insertButton' => '.add-alimento',
    'deleteButton' => '.remove-room',
    'model' => $modelsAlimento[0],
    'formId' => 'formCita',
    'formFields' => [
        'alimento',
		'cantidad'
    ],
]); ?>
<table>
<tr><td>
<table class="table">

    <tbody class="container-rooms">
    <?php foreach ($modelsAlimento as $indexAlimento => $modelAlimento): ?>
        <tr class="room-item">
            <td class="vcenter">
                <?php
                    // necessary for update action.
                    if (! $modelAlimento->isNewRecord) {
                        echo Html::activeHiddenInput($modelAlimento, "[{$indexPreparacion}][{$indexAlimento}]recordatorio_id");
                    }
                ?>
                <?= $form->field($modelAlimento, "[{$indexPreparacion}][{$indexAlimento}]alimento",['wrapperOptions' =>false])->label(false)->textInput(['maxlength' => true]) ?>
            </td>
            <td>
            	 <?= $form->field($modelAlimento, "[{$indexPreparacion}][{$indexAlimento}]cantidad",['wrapperOptions' =>false])->label(false)->textInput(['style'=>'width:50px']) ?>
           
            </td>
            <td class="text-center vcenter" style="padding-right: 0px">
                <button type="button" class="remove-room btn btn-danger btn-xs">-</button>
            </td>
        </tr>
     <?php endforeach; ?>
    </tbody>
</table>
</td>
<td>

                <button type="button" class="add-alimento btn btn-success btn-xs">+</button>
</td>
</tr>
</table>
<?php DynamicFormWidget::end(); ?>