	<?php
	
	/* @var $this yii\web\View */
	use yii\helpers\Html;
	use yii\bootstrap\ActiveForm;
	use app\models\TipoAntecedente;
	use app\models\Antecedentes;
	$this->title = 'MACA';
	?>
	
	     	<div class="jumbotron">
	     	<div class="box_header">Información Cita</div>
	     		<div class="row">
		     		<div class="col-sm-3"><label class="col-sm-3 negrita">Paciente: </label></div>
		     		<div class="col-sm-6"><label class="col-sm-6"><?php echo $modelCita->persona->nombre.' '.$modelCita->persona->apellido?></label></div>
		     		<div class="col-sm-3"><label class="col-sm-3 negrita">Ocupación: </label></div>
		     		<div class="col-sm-6"><label class="col-sm-6"><?php echo $modelCita->persona->ocupacion?></label></div>
		     		
	     		</div>
	     		<div class="row">
		     		<div class="col-sm-3"><label class="col-sm-3 negrita">Telefono: </label></div>
		     		<div class="col-sm-6"><label class="col-sm-6"><?php echo $modelCita->persona->telefono?></label></div>
		     		<div class="col-sm-3"><label class="col-sm-3 negrita">Edad: </label></div>
		     		<div class="col-sm-3"><label class="col-sm-6"><?php echo $modelCita->persona->edad?> años</label></div>
		     		<div class="col-sm-3"><label class="col-sm-3 negrita">&nbsp; </label></div>
	     		</div>
	     		<div class="row">
	     			<div class="col-sm-3"><label class="col-sm-3 negrita">Tipo de Consulta</label></div>
		     		<div class="col-sm-6"><label class="col-sm-6"></label><?php if(count($modelCita->persona->citas)>1){ echo 'Control';  }else{echo 'Primera Vez';} ?></div>
		     		<div class="col-sm-3"><label class="col-sm-3 negrita">No Historia Clínica</label></div>
		     		<div class="col-sm-6"><label class="col-sm-6"><?= $modelCita->persona->historia_clinica?></label></div>
	     		</div>
	     		<div class="row"><span style="width: 4px">&nbsp;</span></div>
	     	</div>
	     	<div class="row"><span style="width: 4px">&nbsp;</span></div>
	     	
	     	<?php 
	     	$formCita = ActiveForm::begin([
	     			'id'=>'cita',
		         'action' =>['iniciar'],
		        'layout' => 'horizontal',
		        'fieldConfig' => [
			        'options' => [
			            'tag' => false,
			        ],
			    ],
			    'class' => 'form-horizontal',
		        
		    ]);
		    ?>
		    <div class="jumbotron">
		   		<div class="box_header">Antecedentes</div>
		   	<?php 
		    $tipoAntecedentes = TipoAntecedente::find()->all();
		    foreach($tipoAntecedentes as $tipoAntecedente){
		    	if($modelCita->persona->antecedentes!= null && !empty($modelCita->persona->antecedentes)){
		    		$existAntecedente = false;
		    		foreach($modelCita->persona->antecedentes as $antecedente){
		    			if($antecedente->tipo_antecedente_id === $tipoAntecedente->id){
		    				$existAntecedente = true;
		    				$modelAntecente = new Antecedentes();
		    				$modelAntecente->tipo_antecedente_id = $tipoAntecedente->id;
		    				$modelAntecente->persona_id =  $modelCita->persona->id;
		    				$modelAntecente->descripcion = $antecedente->descripcion;
		    				?>
		    				<div class="row">
		    				<?php 
		    				echo $formCita->field($modelAntecente, 'descripcion')->textarea(['rows' => '6','cols'=>'70','name'=>'Antecedente[	'.$modelAntecente->tipo_antecedente_id.']','id'=>'Antecedente['.$modelAntecente->tipo_antecedente_id.']'])->label($tipoAntecedente->nombre);
		    				?>
		    				</div>
		    				<?php 
		    			}
		    				
		    		}
		    		if(!$existAntecedente){
		    			$modelAntecente = new Antecedentes();
	    				$modelAntecente->tipo_antecedente_id = $tipoAntecedente->id;
	    				$modelAntecente->persona_id =  $modelCita->persona->id;
	    				?>
		    				<div class="row">
		    				<?php 
	    				echo $formCita->field($modelAntecente, 'descripcion')->textarea(['rows' => '6', 'cols'=>'70','name'=>'Antecedente['.$modelAntecente->tipo_antecedente_id.']','id'=>'Antecedente['.$modelAntecente->tipo_antecedente_id.']'])->label($tipoAntecedente->nombre);
	    			?>
		    				</div>
		    				<?php	
		    		}
		    	}else{
		    		?> 
		    		
		    		<?php 
	    				$modelAntecente = new Antecedentes();
	    				$modelAntecente->tipo_antecedente_id = $tipoAntecedente->id;
	    				$modelAntecente->persona_id =  $modelCita->persona->id;
	    				?>
		    				<div class="row">
		    				<?php 
	    				echo $formCita->field($modelAntecente, 'descripcion')->textarea(['rows' => '6', 'cols'=>'70','name'=>'Antecedente['.$modelAntecente->tipo_antecedente_id.']','id'=>'Antecedente['.$modelAntecente->tipo_antecedente_id.']'])->label($tipoAntecedente->nombre);
	    			?>
		    				</div>
		    				<?php	
		    	
		    	}
		    }
		    ?>
		   	
		   	</div>
		    
	     	<div class="jumbotron">
		   		<div class="box_header">Motivo de Consulta</div>
			    <div class="row">
			    	<?= $formCita->field($modelCita, 'motivo')->textarea(['rows' => '6','cols'=>'70'])?>
			    </div>
			     <div class="row">
			    	<?= $formCita->field($modelCita, 'objetivos_nutricionales')->textarea(['rows' => '6','cols'=>'70'])?>
			    </div>	
			    <div style="text-align: right;" >	
			     <?= Html::submitButton('Iniciar Cita', ['class' => 'btn btn-primary btn_query', 'name' => 'query-button']) ?>
			 	</div>
			 	<div class="error" style="width: 290px;float:left">
			 		<?= $formCita->field($modelCita,'cita_id')->hiddenInput()->label(false);?>
			   			<?= $formCita->errorSummary($modelCita); ?>
			   			</div>
	   		 </div>
	   		 
	    <?php
	     		
	     		 ActiveForm::end(); 

	     ?>
