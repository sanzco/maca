	<?php
	
	/* @var $this yii\web\View */
	use yii\helpers\Html;
	use app\models\TipoIdentificacion;
	use yii\bootstrap\ActiveForm;
	use yii\helpers\ArrayHelper;
	use yii\widgets\ListView;
	use yii\data\ArrayDataProvider;

	$this->title = 'MACA';
	?>
	
	     	<div class="jumbotron">
		   		<div class="box_header">Paciente</div>
	     		<div class="row">
		     		<div class="col-sm-3"><label class="col-sm-3 negrita">Paciente: </label></div>
		     		<div class="col-sm-6"><label class="col-sm-6"><?php echo $modelPersona->nombre.' '.$modelPersona->apellido?></label></div>
		     		<div class="col-sm-3"><label class="col-sm-3 negrita">Ocupación: </label></div>
		     		<div class="col-sm-6"><label class="col-sm-6"><?php echo $modelPersona->ocupacion?></label></div>
	     		</div>
	     		<div class="row">
		     		<div class="col-sm-3"><label class="col-sm-3 negrita">Telefono: </label></div>
		     		<div class="col-sm-6"><label class="col-sm-6"><?php echo $modelPersona->telefono?></label></div>
		     		<div class="col-sm-3"><label class="col-sm-3 negrita">Edad: </label></div>
		     		<div class="col-sm-6"><label class="col-sm-6"><?php echo $modelPersona->edad?> años</label></div>
	     		</div>
	     		<div class="row">
	     			<span style="width: 4px">&nbsp;</span>
	     		</div>
	     	</div>
	     	<span style="width: 4px">&nbsp;</span>
	     	<div class="jumbotron">
		   		<div class="box_header">Historial de Citas</div>
	     	
	     	<?php 
	     	$formCita = ActiveForm::begin([
	     			'id'=>'cita',
		         'action' =>['review'],
		        'layout' => 'horizontal',
		        'fieldConfig' => [
			        'options' => [
			            'tag' => false,
			        ],
			    ],
			    'class' => 'form-horizontal',
		        
		    ]);
		    ?>

	<?php 		    
		    if(isset($modelPersona)){
		    	if(empty($modelPersona->citas)){
		    		echo 'La pesona no tiene citas asociadas';
		   		}else{
		   			
		    		?>
		    <div class="row">
		    	<div class="col-sm-6"><label class="negrita">id</label></div>
		    	<div class="col-sm-6"><label class="negrita">Paciente</label></div>
		    	<div class="col-sm-6"><label class="negrita">Fecha y hora</label></div>
		    	<div class="col-sm-6"><label class="negrita">Detalles</label></div>
		    	
		    </div>
		    		
		    		<?php
		    	}
		    }
		?>    
	     		<?php
	     		 ActiveForm::end(); 

	     ?>
	    </div>
	
	<div class="body-content">
	
	       
	
	</div>