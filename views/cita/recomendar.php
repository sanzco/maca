	<?php
	
	/* @var $this yii\web\View */
	use yii\helpers\Html;
	use yii\bootstrap\ActiveForm;
	use app\models\TipoAntecedente;
	use app\models\Antecedentes;
	$this->title = 'MACA';
	
	$this->RegisterJs ("
    $('document').ready(function(){
        $('#".Html::getInputId($modelCita, 'plan_nutricional')."').keypress(function(event){ 
        			if (event.which == 13) {
				      //event.preventDefault();
				      var s = $(this).val();
				      $(this).val(s+'<br>');
				   }
				   });
                    
    });

    ", 3);
	
	$this->RegisterJs ("
    $('document').ready(function(){
        $('#".Html::getInputId($modelCita, 'recomendaciones')."').keypress(function(event){ 
        			if (event.which == 13) {
				      //event.preventDefault();
				      var s = $(this).val();
				      $(this).val(s+'<br>');
				   }
				   });
                    
    });

    ", 3);
	?>
	
	     	<div class="jumbotron">
	     	<div class="box_header">Información Cita -><?=$modelCita->cita_id ?></div>
	     		<div class="row">
		     		<div class="col-sm-3"><label class="col-sm-3 negrita">Paciente: </label></div>
		     		<div class="col-sm-6"><label class="col-sm-6"><?php echo $modelCita->persona->nombre.' '.$modelCita->persona->apellido?></label></div>
		     		<div class="col-sm-3"><label class="col-sm-3 negrita">Ocupación: </label></div>
		     		<div class="col-sm-6"><label class="col-sm-6"><?php echo $modelCita->persona->ocupacion?></label></div>
		     		
	     		</div>
	     		<div class="row">
		     		<div class="col-sm-3"><label class="col-sm-3 negrita">Telefono: </label></div>
		     		<div class="col-sm-6"><label class="col-sm-6"><?php echo $modelCita->persona->telefono?></label></div>
		     		<div class="col-sm-3"><label class="col-sm-3 negrita">Edad: </label></div>
		     		<div class="col-sm-3"><label class="col-sm-6"><?php echo $modelCita->persona->edad?> años</label></div>
		     		<div class="col-sm-3"><label class="col-sm-3 negrita">Genero: </label></div>
		     		<div class="col-sm-3"><label class="col-sm-6"><?php echo $modelCita->persona->genero?></label></div>
		     		
	     		</div>
	     		<div class="row">
	     			<div class="col-sm-3"><label class="col-sm-3 negrita">Fecha de Consulta</label></div>
		     		<div class="col-sm-6"><label class="col-sm-6"><?=$modelCita->fecha_hora?></label></div>
		     		<div class="col-sm-3"><label class="col-sm-3 negrita">No Historia Clínica</label></div>
		     		<div class="col-sm-6"><label class="col-sm-6"><?= $modelCita->persona->historia_clinica?></label></div>
	     		</div>
	     		 	
	     		<div class="row"><span style="width: 4px">&nbsp;</span></div>
	     	</div>
	     	<div class="row"><span style="width: 4px">&nbsp;</span></div>
	     	
	     	<?php 
	     	$formCita = ActiveForm::begin([
	     			'id'=>'cita',
		         'action' =>['savereco'],
		        'layout' => 'horizontal',
		        'fieldConfig' => [
			        'options' => [
			            'tag' => false,
			        ],
			    ],
			    'class' => 'form-horizontal',
		        
		    ]);
		    $peso = 0;
		    $talla = 0;
		    $edad = $modelCita->persona->edad;
		    $pgraso = 0;
		    $pajustado = 0;
		    if(!empty($modelCita->antropometria)){
		    	$peso = $modelCita->antropometria->peso;
		    	$talla = $modelCita->antropometria->talla;
		    	$pgraso = $modelCita->antropometria->pgraso;
		    	$pesoIdeal = ($modelCita->antropometria->plgraso/(1-($modelCita->antropometria->grasa_objetivo/100)));
		    	$pajustado = $pesoIdeal+(($peso-$pesoIdeal)*0.25);
		    }
		    
		    ?>
		    <div class="jumbotron">
		   		<div class="box_header">GET</div>
		   		<table >
				   		<tr>
				   			<td >
				   				<label class="col-sm-2">Edad en años</label>
				   				<label class="col-sm-2"><?= $edad?></label>
				   			</td>
				   		
				   			<td>
				   				<label class="col-sm-1">Peso (kg)</label>	
				   				<label class="col-sm-2"><?=$peso?></label>
				   			</td>
					   		<td>
					   			<label class="col-sm-1">Talla (m)</label>
					   			<label class="col-sm-2"><?=$talla?></label>
					   		</td>
					   		<td></td>
				   		</tr>
				   		<tr>
				   			<td colspan="2" >
		    					<?= $formCita->field($modelCita, 'f_actividad')->textInput(['style'=>'width:50px'])->label(null,['class'=>'col-sm-3']);?>
				   				<?= Html::submitButton('Calcular', ['class' => 'btn btn-primary btn_query', 'name' => 'refresh-button']) ?>
				   				<?= $formCita->field($modelCita,'cita_id')->hiddenInput()->label(false);?>
				   				<div class="error" style="width: 290px;float:left">
						   			<?= $formCita->errorSummary($modelCita); ?>
						   			</div>
				   			</td>
				   		</tr>
		   		</table>
		   		<?php 
		   			$mlg = ($peso-$pgraso);
		   			$tmb_cunn = (500+(22*$mlg));
		   			$get_cunn = $tmb_cunn * $modelCita->f_actividad;
		   			
		   			$tmb_mc =(370+(21.6*$mlg));
		   			$get_mc = $tmb_mc * $modelCita->f_actividad;
		   			
		   			$tmb_harris = 0;
		   			if($modelCita->persona->edad>10){
			   			if($modelCita->persona->genero ==='M'){
			   				$tmb_harris = (66.47+(13.75*$peso)+(5*($talla*100))-(6.76*$edad));
			   			}else{
			   				$tmb_harris = (655.09+(9.56*$peso)+(1.85*($talla*100))-(4.68*$edad));
			   			}
		   			}
		   			$get_harris = $tmb_harris * $modelCita->f_actividad;
		   			
		   			$tmb_mifflin = 0;
		   			if($modelCita->persona->genero ==='M'){
		   				$tmb_mifflin = (10*$peso)+(6.25*($talla*100))-(5*$edad)+5;
		   			}else{
		   				$tmb_mifflin = (10*$peso)+(6.25*($talla*100))-(5*$edad)-161;
		   			}
		   			$get_mifflin = $tmb_mifflin  * $modelCita->f_actividad;

		   			$get_scho = 0;
		   			if($modelCita->persona->genero ==='M'){
			   			if($edad >=0 && $edad <3){
			   				$get_scho =((16.7*$peso)+(15.174*$pajustado)-617.6);
			   			}else if($edad >=3 && $edad <10){
			   				$get_scho =((19.59*$peso)+(1.303*$pajustado)+414.9);
			   			}else if($edad >=10 && $edad <18){
			   				$get_scho =((16.25*$peso)+(1.372*$pajustado)+515.5);
			   			}else if($edad >18){
			   				$get_scho =((15.057*$peso)+(1.004*$pajustado)+705.8);
			   			}
		   			}else{
		   				if($edad >=0 && $edad <3){
			   				$get_scho =((16.252*$peso)+(10.232*$pajustado)-413.5);
			   			}else if($edad >=3 && $edad <10){
			   				$get_scho =((16.969*$peso)+(1.618*$pajustado)+371.2);
			   			}else if($edad >=10 && $edad <18){
			   				$get_scho =((8.365*$peso)+(4.65*$pajustado)+200);
			   			}else if($edad >18){
			   				$get_scho =((13.623*$peso)+(23.8*$pajustado)+98.2);
			   			}
		   			}
		   			
		   			$get_dietz = 0;
		   			if($modelCita->persona->genero ==='M'){
		   				if($edad >=10 && $edad <=18){
		   					$get_dietz = (16.6*$peso)+(77*$pajustado)+572;
		   				}
		   			}else{
		   				if($edad >=10 && $edad <=18){
		   					$get_dietz = (7.4*$peso)+(482*$pajustado)+217;
		   				}
		   			}
		   			
		   			$get_dri = 0;
		   			if($modelCita->persona->genero ==='M'){
		   				if($edad >=3 && $edad <=8){
		   					$get_dri =(88.5-(61.9*$edad)+$modelCita->f_actividad*((26.7*$peso)+(903*$talla))+20);
		   				}else if($edad >=9 && $edad <=18){
		   					$get_dri =(135.3-(30.8*$edad)+$modelCita->f_actividad*((10*$peso)+(934*$talla))+20);
		   				}else if($edad >18){
		   					$get_dri =(662-(9.53*$edad)+$modelCita->f_actividad*((15.91*$peso)+(539.6*$talla)));
		   				}
		   			}else{
		   				if($edad >=3 && $edad <=8){
		   					$get_dri =(88.5-(61.9*$edad)+$modelCita->f_actividad*((26.7*$peso)+(903*$talla))+25);
		   				}else if($edad >=9 && $edad <=18){
		   					$get_dri =(135.3-(30.8*$edad)+$modelCita->f_actividad*((10*$peso)+(934*$talla))+25);
		   				}else if($edad >18){
		   					$get_dri = (354-(6.91*$edad)+$modelCita->f_actividad*((9.36*$peso)+(726*$talla)));
		   				}
		   			}
		   		?>
		    	<table class="table-bordered">
		    	<tr>
		    		<td class="table-bordered" style="width: 50%"><div class="box_header">GASTO ENERGETICO CUNNIGHAM - DEPORTISTAS</div></td>
		    		<td class="table-bordered"><div class="box_header">GASTO ENERGETICO McAERDLE, KATCH et al </div></td>
		    	</tr>
		    	<tr>
		    		<td class="table-bordered">MLG: <?=round($mlg,0,PHP_ROUND_HALF_UP)?></td>
		    		<td class="table-bordered">MLG: <?=round($mlg,0,PHP_ROUND_HALF_UP)?></td>
		    	</tr>
		    	<tr>
		    		<td class="table-bordered">TMB: <?=round($tmb_cunn,0,PHP_ROUND_HALF_UP)?></td>
		    		<td class="table-bordered">TMB: <?=round($tmb_mc,0,PHP_ROUND_HALF_UP)?></td>
		    	</tr>
		    	<tr>
		    		<td class="table-bordered">GET: <?=round($get_cunn,0,PHP_ROUND_HALF_UP)?></td>
		    		<td class="table-bordered">GET: <?=round($get_mc,0,PHP_ROUND_HALF_UP)?></td>
		    	</tr>
		    	</table>
		    	
		    	<table class="table-bordered">
		    	<tr>
		    		<td class="table-bordered" style="width: 50%"><div class="box_header">GASTO ENERGETICO HARRIS - BENEDICT (TALLA EN CM)</div></td>
		    		<td class="table-bordered"><div class="box_header">GASTO ENERGETICO MIFFLIN - ST JEOR </div></td>
		    	</tr>
		    	<tr>
		    		<td class="table-bordered">TMB: <?=round($tmb_harris,0,PHP_ROUND_HALF_UP)?></td>
		    		<td class="table-bordered">TMB: <?=round($tmb_mifflin,0,PHP_ROUND_HALF_UP)?></td>
		    	</tr>
		    	<tr>
		    		<td class="table-bordered">GET: <?=round($get_harris,0,PHP_ROUND_HALF_UP)?></td>
		    		<td class="table-bordered">GET: <?=round($get_mifflin,0,PHP_ROUND_HALF_UP)?></td>
		    	</tr>
		    	</table>
		    
		    	<table class="table-bordered">
			    	<tr>
			    		<td class="table-bordered" style="width: 50%"><div class="box_header">GASTO ENERGETICO SCHOFIELD</div></td>
			    		<td class="table-bordered"><div class="box_header">GASTO ENERGETICO DIETZ (SOBREPESO Y OBESIDAD)</div></td>
			    	</tr>
			    	
			    	<tr>
			    		<td class="table-bordered">GET: <?=round($get_scho,0,PHP_ROUND_HALF_UP)?></td>
			    		<td class="table-bordered">GET: <?=round($get_dietz,0,PHP_ROUND_HALF_UP)?></td>
			    	</tr>
		    	</table>
		    	
		    	<table class="table-bordered">
			    	<tr>
			    		<td colspan="3" class="table-bordered"><div class="box_header">GASTO ENERGETICO DRI'S</div></td>
			    	</tr>
			    	<tr>
			    		<td class="table-bordered">GET:</td><td class="table-bordered" colspan="2"><?=round($get_dri,0,PHP_ROUND_HALF_UP)?></td>
			    	</tr>
			    	<?php if($modelCita->persona->genero ==='F'){?>
			    	<tr>
			    		<td rowspan="3" class="table-bordered">EMBARAZO</td>
			    		<td class="table-bordered">1er TRIMESTRE</td>
			    		<td class="table-bordered"> <?=round($get_dri,0,PHP_ROUND_HALF_UP)?></td>
			    	</tr>
			    	<tr>
			    		<td class="table-bordered">2do TRIMESTRE</td>
			    		<td class="table-bordered"> <?=round($get_dri+340,0,PHP_ROUND_HALF_UP)?></td>
			    	</tr>
			    	<tr>
			    		<td class="table-bordered">3er TRIMESTRE</td>
			    		<td class="table-bordered"> <?=round($get_dri+452,0,PHP_ROUND_HALF_UP)?></td>
			    	</tr>
			    	<tr>
			    		<td rowspan="2" class="table-bordered">Lactancia</td>
			    		<td class="table-bordered">0 - 6 M POST</td>
			    		<td class="table-bordered"> <?=round($get_dri+500-170,0,PHP_ROUND_HALF_UP)?></td>
			    	</tr>
			    	<tr>
			    		<td class="table-bordered">7 - 12 M POST</td>
			    		<td class="table-bordered"> <?=round($get_dri+400,0,PHP_ROUND_HALF_UP)?></td>
			    	</tr>
			    	<?php }?>
		    	</table>
		    	
		    
	     	<div class="jumbotron">
		   		<div class="box_header">Prescripcion dietaria</div>
			    <div class="row">
			    	<?= $formCita->field($modelCita, 'plan_nutricional')->textarea(['rows' => '6','cols'=>'70'])?>
			    </div>
			     <div class="row">
			    	<?= $formCita->field($modelCita, 'recomendaciones')->textarea(['rows' => '6','cols'=>'70'])?>
			    </div>	
			    <div style="text-align: right;" >	
			    <?= Html::submitButton('Volver', ['class' => 'btn btn-primary btn_query', 'name' => 'back-button']) ?>
			     <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary btn_query', 'name' => 'save-button']) ?>
			 	 <?= Html::submitButton('Imprimir', ['class' => 'btn btn-primary btn_query', 'name' => 'print-button']) ?>
			 	
			 	</div>
			 	<div class="error" style="width: 290px;float:left">
			 		<?= $formCita->field($modelCita,'cita_id')->hiddenInput()->label(false);?>
			   			<?= $formCita->errorSummary($modelCita); ?>
			   			</div>
	   		 </div>
	   		 
	    <?php
	     		
	     		 ActiveForm::end(); 

	     ?>
