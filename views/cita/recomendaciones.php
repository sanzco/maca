<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use app\models\Persona;
use yii\bootstrap\ActiveForm;
?>
    
 <div class="usuario-index">

	     	<div class="box_header " style="font-size: 1.2rem">Recomendaciones Pendientes</div>
	     	<div class="card-panel">
	<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' =>"Nombre",
            'attribute' => 'nombre',
            
        ],
        'apellido',
        [
        	'class' => 'yii\grid\DataColumn',
        	'attribute' =>'fecha_hora',
        	'label'=>"Fecha Cita",
        	'headerOptions' => ['class' => 'text-center'],
        ],
        [
        	'class' => 'yii\grid\ActionColumn', 
        	'template' => '{update}',
        	'buttons' => [ 
    			'update' => function ($url, $model) {
		                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
		                            'title' => 'Recomendar',
		                ]);
            	},
    		],
    		'urlCreator' => function ($action, $model, $key, $index) {
		     if ($action === 'update') {
		                $url ='index.php?r=cita/recomendar&id='.$model['cita_id'];
		                return $url;
		     }
	 		}
       	],
        
    ],
    'tableOptions' => ['class' => 'table  table-bordered table-hover'],
    
    
]);
?>
	</div>
	</div>
   		
   			
