<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Persona;
use app\models\PersonaForm;
use yii\bootstrap\ActiveForm;
?>
<style type="text/css">
.bgimg {
    float:right;
    text-align: right; 
}
.jumbotron {
    border: 1px solid #ddd !important;
}

</style>
<div class="bgimg" style="padding-top: 0px; width: 81px; height: 88px">
	<img src="<?=$this->theme->baseUrl ?>/images/flor_maca.png" width="81px" height="84px">
</div>
<div style="text-align: center;float: right;">
	MACA<br>
	Centro Integral de Nutrición
</div>

    <div class="box_header " style="font-size: 1.2rem; width: 100%; color: white">Detalle del Examen</div>
	
   		
   			<?php 
   				if(isset($modelCita)){
   					if(isset($modelCita->persona)){
   						?>
   						<div class="card-panel" style="display: block;">
   						<table class="table table-bordered">
	   						<tr>
		   						<td class="col-sm-3"><label class="negrita">Paciente: </label></td>
		   						<td class="col-sm-6"><label><?=$modelCita->persona->nombre?>&nbsp;<?=$modelCita->persona->apellido?></label></td>
		   						<td class="col-sm-3"><label class="negrita">Ocupación: </label></td>
		   						<td class="col-sm-6"><label ><?=$modelCita->persona->ocupacion?></label></td>
		   					</tr>
		   					<tr>
		   						<td class="col-sm-3"><label class=" negrita">Telefono: </label></td>
		   						<td class="col-sm-6"><label><?=$modelCita->persona->telefono?></label></td>
		   						<td class="col-sm-3"><label class="negrita">Edad: </label></td>
		   						<td class="col-sm-6"><label><?=$modelCita->persona->edad?></label></td>
		   					</tr>
		   					<tr>
		   						<td class="col-sm-3"><label class="negrita"> Fecha</label></td>
		   						<td class="col-sm-6"><label><?=$modelCita->fecha_hora?></label></td>
		   						<td class="col-sm-3"><label class="negrita"> Historia Clinica</label></td>
		   						<td class="col-sm-6"><label><?=$modelCita->persona->historia_clinica?></label></td>
		   					</tr>
	   					</table>
	   					</div>
	   					<?php 
   						} 
	   						if(!empty($modelCita->motivo) || !empty($modelCita->objetivos_nutricionales)){
	   					?>
	   					<div class="box_header " style="font-size: 1.2rem; width: 100%">Motivos / objetivos</div>
	   					<div class="card-panel" style="display: block;">
		   					<table class="table table-bordered">
		   						<?php if(!empty($modelCita->motivo)){?>
			   						<tr> 
			   							<td class="table table-bordered "><label class="negrita">Motivo de consulta: </label></td>
				   						<td class="table table-bordered"><label ><?=$modelCita->motivo?></label></td>
			   						</tr>
			   						<?php }
			   							if(!empty($modelCita->objetivos_nutricionales)){?>
			   						<tr>
			   							<td class="table table-bordered"><label class="negrita">Objetivos nutricionales: </label></td>
				   						<td class="table table-bordered"><label ><?=$modelCita->objetivos_nutricionales?></label></td>
			   						</tr>
			   						<?php }?>
			   					
		   					</table>
	   					</div>
	   				<?php } 
	   					if(isset($modelCita->antropometria)){
	   				?>
	   				<div class="box_header " style="font-size: 1.2rem; width: 100%; color: white">Datos Antropométricos</div>
	   					<div class="card-panel" style="display: block;">
   							<div class="row">
		   						<div class="col-sm-3"><label class="col-sm-3 negrita">Peso: </label></div>
		   						<div class="col-sm-3"><label class="col-sm-3"><?=$modelCita->antropometria->peso?> Kg</label></div>
		   						<div class="col-sm-3"><label class="col-sm-3 negrita">Peso Graso: </label></div>
		   						<div class="col-sm-3"><label class="col-sm-3"><?=$modelCita->antropometria->pgraso?>Kg</label></div>
		   						<div class="col-sm-3"><label class="col-sm-3 negrita">C. Cintura</label></div>
		   						<div class="col-sm-3"><label class="col-sm-3"><?=$modelCita->antropometria->cintura?> cm</label></div>
	   						</div>
	   						<div class="row">
		   						<div class="col-sm-3"><label class="col-sm-3 negrita">Talla: </label></div>
		   						<div class="col-sm-3"><label class="col-sm-3"><?=$modelCita->antropometria->talla?> m</label></div>
		   						<div class="col-sm-3"><label class="col-sm-3 negrita">Peso libre de grasa: </label></div>
		   						<div class="col-sm-3"><label class="col-sm-3"><?=$modelCita->antropometria->plgraso?> Kg</label></div>
		   						<div class="col-sm-3"><label class="col-sm-3 negrita">C. Cadera</label></div>
		   						<div class="col-sm-3"><label class="col-sm-3"><?=$modelCita->antropometria->cadera?> cm</label></div>
	   						</div>
	   						<div class="row">
		   						<div class="col-sm-3"><label class="col-sm-3 negrita">IMC: </label></div>
		   						<div class="col-sm-3"><label class="col-sm-3"><?=$modelCita->antropometria->imc?>(kg/mt2)</label></div>
		   						<div class="col-sm-3"><label class="col-sm-3 negrita">Grasa Objetivo:</label></div>
		   						<div class="col-sm-3"><label class="col-sm-3"><?=$modelCita->antropometria->grasa_objetivo?>%</label></div>
		   						<div class="col-sm-3"><label class="col-sm-3 negrita">C. Carpo</label></div>
		   						<div class="col-sm-3"><label class="col-sm-3"><?=$modelCita->antropometria->carpo?> cm</label></div>
	   						</div>
	   						<div class="row">
		   						<div class="col-sm-3"><label class="col-sm-3 negrita">Grasa Actual: </label></div>
		   						<div class="col-sm-3"><label class="col-sm-3"><?=$modelCita->antropometria->grasa_actual?> %</label></div>
		   						<div class="col-sm-3"><label class="col-sm-3 negrita">Triceps: </label></div>
		   						<div class="col-sm-3"><label class="col-sm-3"><?=$modelCita->antropometria->triceps?> mm</label></div>
		   						<div class="col-sm-3"><label class="col-sm-3 negrita">Peso Ideal estructura ósea: </label></div>
		   						<div class="col-sm-3"><label class="col-sm-3"><?=$modelCita->antropometria->peso_ideal_estructura_osea?> Kg</label></div>
		   					</div>
		   					<div class="row">
		   						<div class="col-sm-3"><label class="col-sm-3 negrita">Interpretación IMC </label></div>
		   						<div class="col-sm-3"><label class="col-sm-3"><?=$modelCita->antropometria->interpretacion_imc?></label></div>
		   						
		   					</div>
	   					</div>
	   					<?php 
	   					}
	   						if(isset($modelCita->interpretacionCircunferencias) && !empty($modelCita->interpretacionCircunferencias)){?>
	   						<div class="card-panel" style="display: block;">
		   							<table>
	   							<tr>
	   								
		   							<th class="table table-bordered"><label class="col-sm-6 negrita">Circunferencia</label></th>
		   							<th class="table table-bordered"><label class="col-sm-6 negrita">Valor</label></th>
		   							<th class="table table-bordered"><label class="col-sm-6 negrita">Interpretación</label></th>
		   						</tr>
	   						<?php foreach($modelCita->interpretacionCircunferencias as $inter){?>
	   							<tr>
		   							<td class="table table-bordered"><label ><?=$inter->circunferencia->nombre?></label></td>
		   							<td class="table table-bordered"><label ><?=$inter->valor?></label></td>
		   							<td class="table table-bordered"><label ><?=$inter->interpretacion?></label></td>
		   						</tr>
	   					<?php }?>
	   							</table>
	   						</div>
	   					<?php 
	   						}

	   						if(isset($modelCita->examenes) && !empty($modelCita->examenes)){
	   						?>
	   						<div class="box_header " style="font-size: 1.2rem; width: 100%;color: white">Examenes de Laboratorio</div>
		   						<div class="card-panel" style="display: block;">
		   							<table>
		   								<tr>
				   							<th class=" table-bordered"><label class=" negrita">Parametro</label></th>
				   							<th class=" table-bordered"><label class=" negrita">Valor</label></th>
				   							<th class=" table-bordered"><label class=" negrita">Valor Referencia</label></th>
				   							<th class=" table-bordered"><label class=" negrita">Interpretación</label></th>
				   							<th class=" table-bordered"><label class=" negrita">fecha</label></th>
				   						</tr>
			   						<?php foreach($modelCita->examenes as $examen){
			   							if(!empty($examen->examen)){
			   							?>
		   								<tr>
				   							<td class="table-bordered"><label ><?=$examen->examen->nombre?></label></td>
				   							<td class="table-bordered"><label ><?=$examen->valor?></label></td>
				   							<td class="table-bordered"><label ><?=$examen->examen->valor_referencia_superior?>-<?=$examen->examen->valor_referencia_bajo?></label></td>
				   							<td class="table-bordered"><label ><?=$examen->interpretacion?></label></td>
				   							<td class="table-bordered"> <label ><?=$examen->fecha?></label></td>
			   						</tr>
	   							<?php }
			   						}?>
			   						</table>
	   							</div>
	   						<?php }
	   						
	   						if(isset($modelCita->examen_fisico) && !empty($modelCita->examen_fisico)){
	   						?>
	   						
	   						<div class="box_header " style="font-size: 1.2rem; width: 100%; color: white">Examen Fisico</div>
		   						<div class="card-panel" style="display: block;">
		   							<div class="row">
		   								<div class="col-sm-10"><label><?=$modelCita->examen_fisico?></label></div>
		   							</div>
		   						</div>
	   					
			   				<?php 	
			   				}
			   				
			   				if(isset($modelCita->recordatorios) && !empty($modelCita->recordatorios)){
			   					?>
			   					<div class="box_header " style="font-size: 1.2rem; width: 100%; color: white">Recordatorio 24 horas</div>
		   						<div class="card-panel" style="display: block;">
		   							<table class="table table-bordered">
		   								<thead>
			   								<tr>
					   							<th class="table-bordered center"><label class="negrita">Hora</label></th>
					   							<th class="table-bordered center"><label class="negrita">Tiempo</label></th>
					   							<th class="table-bordered center"><label class="negrita">Preparación</label></th>
					   							<th class="table-bordered center"><label class="negrita">Alimentos / Cantidad</label></th>
				   							</tr>
				   						</thead>
				   						<tbody class="container-items">
			   					<?php 
			   					foreach($modelCita->recordatorios as $recordatorio){
			   						if(!empty($recordatorio->tiempo)){
			   					?>
			   						<tr>
			   							<td class="table-bordered center"><label ><?=$recordatorio->hora?></label></td>
			   							<td class="table-bordered center"><label ><?=$recordatorio->tiempo->nombre?></label></td>
			   							<td class="table-bordered center"><label ><?=$recordatorio->preparacion?></label></td>
			   							<td class="table-bordered center">
			   								<label>
				   								<table class="table-bordered center" width="100%">
				   								<?php
				   									foreach ($recordatorio->recordatorioPreparaciones as $preparaciones){
				   										?>
				   										<tr>
				   											<td class="table-bordered center"><?=$preparaciones->alimento?></td>
				   											<td class="table-bordered center" nowrap="nowrap"><?=$preparaciones->cantidad?></td>
				   										</tr>
				   										<?php 
				   									} 
				   								?>
				   								</table>
			   								</label>
			   							</td>
			   						</tr>
			   					<?php 
			   						}
			   					}
			   					?>
			   					</tbody>
			   					</table>
			   					</div>
			   					<?php 
			   				}
			   				?>
			   				<div class="row">&nbsp;</div>
			   				<?php 
			   				if(isset($modelCita->menus) && !empty($modelCita->menus)){
			   					?>
		   						<div class="card-panel" style="display: block;">
		   							<table>
		   							<tr>
			   							<th class="table-bordered center"><label  class="negrita">Alimento</label></th>
			   							<th class="table-bordered center"><label  class="negrita">Frecuencia</label></th>
			   							<th class="table-bordered center"><label  class="negrita"></label></th>
			   							<th class="table-bordered center"><label  class="negrita">Observación</label></th>
			   						</tr>
			   					<?php 
			   					foreach($modelCita->menus as $menu){
			   					?>
			   						<tr>
			   							<td class="table-bordered center"><?=$menu->alimento->nombre?></td>
			   							<td class="table-bordered center"><?=$menu->frecuencia->frecuencia?></td>
			   							<td class="table-bordered center"><?=$menu->valor?></td>
			   							<td class="table-bordered center"><?=$menu->observacion?></td>
			   						</tr>
			   					<?php 
			   					}
			   					?>
			   					</table>
			   					<?php 
			   				}?>
			   			
			   				<?php 
   							if(isset($modelCita->anamnesis) && !empty($modelCita->anamnesis) ){
	   							if(isset($modelCita->anamnesis->analisis_ingesta)&& !empty($modelCita->anamnesis->analisis_ingesta))
	   							{
   							?>
	   						
	   						<div class="box_header " style="font-size: 1.2rem; width: 100%; color: white">Analisis de ingesta</div>
		   						<div class="card-panel" style="display: block;">
		   							<div class="row">
		   								<div><label ><?=$modelCita->anamnesis->analisis_ingesta?></label></div>
		   							</div>
		   						</div>
		   					<?php }?>
		   					
	   						<div class="box_header " style="font-size: 1.2rem; width: 100%; color: white">Datos Anamnesis alimentaria</div>
		   						<div class="card-panel" style="display: block;">
		   							<div class="row">
		   								<div class="col-sm-6"><label class=" negrita">Apetito: </label></div>
		   								<div class="col-sm-6"><label><?=(!empty($modelCita->anamnesis->apetito))?$modelCita->anamnesis->apetito->nombre:''?></label></div>
		   							</div>
		   							<div class="row">
		   								<div class="col-sm-6"><label class=" negrita">Cambios en la ingesta: </label></div>
		   								<div class="col-sm-6"><label ><?=($modelCita->anamnesis->cambios_ingesta==1)?'SI':'NO'?></label></div>
		   							</div>
		   								
		   								<?php
		   								if(isset($modelCita->anamnesis->ingestaAnamneses) &&  !empty($modelCita->anamnesis->ingestaAnamneses))
		   								{
		   									?>
		   									<div class="row">
		   										<div class="col-sm-6"><label class=" negrita">Tipo de Cambio</label></div>
		   									</div>
		   									<?php 
		   									foreach($modelCita->anamnesis->ingestaAnamneses as $anamnesis )
		   									{?>
		   										<div class="row">
		   											<div class="col-sm-6">&nbsp;</div>
			   										<div class="col-sm-6"><label class=" negrita"><?=$anamnesis->tipoIngesta->nombre?></label></div>
			   										<div class="col-sm-6"><label ><?=$anamnesis->texto_descriptivo?></label></div>
		   										</div>
		   							<?php  	} 
		   							
   										}
		   								?>
		   								<div class="row">
			   								<div class="col-sm-6"><label class=" negrita">Alimentos Preferidos: </label></div>
			   								<div class="col-sm-7"><label ><?=$modelCita->anamnesis->alimentos_preferidos?></label></div>
			   							</div>
		   								<div class="row">
			   								<div class="col-sm-6"><label class=" negrita">Alimentos Rechazados: </label></div>
			   								<div class="col-sm-7"><label ><?=$modelCita->anamnesis->alimentos_rechazados?></label></div>
			   							</div>
			   							<div class="row">
			   								<div class="col-sm-6"><label class=" negrita">Intolerancias/Alergias alimentarias: </label></div>
			   								<div class="col-sm-7"><label ><?=$modelCita->anamnesis->intolerancias?></label></div>
			   							</div>
			   							<div class="row">
			   								<div class="col-sm-6"><label class=" negrita">Percepción de volumen consumido:</label></div>
			   								<div class="col-sm-7"><label ><?=(!empty($modelCita->anamnesis->volumenConsumido))?$modelCita->anamnesis->volumenConsumido->nombre:''?></label></div>
			   							</div>
			   							<div class="row">
			   								<div class="col-sm-6"><label class=" negrita">Tiempo Empleado para el consumo de alimentos:</label></div>
			   								<div class="col-sm-7"><label ><?=$modelCita->anamnesis->tiempo_empleado?></label></div>
			   							</div>
			   							<div class="row">
			   								<div class="col-sm-6"><label class=" negrita">Utiliza el salero en la mesa?:</label></div>
			   								<div class="col-sm-7"><label ><?=($modelCita->anamnesis->uso_sal === 1)?"SI":"NO"?></label></div>
			   							</div>
		   							</div>
			   				<?php 	
			   				}//Fin Anamnesis
			   				if(isset($modelCita->farmacos) && !empty($modelCita->farmacos) )
			   				{?>
			   					<div class="box_header " style="font-size: 1.2rem; width: 100%; color: white">Farmacos</div>
		   						<div class="card-panel" style="display: block;">
		   						<table class="table-bordered center">
		   							<thead>
		   								<tr>
				   							<th class="table-bordered center"><label negrita">Fármaco</label></th>
				   							<th class="table-bordered center"><label negrita">Dosis/Frecuencia</label></th>
				   							<th class="table-bordered center"><label negrita">Objetivo</label></th>
				   							<th class="table-bordered center"><label negrita">Prescripción</label></th>
				   							
			   							</tr>
			   						</thead>
			   						<tbody>
			   					<?php 
			   					foreach($modelCita->farmacos as $farmaco)
			   					{?>
				   						<tr>
			   								<td class="table-bordered center"><label ><?=$farmaco->nombre?></label></td>
			   								<td class="table-bordered center"><label ><?=$farmaco->dosis_frecuencia?></label></td>
			   								<td class="table-bordered center"><label><?=$farmaco->objetivo?></label></td>
			   								<td class="table-bordered center"><label><?=($farmaco->prescripcion === '1')?"SI":"NO"?></label></td>
			   							</tr>
			   					<?php 
			   					}
	   						?>
	   								</tbody>
	   							</table>
	   						</div>
	   						<?php 
			   				}
	   						?>
	   						
	   						<?php 
	   						if(isset($modelCita->habitos) && !empty($modelCita->habitos))
	   						{
	   							?>
	   							<div class="box_header " style="font-size: 1.2rem; width: 100%; color: white">Habitos</div>
		   						<div class="card-panel" style="display: block;">
		   							<table>
		   								<tr>
		   									<td><label class=" negrita">Lugar donde consume los alimentos</label></td>
			   								<td><label ><?=$modelCita->habitos->lugar_consumo?></label></td>
			   							</tr>
			   							<tr>
		   									<td><label class=" negrita">Tiene compañia durante las ingestas?</label></td>
			   								<td><label ><?=($modelCita->habitos->compania === 1)?"SI":"NO"?></label></td>
			   							</tr>
			   							<tr>
		   									<td><label class=" negrita">Quién prepara los alimentos para Ud:</label></td>
			   								<td><label ><?=$modelCita->habitos->quien_prepara?></label></td>
			   							</tr>
			   							<tr>
		   									<td><label class=" negrita">Consumo de licor</label></td>
			   								<td><label ><?=$modelCita->habitos->licor?></label></td>
			   							</tr>
			   							<tr>
		   									<td><label class=" negrita">Fuma</label></td>
			   								<td><label ><?=$modelCita->habitos->cigarrillo?></label></td>
			   							</tr>
			   							<tr>
			   								<td><label class=" negrita">Actividad física o ejercicio</label></div>
			   								<td><label ><?=$modelCita->habitos->actividad_fisica?></label></div>
				   						</tr>
				   						<tr>
				   							<td><label class=" negrita">Deporte</label></td>
				   							<td><label ><?=$modelCita->habitos->deporte?></label></td>
				   						</tr>
				   						<tr>
				   							<td><label class=" negrita">Hábito intestinal</label></td>
				   							<td><label ><?=$modelCita->habitos->habito_intestinal?></label></td>
				   						</tr>
				   							<tr>
				   							<td><label class=" negrita">Horas de sueño</label></td>
				   							<td><label ><?=$modelCita->habitos->horas_sueno?></label></td>
				   						</tr>
				   						<tr>
				   							<td><label class=" negrita">Percepción del sueño</label></td>
				   							<td><label ><?=$modelCita->habitos->percepcion_sueno?></label></td>
				   						</tr>
		   							</table>
		   						</div>
	   							<?php 
	   						}
	   						if(isset($modelCita->estadoPsicosocial))
	   						{
	   							?>
	   							<div class="box_header " style="font-size: 1.2rem; width: 100%; color: white">Estado PsicoSocial</div>
		   						<div class="card-panel" style="display: block;">
		   						<table>
			   							<tr>
				   							<td style="width: 40%"><label class=" negrita">Quien vive con usted</label></td>
				   							<td><label ><?=$modelCita->estadoPsicosocial->quien_vive?></label></td>
			   							</tr>
			   							<tr>
				   							<td ><label class="negrita">Tiene relaciones de amistad?</label></td>
				   							<td><label ><?=($modelCita->estadoPsicosocial->relaciones_amistad==1)?'SI':'NO'?></label></td>
			   							</tr>
			   						</table>
		   						</div>
		   						<?php 
	   						}
	   						?>
	   						<?php 
	   						if(isset($modelCita->conductaCitas))
	   						{
	   							?>
	   							<div class="box_header " style="font-size: 1.2rem; width: 100%; color: white">Conductas compensatorias asociadas a la enfermedad</div>
		   						<div class="card-panel" style="display: block;">
		   							<?php 
		   								foreach($modelCita->conductaCitas as $conducta)
		   								{
		   									Yii::trace($conducta->conducta->conducta);
		   									
			   								?>
			   								<div class="row">
					   							<div class="col-sm-6"><label class="col-sm-6 negrita"><?=$conducta->conducta->conducta?></label></div>
					   							<div class="col-sm-6"><label class="col-sm-6 "><?=($conducta->posee_conducta===1)?'SI':'NO'?></label></div>
					   							<div class="col-sm-6"><label class="col-sm-6 negrita">Frecuencia</label></div>
					   							<div class="col-sm-6"><label class="col-sm-6 "><?=$conducta->frecuencia?></label></div>
				   							</div>
			   								<?php 
		   								}
		   							?>
		   						</div>
	   							<?php 
	   						}
	   						?>
	   						<div class="box_header " style="font-size: 1.2rem; width: 100%; color: white">Concepto y Diagnostico Nutricional</div>
		   						<div class="card-panel" style="display: block;">
			   						<table>
		   							<tr><td><label><?=$modelCita->diagnostico?></label></td></tr>
		   						</table>
		   						</div>
		   						
		   						<div class="row">
		   							<div class="col-sm-7">
		   								<label class="col-sm-7 ">Nutricionista Dietista:<br> 
		   								<?= $modelCita->personaMedico->nombre?>&nbsp;<?= $modelCita->personaMedico->apellido?>
		   								<br>
		   									<?=$modelCita->personaMedico->tarjeta_profesional?>
		   								</label>
		   							</div>
		   						</div>
		   						
	   						<?php 
		   			
   		}?>
