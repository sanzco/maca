<?php
use yii\helpers\Html;

$this->RegisterJs ("
    $('document').ready(function(){
        $('#".Html::getInputId($modelCita->antropometria, 'peso')."').on('change',function(){ 
                     var total = this.value;
                     var talla = $('#".Html::getInputId($modelCita->antropometria, 'talla')."').val();
                     var grasa = $('#".Html::getInputId($modelCita->antropometria, 'grasa_actual')."').val();
                     var imc = (total)/(talla * talla);
                     var pgraso = (grasa*total)/100;
                     var plgraso = total - pgraso;
                     $('#sendButton').trigger('click');
                    $('#".Html::getInputId($modelCita->antropometria, 'imc')."').val(imc.toFixed(2));  
                    $('#".Html::getInputId($modelCita->antropometria, 'pgraso')."').val(pgraso.toFixed(2)); 
                     $('#".Html::getInputId($modelCita->antropometria, 'plgraso')."').val(plgraso.toFixed(2)); 
                 });
    });

    ", 3);
    
     $this->RegisterJs ("
    $('document').ready(function(){
        $('#".Html::getInputId($modelCita->antropometria, 'talla')."').on('change',function(){ 
                     var talla = this.value;
                     var peso = $('#".Html::getInputId($modelCita->antropometria, 'peso')."').val();
                     var cintura = $('#".Html::getInputId($modelCita->antropometria, 'cintura')."').val();
                     var imc = (peso)/(talla * talla);
                     var relacionCinturaTalla = cintura/(talla*100);
                     if(relacionCinturaTalla>0.5){
                     	$('#interpretacioncircunferencia-7-interpretacion').val('Riesgo Alto');
                     }else{
                     	$('#interpretacioncircunferencia-7-interpretacion').val('');
                     }
                    $('#".Html::getInputId($modelCita->antropometria, 'imc')."').val(imc.toFixed(2));
                    $('#interpretacioncircunferencia-7-valor').val(relacionCinturaTalla.toFixed(2));
                 });
    });
    ", 3);
     
     $this->RegisterJs ("
    $('document').ready(function(){
        $('#".Html::getInputId($modelCita->antropometria, 'cintura')."').on('change',function(){ 
                     var cintura = this.value;
                     var cadera = $('#".Html::getInputId($modelCita->antropometria, 'cadera')."').val();
                     var talla = $('#".Html::getInputId($modelCita->antropometria, 'talla')."').val();
                     var relacion = (cintura)/(cadera);
                     var relacionCinturaTalla = cintura/(talla*100);
                     if(relacionCinturaTalla>0.5){
                     	$('#interpretacioncircunferencia-7-interpretacion').val('Riesgo Alto');
                     }else{
                     	$('#interpretacioncircunferencia-7-interpretacion').val('');
                     }
                    $('#interpretacioncircunferencia-1-valor').val(relacion.toFixed(2));
                     $('#interpretacioncircunferencia-7-valor').val(relacionCinturaTalla.toFixed(2));
                     var genero = $('#".Html::getInputId($modelCita->persona, 'genero')."').val();
                     var interpretacion = '';
                     //alert(relacion);
                     if(genero == 'F'){
                     	if(relacion > 0.85){
                     		interpretacion = 'Riesgo Alto';
                     	}else{
                     		interpretacion = 'Sin Riesgo alto';
                     	}
                     }else{
                     	if(relacion > 0.9){
                     		interpretacion = 'Riesgo Alto';
                     	}else{
                     		interpretacion = 'Sin riesgo alto';
                     	}
                     }
                    $('#interpretacioncircunferencia-1-interpretacion').val(interpretacion);
                    
                 });
    });
    ", 3);
     
     $this->RegisterJs ("
    $('document').ready(function(){
        $('#".Html::getInputId($modelCita->antropometria, 'cadera')."').on('change',function(){ 
                     var cadera = this.value;
                     var cintura = $('#".Html::getInputId($modelCita->antropometria, 'cintura')."').val();
                     var relacion = (cintura)/(cadera);
                     $('#sendButton').trigger('click');
                    $('#interpretacioncircunferencia-1-valor').val(relacion.toFixed(2));
                    var genero = $('#".Html::getInputId($modelCita->persona, 'genero')."').val();
                    var interpretacion = '';
                     //alert(relacion);
                     if(genero == 'F'){
                     	if(relacion > 0.85){
                     		interpretacion = 'Riesgo Alto';
                     	}else{
                     		interpretacion = 'Sin Riesgo alto';
                     	}
                     }else{
                     	if(relacion > 0.9){
                     		interpretacion = 'Riesgo Alto';
                     	}else{
                     		interpretacion = 'Sin Riesgo alto';
                     	}
                     }
                    $('#interpretacioncircunferencia-1-interpretacion').val(interpretacion);
                    
                 });
    });
    ", 3);
     
     
     $this->RegisterJs ("
    $('document').ready(function(){
        $('#".Html::getInputId($modelCita->antropometria, 'talla')."').on('change',function(){ 
                     var talla = this.value;
                     var carpo = $('#".Html::getInputId($modelCita->antropometria, 'carpo')."').val();
                     var relacion = ((talla)*100)/(carpo);
                     var genero = $('#".Html::getInputId($modelCita->persona, 'genero')."').val();
                     var edad = $('#".Html::getInputId($modelCita->persona, 'edad')."').val();
                     var interpretacion = '';
                     var factor = '';
                     var peso_ideal = 0;
                     
                     if(genero === 'F'){
                     	if(relacion < 9.9){
                     		interpretacion = 'Grande';
                     		factor = 'G';
                     	}else if(relacion >9.8 && relacion < 11){
                     		interpretacion = 'Mediana';
                     		factor = 'M';
                     	}else if(relacion >10.9){
                     		interpretacion = 'Pequeña';
                     		factor = 'P';
                     	}
                     }else{
                     	//alert('por masculino');
                     	if(relacion < 9.6){
                     		interpretacion = 'Grande';
                     		factor = 'G';
                     	}else if(relacion >9.6 && relacion < 10.5){
                     		interpretacion = 'Mediana';
                     		factor = 'M';
                     	}else if(relacion >10.5){
                     		interpretacion = 'Pequeña';
                     		factor = 'P';
                     	}
                     }
                     //alert('genero:'+genero+' factor'+factor + ' relacion'+relacion +' edad:'+edad);
                     
                     var factorValor = 0;
                     if(edad<65){
                     	if(factor =='P'){
                     		factorValor = 20;
                     	}else if(factor =='M'){
                     		factorValor = 22.5;
                     	}else if(factor =='G'){
                     		factorValor =25;
                     	}
                     }else{
                     	if(factor =='P'){
                     		factorValor = 22;
                     	}else if(factor =='M'){
                     		factorValor = 25;
                     	}else if(factor =='G'){
                     		factorValor =27;
                     	}
                     }
                    //alert(talla+' factorValor:'+factorValor); 
                    peso_ideal = (talla*talla)*factorValor;
                    //alert('peso_ideal:'+peso_ideal);
                    
                     
                    $('#interpretacioncircunferencia-2-valor').val(relacion.toFixed(2));
                    $('#interpretacioncircunferencia-2-interpretacion').val(interpretacion);
                     $('#".Html::getInputId($modelCita->antropometria, 'peso_ideal_estructura_osea')."').val(peso_ideal.toFixed(2)); 
                 });
    });
    ", 3); 
     
    $this->RegisterJs ("
    $('document').ready(function(){
        $('#".Html::getInputId($modelCita->antropometria, 'carpo')."').on('change',function(){ 
                     var carpo = this.value;
                     var talla = $('#".Html::getInputId($modelCita->antropometria, 'talla')."').val();
                     var relacion = ((talla)*100)/(carpo);
                     var genero = $('#".Html::getInputId($modelCita->persona, 'genero')."').val();
                     var edad = $('#".Html::getInputId($modelCita->persona, 'edad')."').val();
                     var interpretacion = '';
                     //alert(relacion);
                      if(genero === 'F'){
                     	if(relacion < 9.9){
                     		interpretacion = 'Grande';
                     		factor = 'G';
                     	}else if(relacion >9.8 && relacion < 11){
                     		interpretacion = 'Mediana';
                     		factor = 'M';
                     	}else if(relacion >10.9){
                     		interpretacion = 'Pequeña';
                     		factor = 'P';
                     	}
                     }else{
                     	//alert('por masculino');
                     	if(relacion < 9.6){
                     		interpretacion = 'Grande';
                     		factor = 'G';
                     	}else if(relacion >9.6 && relacion < 10.5){
                     		interpretacion = 'Mediana';
                     		factor = 'M';
                     	}else if(relacion >10.5){
                     		interpretacion = 'Pequeña';
                     		factor = 'P';
                     	}
                     }
                     //alert('genero:'+genero+' factor'+factor + ' relacion'+relacion+' edad:'+edad);
                     
                     var factorValor = 0;
                     if(edad<65){
                     	if(factor =='P'){
                     		factorValor = 20;
                     	}else if(factor =='M'){
                     		factorValor = 22.5;
                     	}else if(factor =='G'){
                     		factorValor =25;
                     	}
                     }else{
                     	if(factor =='P'){
                     		factorValor = 22;
                     	}else if(factor =='M'){
                     		factorValor = 25;
                     	}else if(factor =='G'){
                     		factorValor =27;
                     	}
                     }
                     
                    peso_ideal = (talla*talla)*factorValor;
                     //alert('peso'+peso_ideal+' factor:'+factorValor);
                    $('#interpretacioncircunferencia-2-valor').val(relacion.toFixed(2));
                    $('#interpretacioncircunferencia-2-interpretacion').val(interpretacion);
                    $('#".Html::getInputId($modelCita->antropometria, 'peso_ideal_estructura_osea')."').val(peso_ideal.toFixed(2)); 
                 
                 });
    });
    ", 3);  
     
    
    $this->RegisterJs ("
    $('document').ready(function(){
        $('#".Html::getInputId($modelCita->antropometria, 'grasa_actual')."').on('change',function(){ 
                     var grasa_actual = this.value;
                     var peso = $('#".Html::getInputId($modelCita->antropometria, 'peso')."').val();
                     var pgraso = (peso * grasa_actual)/100;
                      var plgraso = peso - pgraso;
                    $('#".Html::getInputId($modelCita->antropometria, 'pgraso')."').val(pgraso.toFixed(2));   
                      $('#".Html::getInputId($modelCita->antropometria, 'plgraso')."').val(plgraso.toFixed(2)); 
                 });
    });

    ", 3);
    
    $this->RegisterJs ("
    $('document').ready(function(){
        $('#interpretacioncircunferencia-3-valor').on('change',function(){
                     var pantorrilla = this.value;
                     var edad = $('#".Html::getInputId($modelCita->persona, 'edad')."').val();
                     var interpretacion  = '';
					 if(edad>60){ 
					 	if(pantorrilla>30){
					 		interpretacion = 'Normal';
					 	}else{
					 		interpretacion = 'Depleción';
					 	}
					 }
					 $('#interpretacioncircunferencia-3-interpretacion').val(interpretacion);
                 });
    });

    ", 3);

    $this->RegisterJs ("
    $('document').ready(function(){
        $('#".Html::getInputId($modelCita->antropometria, 'grasa_actual')."').on('change',function(){ 
                     var grasa_actual = this.value;
                     var peso = $('#".Html::getInputId($modelCita->antropometria, 'peso')."').val();
                     var pgraso = (peso * grasa_actual)/100;
                      var plgraso = peso - pgraso;
                    $('#".Html::getInputId($modelCita->antropometria, 'pgraso')."').val(pgraso.toFixed(2));   
                      $('#".Html::getInputId($modelCita->antropometria, 'plgraso')."').val(plgraso.toFixed(2)); 
                 });
    });

    ", 3);
    
     $this->RegisterJs ("
    $('document').ready(function(){
        $('#interpretacioncircunferencia-6-valor').on('change',function(){
                     var cintura = this.value;
                     var genero = $('#".Html::getInputId($modelCita->persona, 'genero')."').val();
                     var interpretacion  = '';
                    
					 if(genero == 'F'){
					 	if(cintura>80 && cintura<89){
					 		interpretacion = 'Riego Alto';
					 	}
					 	if(cintura>88){
					 		interpretacion = 'Riego Muy Alto';
					 	}
					 }else{
						if(cintura>94 && cintura<103){
					 		interpretacion = 'Riego Alto';
					 	}
					 	if(cintura>102){
					 		interpretacion = 'Riego Muy Alto';
					 	}
					 }
					 $('#interpretacioncircunferencia-6-interpretacion').val(interpretacion);
					 $('#sendButton').trigger('click');
                 });
    });
     ", 3);
     
     
      $this->RegisterJs ("
    $('document').ready(function(){
        $('#interpretacioncircunferencia-4-valor').on('change',function(){
                     var cbrazo = this.value;
                     var triceps = $('#".Html::getInputId($modelCita->antropometria, 'triceps')."').val();
                     var valor  =(cbrazo)-(3.1416*triceps) ;
					  $('#interpretacioncircunferencia-5-valor').val(valor.toFixed(2));
                 });
    });
     ", 3);
     
     
      
      $this->RegisterJs ("
    $('document').ready(function(){
        $('#".Html::getInputId($modelCita->antropometria, 'triceps')."').on('change',function(){ 
                     var triceps = this.value;
                     var cbrazo = $('#interpretacioncircunferencia-4-valor').val();
                      var valor  =(cbrazo)-(3.1416*triceps) ;
                     $('#interpretacioncircunferencia-5-valor').val(valor.toFixed(2));
                 });
    });

    ", 3);
      
       $this->RegisterJs ("
    $('document').ready(function(){
        $('#".Html::getInputId($modelCita, 'diagnostico')."').keypress(function(event){ 
        			if (event.which == 13) {
				      //event.preventDefault();
				      var s = $(this).val();
				      $(this).val(s+'<br>');
				   }
				   });
                    
    });

    ", 3);
       
        $this->RegisterJs ("
    $('document').ready(function(){
        $('#".Html::getInputId($modelCita, 'examen_fisico')."').on('change',function(){ 
        			 $('#sendButton').trigger('click');
				   });
                    
    });

    ", 3);
        
         $this->RegisterJs ("
    $('document').ready(function(){
        $('#".Html::getInputId($modelCita->anamnesis, 'analisis_ingesta')."').on('change',function(){ 
        			 $('#sendButton').trigger('click');
				   });
                    
    });

    ", 3);
        

       
         $this->RegisterJs ("
    $('document').ready(function(){
        $('#".Html::getInputId($modelCita->anamnesis, 'alimentos_preferidos')."').on('change',function(){ 
        			 $('#sendButton').trigger('click');
				   });
                    
    });

    ", 3);    

         
          $this->RegisterJs ("
    $('document').ready(function(){
        $('#".Html::getInputId($modelCita->anamnesis, 'intolerancias')."').on('change',function(){ 
        			 $('#sendButton').trigger('click');
				   });
                    
    });

    ", 3);  

          
      $this->RegisterJs ("
    $('document').ready(function(){
        $('#".Html::getInputId($modelCita->anamnesis, 'tiempo_empleado')."').on('change',function(){ 
        			 $('#sendButton').trigger('click');
				   });
                    
    });

    ", 3);       

       $this->RegisterJs ("
    $('document').ready(function(){
        $('#".Html::getInputId($modelCita->habitos, 'lugar_consumo')."').on('change',function(){ 
        			 $('#sendButton').trigger('click');
				   });
                    
    });

    ", 3);   

       $this->RegisterJs ("
    $('document').ready(function(){
        $('#".Html::getInputId($modelCita->habitos, 'licor')."').on('change',function(){ 
        			 $('#sendButton').trigger('click');
				   });
    });
    ", 3);   

      $this->RegisterJs ("
    $('document').ready(function(){
        $('#".Html::getInputId($modelCita->habitos, 'actividad_fisica')."').on('change',function(){ 
        			 $('#sendButton').trigger('click');
				   });
    });
    ", 3);    
       
       $this->RegisterJs ("
    $('document').ready(function(){
        $('#".Html::getInputId($modelCita->habitos, 'percepcion_sueno')."').on('change',function(){ 
        			 $('#sendButton').trigger('click');
				   });
    });
    ", 3);   
      
       $this->RegisterJs ("
    $('document').ready(function(){
        $('#".Html::getInputId($modelCita->estadoPsicosocial, 'quien_vive')."').on('change',function(){ 
        			 $('#sendButton').trigger('click');
				   });
    });
    ", 3);   
       
  ?>