	<?php
	
	/* @var $this yii\web\View */
	$this->title = 'MACA';
	?>

<style type="text/css">
.bgimg {
    float:right;
    text-align: right; 
}
.jumbotron {
    border: 1px solid #ddd !important;
}

</style>
<div class="bgimg" style="padding-top: 0px; width: 81px; height: 88px">
	<img src="<?=$this->theme->baseUrl ?>/images/flor_maca.png" width="81px" height="84px">
</div>
<div style="text-align: center;float: right;">
	MACA<br>
	Centro Integral de Nutrición
</div>

	
	     	<div class="jumbotron">
	     	<div class="box_header" style="color: white">Información Cita -><?=$modelCita->cita_id ?></div>
	     		<div class="row">
		     		<div class="col-sm-3"><label class="col-sm-3 negrita">Paciente: </label></div>
		     		<div class="col-sm-6"><label class="col-sm-3"><?php echo $modelCita->persona->nombre.' '.$modelCita->persona->apellido?></label></div>
		     		<div class="col-sm-3"><label class="col-sm-3 negrita">Ocupación: </label></div>
		     		<div class="col-sm-6"><label class="col-sm-3"><?php echo $modelCita->persona->ocupacion?></label></div>
		     		
	     		</div>
	     		<div class="row">
		     		<div class="col-sm-3"><label class="col-sm-3 negrita">Telefono: </label></div>
		     		<div class="col-sm-6"><label class="col-sm-3"><?php echo $modelCita->persona->telefono?></label></div>
		     		<div class="col-sm-3"><label class="col-sm-3 negrita">Edad: </label></div>
		     		<div class="col-sm-3"><label class="col-sm-3"><?php echo $modelCita->persona->edad?> años</label></div>
		     		<div class="col-sm-3"><label class="col-sm-3 negrita">Genero: </label></div>
		     		<div class="col-sm-3"><label class="col-sm-3"><?php echo $modelCita->persona->genero?></label></div>
		     		
	     		</div>
	     		<div class="row">
	     			<div class="col-sm-3"><label class="col-sm-3 negrita">Fecha de Consulta</label></div>
		     		<div class="col-sm-6"><label class="col-sm-3"><?=$modelCita->fecha_hora?></label></div>
		     		<div class="col-sm-3"><label class="col-sm-3 negrita">No Historia Clínica</label></div>
		     		<div class="col-sm-6"><label class="col-sm-3"><?= $modelCita->persona->historia_clinica?></label></div>
	     		</div>
	     	</div>		    
	     	<div class="jumbotron">
		   		<div class="box_header" style="color: white">Prescripción dietaria</div>
			    <div class="row">
			    	<div style="padding-left: 3px"><?= $modelCita->plan_nutricional?></div>
			    </div>
			    <div class="box_header" style="color: white">Recomendaciones</div>
			     <div class="row">
			    	<div style="padding-left: 3px"><?=$modelCita->recomendaciones?></div>
			    </div>	
	   		 </div>
	   		
	   		 <div style="padding-top: 50px">
	   		 	Nutricionista Dietista:<br> <?=$modelCita->personaMedico->nombre?>&nbsp;<?=$modelCita->personaMedico->apellido?>
	   		 </div>
