<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ExamenReferencia */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="examen-referencia-form">

    <?php $form = ActiveForm::begin(); ?>

	<div class="row">
	    <?= $form->field($model, 'nombre')->textInput() ?>
	
	    <?= $form->field($model, 'valor_referencia_bajo')->textInput(['maxlength' => true]) ?>
	
	    <?= $form->field($model, 'valor_referencia_superior')->textInput(['maxlength' => true]) ?>

	    <div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Crear') : Yii::t('app', 'Actualizar'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
		<div class="help-block">
		      <?php 
				if($model->hasErrors()){
			  		echo $form->errorSummary($model); 
				}
			  ?>
		</div>
	</div>
    <?php ActiveForm::end(); ?>

</div>
