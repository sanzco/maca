<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ExamenReferencia */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Examen Referencia',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Examen Referencias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
 <div class="box_header " style="font-size: 1.2rem">Actualizar Examenes</div>
	<div class="card-panel">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
