<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Examen Referencias');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="examen-referencia-index">
	<div class="box_header " style="font-size: 1.2rem">Examenes Referencia</div>
	     	<div class="card-panel">
  
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'nombre',
            'valor_referencia_bajo',
            'valor_referencia_superior',
            ['class' => 'yii\grid\ActionColumn','template' => '{update}'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

  <p>
        <?= Html::a(Yii::t('app', 'Crear Examen Referencia'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    </div>
</div>	