<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ExamenReferencia */

$this->title = Yii::t('app', 'Create Examen Referencia');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Examen Referencias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
   <div class="box_header " style="font-size: 1.2rem">Nuevo Examen de referencia</div>
	<div class="card-panel">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
