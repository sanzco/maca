<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Usuario */

$this->title = Yii::t('app', 'Create Usuario');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Usuarios'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="box_header " style="font-size: 1.2rem">Nuevo Usuario</div>
	<div class="card-panel">

	    <?= $this->render('_form', [
	        'model' => $model,
	    	'modelPersona' =>$modelPersona
	    ]) ?>
	</div>
