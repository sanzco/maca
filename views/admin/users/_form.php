<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\TipoIdentificacion;
use app\models\Persona;
use app\models\Perfil;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;


/* @var $this yii\web\View */
/* @var $model app\models\Usuario */
/* @var $form yii\widgets\ActiveForm */
?>


    <?php $form = ActiveForm::begin();
    	
		?>
		<div class="row">
			<table>
				<tr>
					<td>
			   			<?=$form->field($modelPersona, 'nombre')->textInput(['autofocus' => true,'style'=>'width:150px']);?>
			   		</td>	
			   		<td>
			   			<?=$form->field($modelPersona, 'apellido')->textInput(['style'=>'width:150px']);?>
					</td>
				</tr>
				<tr>
					
	        		<td>
		        		<?php 	
			    		$tipoIdItems = ArrayHelper::map(TipoIdentificacion::find()->all(),'id','nombre');
			   			echo $form->field($modelPersona,'tipo_identificacion_id')->dropDownList($tipoIdItems)->label('Tipo Identificacion'); 
			    
			   		?>
	        		</td>
	        		<td>
	        			<?=$form->field($modelPersona, 'identificacion')->textInput(['style'=>'width:100px']);?>
	        		</td>
					<td><?php 	
		   			echo $form->field($modelPersona, 'fecha_nacimiento')->widget(
		   										DatePicker::className(),[
		   											'dateFormat' =>'yyyy-MM-dd',
												    'clientOptions' => [
		   												'yearRange' => '-80:+0',
	           											'changeYear' => true
		   											],
		   											
											])->textInput(['placeholder' =>'Seleccione..' , 'style'=>'width:100px']);
				?>
					</td>
				</tr>
				<tr>
					<td>
						<?=$form->field($modelPersona, 'telefono')->textInput(['style'=>'width:100px']);?>
					</td>
	        		<td><?=$form->field($modelPersona, 'email')->input('email',['style'=>'width:250px']);?>
	        		</td>
	        	</tr>
	        </table>
	        <table>
	        	<tr><td><?= $form->field($model, 'username')->textInput(['style'=>'width:100px']) ?></td></tr>
	        	<tr>
	        		<td> <?= $form->field($model, 'password')->passwordInput(['style'=>'width:150px']) ?></td>
		        	<td> <?= $form->field($model, 'password_repeat')->passwordInput(['style'=>'width:150px']) ?></td>
	        	</tr>
	        	<?php if(!$model->isNewRecord){
	        			/*if($model->estado === 'A'){
	        				$model->estado = 
	        			}*/
	        		?>
	        		<tr>
	        			<td colspan="3"><?=$form->field($model, 'estado')->checkbox(['value'=>'A']);?></td>
	        		</tr><?php 
	        	}?>
	        	
	        </table>
	        <div class="box_header " style="font-size: 1.2rem; width: 100%">Perfiles</div>
	        	<div class="card-panel" style="display: block;">
	        		<?php 
	        			$perfiles = ArrayHelper::map(Perfil::find()->all(),'id','nombre');
	        			$model->tempperfiles = $model->perfiles;
							?>
								<?=$form->field($model, "tempperfiles")->checkboxList($perfiles, ['separator' => '<br>'])->label('');?>
		   			</div>
	        
	        <div class="help-block">
		      <?php 
				if($model->hasErrors()){
			  		echo $form->errorSummary($model); 
				}
			  ?>
			</div>
	        
	        </div>
	        
		   		
		    	
    	

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Crear') : Yii::t('app', 'Actualizar'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

