<?php

use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel app\models\UsuarioQuery */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Usuarios');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuario-index">

	     	<div class="box_header " style="font-size: 1.2rem">Usuarios del Sistema</div>
	     	<div class="card-panel">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
       
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			[
				'attribute'=>'persona.nombre',
				'headerOptions'=>['style'=>'color:#6ccad5']
			],
			[
				'attribute'=>'persona.apellido',
				'headerOptions'=>['style'=>'color:#6ccad5']	
			],
            [
				'attribute'=>'username',
				'headerOptions'=>['style'=>'color:#6ccad5']	
			],
            'estado',
            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}'],
        ],
    ]); ?>
    
    </div>
    <p>
        <?= Html::a(Yii::t('app', 'Nuevo Usuario'), ['/usuario/create'], ['class' => 'btn btn-success']) ?>
    </p>
    </div>
</div>
