<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Usuario */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Usuario',
]) . $model->persona_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Usuarios'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->persona_id, 'url' => ['view', 'id' => $model->persona_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
    <div class="box_header " style="font-size: 1.2rem">Actualizar Usuario</div>
	<div class="card-panel">
    <?= $this->render('_form', [
        'model' => $model,
    	'modelPersona' =>$modelPersona
    ]) ?>

</div>
