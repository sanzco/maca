<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Alimento */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Alimento',
]) . $model->alimento_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Alimentos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->alimento_id, 'url' => ['view', 'id' => $model->alimento_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
 <div class="box_header " style="font-size: 1.2rem">Actualizar Alimento</div>
	<div class="card-panel">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
