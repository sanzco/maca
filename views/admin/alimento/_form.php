<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Alimento */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="alimento-form">

    <?php $form = ActiveForm::begin(); ?>

	<div class="row">
	    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
	
	    <?= $form->field($model, 'en_lista')->checkbox() ?>
	
		 <?= $form->field($model, 'orden')->textInput(['style'=>'width:50px','type'=>'number']) ?>
		
	    <div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Crear') : Yii::t('app', 'Actualizar'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	    <div class="help-block">
	      <?php 
			if($model->hasErrors()){
		  		echo $form->errorSummary($model); 
			}
		  ?>
		 </div>
	   </div>

    <?php ActiveForm::end();  ?>

</div>
