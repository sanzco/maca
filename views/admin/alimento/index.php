<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Alimentos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alimento-index">
		<div class="box_header " style="font-size: 1.2rem">Alimentos</div>
	     	<div class="card-panel">

<?php Pjax::begin(); ?>    
	<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'nombre',
            'en_lista',
            'orden',
            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}'],
        ],
    ]); ?>
<?php Pjax::end(); ?>
</div>

<p>
        <?= Html::a(Yii::t('app', 'Crear Alimento'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    </div>
</div>	