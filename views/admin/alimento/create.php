<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Alimento */

$this->title = Yii::t('app', 'Create Alimento');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Alimentos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="box_header " style="font-size: 1.2rem">Nuevo Alimento</div>
	<div class="card-panel">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
