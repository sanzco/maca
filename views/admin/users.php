<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;

$this->title = 'MACA';
?>

    <div class="body-content">
			<div style="border: 1px solid #ddd">
					<div class="box_header">Usuarios</div>      
			</div>
			
			<div align="right" style="margin-top: 5px">
		        <?= Html::a('Crear Usuario', ['create'], ['class' => 'btn btn-success']) ?>
		    </div>
					
			   <?= GridView::widget([
		        'dataProvider' => $dataProvider,
		        'columns' => [
		            ['class' => 'yii\grid\SerialColumn'],
		
		            'persona_id',
		            'username',
		
		            ['class' => 'yii\grid\ActionColumn'],
		        ],
		    ]); ?>  
            

</div>