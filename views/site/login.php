<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
     
<div class="module form-module">
<div class="right" style="padding-top: 3px"><img alt="logo" width="83px" height="80px" src="<?php echo $this->theme->baseUrl ?>/images/flor_maca.png"></div>
    
  <div class="form">
  	<div class="left"><h2>Ingresa a tu cuenta</h2></div>
    
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        
    ]); ?>
    <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
    <?= $form->field($model, 'password')->passwordInput() ?>
      <?= Html::submitButton('Ingresar', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
      
   <?= $form->field($model, 'rememberMe')->checkbox([
            	'style'=>' position: absolute;',
   				'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
        ]) ?>
  
  </div>
  
  <div class="cta"><a href="">Olvido su contrasena</a></div>
</div>  

       

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
               </div>
        </div>

    <?php ActiveForm::end(); ?>

</div>
