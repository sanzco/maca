<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use app\models\TipoIdentificacion;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

$this->title = 'MACA';
?>

    <div class="jumbotron">
    
	       <?php $form = ActiveForm::begin([
	         'action' =>['cita/index'],
	        'layout' => 'horizontal',
	        'fieldConfig' => [
		        'options' => [
		            'tag' => false,
		        ],
		    ],
		    'class' => 'form-horizontal',
	        
	    ]);  ?>
	    <div class="row">
        	<?php 
	    		echo $form->field($model, 'id')->textInput(['autofocus' => true,'style'=>'width:150px']);
	    		
	    	?>
	    	<?php 
	    		$tipoIdItems = ArrayHelper::map(TipoIdentificacion::find()->all(),'id','nombre');
	   			 echo $form->field($model,'idType')->dropDownList($tipoIdItems)->label('Tipo Identificacion'); ?>
	    
	    <?= Html::submitButton('Consultar', ['class' => 'btn btn-primary btn_query', 'name' => 'query-button']) ?>
	    </div>
	    <div class="error" style="width: 290px;float:left">
	    		<?= $form->errorSummary($model); ?>
	    </div>
	    <?php ActiveForm::end(); ?>
    </div>

    <div class="body-content">

        <!--  <div class="row">
            <div class="col-lg-4">
                

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>
			 </div>
           -->
        </div>

</div>