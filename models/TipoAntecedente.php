<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipo_antecedente".
 *
 * @property integer $id
 * @property string $nombre
 *
 * @property Antecedentes[] $antecedentes
 * @property Persona[] $personas
 */
class TipoAntecedente extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipo_antecedente';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['nombre'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nombre' => Yii::t('app', 'Nombre'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAntecedentes()
    {
        return $this->hasMany(Antecedentes::className(), ['tipo_antecedente_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonas()
    {
        return $this->hasMany(Persona::className(), ['id' => 'persona_id'])->viaTable('antecedentes', ['tipo_antecedente_id' => 'id']);
    }
}
