<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "antecedentes".
 *
 * @property integer $tipo_antecedente_id
 * @property integer $persona_id
 * @property string $descripcion
 *
 * @property Persona $persona
 * @property TipoAntecedente $tipoAntecedente
 */
class Antecedentes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'antecedentes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipo_antecedente_id', 'persona_id'], 'required'],
            [['tipo_antecedente_id', 'persona_id'], 'integer'],
            [['descripcion'], 'string', 'max' => 500],
            [['persona_id'], 'exist', 'skipOnError' => true, 'targetClass' => Persona::className(), 'targetAttribute' => ['persona_id' => 'id']],
            [['tipo_antecedente_id'], 'exist', 'skipOnError' => true, 'targetClass' => TipoAntecedente::className(), 'targetAttribute' => ['tipo_antecedente_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tipo_antecedente_id' => Yii::t('app', 'Tipo Antecedente ID'),
            'persona_id' => Yii::t('app', 'Persona ID'),
            'descripcion' => Yii::t('app', 'Descripcion'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersona()
    {
        return $this->hasOne(Persona::className(), ['id' => 'persona_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoAntecedente()
    {
        return $this->hasOne(TipoAntecedente::className(), ['id' => 'tipo_antecedente_id']);
    }
}
