<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "conductas_compensatorias".
 *
 * @property integer $id
 * @property string $conducta
 *
 * @property ConductaCita[] $conductaCitas
 * @property Cita[] $citas
 */
class ConductasCompensatorias extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'conductas_compensatorias';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['conducta'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'conducta' => Yii::t('app', 'Conducta'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConductaCitas()
    {
        return $this->hasMany(ConductaCita::className(), ['conducta_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCitas()
    {
        return $this->hasMany(Cita::className(), ['cita_id' => 'cita_id'])->viaTable('conducta_cita', ['conducta_id' => 'id']);
    }
}
