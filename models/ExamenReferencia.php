<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "examen_referencia".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $valor_referencia_bajo
 * @property string $valor_referencia_superior
 *
 * @property Examen[] $examens
 */
class ExamenReferencia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'examen_referencia';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['valor_referencia_bajo', 'valor_referencia_superior'], 'number'],
            [['nombre'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nombre' => Yii::t('app', 'Nombre'),
            'valor_referencia_bajo' => Yii::t('app', 'Valor Referencia Bajo'),
            'valor_referencia_superior' => Yii::t('app', 'Valor Referencia Superior'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExamens()
    {
        return $this->hasMany(Examen::className(), ['examen_id' => 'id']);
    }
}
