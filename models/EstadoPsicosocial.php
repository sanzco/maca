<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estado_psicosocial".
 *
 * @property integer $cita_id
 * @property string $quien_vive
 * @property integer $relaciones_amistad
 *
 * @property Cita $cita
 */
class EstadoPsicosocial extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'estado_psicosocial';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cita_id'], 'required'],
            [['cita_id', 'relaciones_amistad'], 'integer'],
            [['quien_vive'], 'string', 'max' => 50],
            [['cita_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cita::className(), 'targetAttribute' => ['cita_id' => 'cita_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cita_id' => Yii::t('app', 'Cita ID'),
            'quien_vive' => Yii::t('app', 'Quien vive con usted?'),
            'relaciones_amistad' => Yii::t('app', '¿Tiene relaciones de amistad?'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCita()
    {
        return $this->hasOne(Cita::className(), ['cita_id' => 'cita_id']);
    }
}
