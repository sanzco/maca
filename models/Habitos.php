<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "habitos".
 *
 * @property integer $cita_id
 * @property string $lugar_consumo
 * @property integer $compania
 * @property string $quien_prepara
 * @property string $licor
 * @property string $cigarrillo
 * @property string $actividad_fisica
 * @property string $habito_intestinal
 * @property string $horas_sueno
 *
 * @property Cita $cita
 */
class Habitos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'habitos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cita_id'], 'required'],
            [['cita_id', 'compania', ], 'integer'],
            [['deporte','percepcion_sueno'],'safe'],
            [['quien_prepara'], 'string', 'max' => 50],
            [['horas_sueno'],'string', 'max' => 10],
            [['licor', 'cigarrillo'], 'string', 'max' => 250],
            [['actividad_fisica', 'habito_intestinal','lugar_consumo'], 'string', 'max' => 500],
            [['cita_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cita::className(), 'targetAttribute' => ['cita_id' => 'cita_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cita_id' => Yii::t('app', 'Cita ID'),
            'lugar_consumo' => Yii::t('app', 'Lugar donde consume los alimentos'),
            'compania' => Yii::t('app', 'Tiene compania durante las ingestas?'),
            'quien_prepara' => Yii::t('app', 'Quién prepara los alimentos para ud?'),
            'licor' => Yii::t('app', 'Consume licor'),
            'cigarrillo' => Yii::t('app', 'Fuma'),
            'actividad_fisica' => Yii::t('app', 'Actividad Fisica o Ejercicio'),
            'habito_intestinal' => Yii::t('app', 'Habito Intestinal'),
            'horas_sueno' => Yii::t('app', 'Horas de sueño'),
        	'percepcion_sueno' => Yii::t('app', 'Percepción del sueño'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCita()
    {
        return $this->hasOne(Cita::className(), ['cita_id' => 'cita_id']);
    }
}
