<?php

namespace app\models;
use yii\base\Model;

class PersonaForm extends Model {

 public $id;
 public $idType;
 
 
  /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Identificacion',
       		'idType' => 'Tipo de Identificacion',
        ];
    }
    
    public function rules()
	{
		return[
				[['id'], 'required'],
				[['idType'],'safe']
		];
	}
}