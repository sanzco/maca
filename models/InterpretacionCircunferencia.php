<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "interpretacion_circunferencia".
 *
 * @property integer $cita_id
 * @property integer $circunferencia_id
 * @property string $valor
 * @property string $interpretacion
 *
 * @property Cita $cita
 * @property Circunferencias $circunferencia
 */
class InterpretacionCircunferencia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'interpretacion_circunferencia';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cita_id', 'circunferencia_id'], 'required'],
            [['cita_id', 'circunferencia_id'], 'integer'],
            [['valor'], 'number'],
            [['interpretacion'], 'string', 'max' => 500],
            [['cita_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cita::className(), 'targetAttribute' => ['cita_id' => 'cita_id']],
            [['circunferencia_id'], 'exist', 'skipOnError' => true, 'targetClass' => Circunferencias::className(), 'targetAttribute' => ['circunferencia_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cita_id' => Yii::t('app', 'Cita ID'),
            'circunferencia_id' => Yii::t('app', 'Circunferencia ID'),
            'valor' => Yii::t('app', 'Valor'),
            'interpretacion' => Yii::t('app', 'Interpretacion'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCita()
    {
        return $this->hasOne(Cita::className(), ['cita_id' => 'cita_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCircunferencia()
    {
        return $this->hasOne(Circunferencias::className(), ['id' => 'circunferencia_id']);
    }
}
