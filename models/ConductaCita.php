<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "conducta_cita".
 *
 * @property integer $cita_id
 * @property integer $conducta_id
 * @property integer $posee_conducta
 * @property string $frecuencia
 *
 * @property ConductasCompensatorias $conducta
 * @property Cita $cita
 */
class ConductaCita extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'conducta_cita';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cita_id', 'conducta_id'], 'required'],
            [['cita_id', 'conducta_id', 'posee_conducta'], 'integer'],
            [['frecuencia'], 'string', 'max' => 50],
            [['conducta_id'], 'exist', 'skipOnError' => true, 'targetClass' => ConductasCompensatorias::className(), 'targetAttribute' => ['conducta_id' => 'id']],
            [['cita_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cita::className(), 'targetAttribute' => ['cita_id' => 'cita_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cita_id' => Yii::t('app', 'Cita ID'),
            'conducta_id' => Yii::t('app', 'Conducta ID'),
            'posee_conducta' => Yii::t('app', 'Posee Conducta'),
            'frecuencia' => Yii::t('app', 'Frecuencia'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConducta()
    {
        return $this->hasOne(ConductasCompensatorias::className(), ['id' => 'conducta_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCita()
    {
        return $this->hasOne(Cita::className(), ['cita_id' => 'cita_id']);
    }
}
