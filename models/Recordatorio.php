<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "recordatorio".
 *
 * @property integer $id
 * @property integer $cita_id
 * @property string $hora
 * @property integer $tiempo_id
 * @property string $preparacion
 *
 * @property TiempoComida $tiempo
 * @property Cita $cita
 */
class Recordatorio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recordatorio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cita_id', 'tiempo_id'], 'integer'],
            [['hora','tiempo_id'], 'safe'],
            [['preparacion'], 'string', 'max' => 500],
            [['tiempo_id'], 'exist', 'skipOnError' => true, 'targetClass' => TiempoComida::className(), 'targetAttribute' => ['tiempo_id' => 'tiempo_id']],
            [['cita_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cita::className(), 'targetAttribute' => ['cita_id' => 'cita_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'cita_id' => Yii::t('app', 'Cita ID'),
            'hora' => Yii::t('app', 'Hora'),
            'tiempo_id' => Yii::t('app', 'Tiempo ID'),
            'preparacion' => Yii::t('app', 'Preparacion'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTiempo()
    {
        return $this->hasOne(TiempoComida::className(), ['tiempo_id' => 'tiempo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCita()
    {
        return $this->hasOne(Cita::className(), ['cita_id' => 'cita_id']);
    }
    
     public function getRecordatorioPreparaciones(){
     	 return $this->hasMany(RecordatorioPreparacion::className(), ['recordatorio_id' => 'id']);
     }
}
