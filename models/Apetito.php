<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "apetito".
 *
 * @property integer $id
 * @property string $nombre
 *
 * @property Anamnesis[] $anamneses
 */
class Apetito extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'apetito';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nombre' => Yii::t('app', 'Nombre'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnamneses()
    {
        return $this->hasMany(Anamnesis::className(), ['apetito_id' => 'id']);
    }
}
