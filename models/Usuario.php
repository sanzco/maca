<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuario".
 *
 * @property integer $persona_id
 * @property string $username
 * @property string $password
 * @property string $estado
 *
 * @property PerfilUsuario[] $perfilUsuarios
 * @property Perfil[] $perfiles
 * @property Persona $persona
 */
class Usuario extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	public $ACTIVE = 'A';
	public $password_repeat; 
	public $tempperfiles;

	public static function tableName()
    {
        return 'usuario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['persona_id'], 'integer'],
            [['username'], 'string', 'max' => 50],
            [['password'], 'string', 'max' => 80],
            [['estado'], 'string', 'max' => 1],
            [['persona_id'], 'exist', 'skipOnError' => true, 'targetClass' => Persona::className(), 'targetAttribute' => ['persona_id' => 'id']],
	        ['username', 'required'],
            ['password', 'required'],
            ['password_repeat', 'required'],
	        ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords no coinciden" ], 
	        ['tempperfiles','safe']
       ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'persona_id' => Yii::t('app', 'Persona ID'),
            'username' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
            'estado' => Yii::t('app', 'Estado'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerfilUsuarios()
    {
        return $this->hasMany(PerfilUsuario::className(), ['usuario_id' => 'persona_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerfiles()
    {
        return $this->hasMany(Perfil::className(), ['id' => 'perfil_id'])->viaTable('perfil_usuario', ['usuario_id' => 'persona_id']);
    }

 	/**
     * @return \yii\db\ActiveQuery
     */
	public function getTempperfiles(){
		 return $this->perfiles;
   
	}
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersona()
    {
        return $this->hasOne(Persona::className(), ['id' => 'persona_id']);
    }
    
	public static function findUser($username){
    	return Usuario::find()
       		->where(['username'=> $username])
       		->one();
    }
    
	public function beforeSave($insert)
	{
	    if (parent::beforeSave($insert)) {
	    	if($this->estado===null || $this->estado=='1'){
	       	 	$this->estado = $this->ACTIVE;
	    	}
	    	$this->password = Yii::$app->security->generatePasswordHash($this->password);
	        return true;
	    } else {
	        return false;
	    }
	}
	

	
}
