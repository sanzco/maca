<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipo_cambio_ingesta".
 *
 * @property integer $id
 * @property string $nombre
 *
 * @property IngestaAnamnesis[] $ingestaAnamneses
 * @property Anamnesis[] $anamneses
 */
class TipoCambioIngesta extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipo_cambio_ingesta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nombre' => Yii::t('app', 'Nombre'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngestaAnamneses()
    {
        return $this->hasMany(IngestaAnamnesis::className(), ['tipo_ingesta_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnamneses()
    {
        return $this->hasMany(Anamnesis::className(), ['anamnesis_id' => 'anamnesis_id'])->viaTable('ingesta_anamnesis', ['tipo_ingesta_id' => 'id']);
    }
}
