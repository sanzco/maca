<?php

namespace app\models;
use Yii;
use app\models\Usuario;

class User extends \yii\base\Object implements \yii\web\IdentityInterface
{
    public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;
    public $state;

    


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        $otroUser = Usuario::find()
            ->where([
                "persona_id" => $id
            ])
            ->one();
            $user = new User();
           $user->id = $id;
		   $user->username =$otroUser->username;
		   $user->password = $otroUser->password;
	       $user->state = $otroUser->estado;   
	    if (!count($user)) {
	        return null;
	    }
	    return new static($user);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
       throw new NotSupportedException();
    }
    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
       $model = new Usuario();
       $otro = Usuario::findUser($username);
       $user = new User();
       //Yii::trace($otro->password);
       if($otro!=null){
	       $user->id = $otro->persona_id;
		   $user->username = $username;
		   $user->password = $otro->password;
	       $user->state = $otro->estado;
	       if(isset($user)){
	           return $user;
	       }
       }
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
}
