<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ingesta_anamnesis".
 *
 * @property integer $anamnesis_id
 * @property integer $tipo_ingesta_id
 * @property string $texto_descriptivo
 *
 * @property Anamnesis $anamnesis
 * @property TipoCambioIngesta $tipoIngesta
 */
class IngestaAnamnesis extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ingesta_anamnesis';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['anamnesis_id', 'tipo_ingesta_id'], 'required'],
            [['anamnesis_id', 'tipo_ingesta_id'], 'integer'],
            [['texto_descriptivo'], 'string', 'max' => 250],
            [['anamnesis_id'], 'exist', 'skipOnError' => true, 'targetClass' => Anamnesis::className(), 'targetAttribute' => ['anamnesis_id' => 'anamnesis_id']],
            [['tipo_ingesta_id'], 'exist', 'skipOnError' => true, 'targetClass' => TipoCambioIngesta::className(), 'targetAttribute' => ['tipo_ingesta_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'anamnesis_id' => Yii::t('app', 'Anamnesis ID'),
            'tipo_ingesta_id' => Yii::t('app', 'Tipo Ingesta ID'),
            'texto_descriptivo' => Yii::t('app', 'Texto Descriptivo'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnamnesis()
    {
        return $this->hasOne(Anamnesis::className(), ['anamnesis_id' => 'anamnesis_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoIngesta()
    {
        return $this->hasOne(TipoCambioIngesta::className(), ['id' => 'tipo_ingesta_id']);
    }
}
