<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipo_identificacion".
 *
 * @property integer $id
 * @property string $nombre
 *
 * @property Persona[] $personas
 */
class TipoIdentificacion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipo_identificacion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['nombre'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nombre' => Yii::t('app', 'Nombre'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonas()
    {
        return $this->hasMany(Persona::className(), ['tipo_identificacion_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return TipoIdentificacionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TipoIdentificacionQuery(get_called_class());
    }
}
