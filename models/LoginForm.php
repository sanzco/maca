<?php

namespace app\models;

use Yii;
use yii\base\Model;
//use app\models\Usuario;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;
    public $ESTADO_ACTIVO = 'A';

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

     public function attributeLabels()
    {
    	return [
            'username' => 'Usuario',
    		'password' => 'Clave',
    		'rememberMe'=> 'Recordarme'
        ];
    }
    
    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
         
            //Yii::trace('Pass guardado'.$user->password);
            //Yii::trace('hash del pass:'.Yii::$app->getSecurity()->generatePasswordHash($this->password));
            if($user!=null){
            	if(!Yii::$app->getSecurity()->validatePassword($this->password,$user->password)){
            		$this->addError($attribute, 'Usuario o password errado');
            	}
            	if($user->state!=$this->ESTADO_ACTIVO){
            		$this->addError($attribute, 'Usuario Desactivado');
            	}
            }else{
	        	$this->addError($attribute, 'Usuario o password errado');
	        }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
    	if ($this->validate()) {
			$this->validatePassword($this->username,array());			
        	if($this->getUser()!=null){
            		return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
        	}
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }
        return $this->_user;
    }
}
