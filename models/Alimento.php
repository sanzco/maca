<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alimento".
 *
 * @property integer $alimento_id
 * @property string $nombre
 * @property integer $en_lista
 */
class Alimento extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'alimento';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            	[['alimento_id', 'en_lista', 'orden'], 'integer'],
            	[['nombre'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'alimento_id' => Yii::t('app', 'Alimento ID'),
            'nombre' => Yii::t('app', 'Nombre'),
            'en_lista' => Yii::t('app', 'Mostrar en formulario'),
        ];
    }
}
