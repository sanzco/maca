<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "recordatorio_preparacion".
 *
 * @property integer $recordatorio_id
 * @property string $alimento
 * @property string $cantidad
 *
 * @property Recordatorio $recordatorio
 */
class RecordatorioPreparacion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recordatorio_preparacion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['recordatorio_id'], 'integer'],
            [['alimento', 'cantidad'], 'string', 'max' => 50],
            [['recordatorio_id'], 'exist', 'skipOnError' => true, 'targetClass' => Recordatorio::className(), 'targetAttribute' => ['recordatorio_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'recordatorio_id' => Yii::t('app', 'Recordatorio ID'),
            'alimento' => Yii::t('app', 'Alimento'),
            'cantidad' => Yii::t('app', 'Cantidad'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecordatorio()
    {
        return $this->hasOne(Recordatorio::className(), ['id' => 'recordatorio_id']);
    }
}
