<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TipoIdentificacion]].
 *
 * @see TipoIdentificacion
 */
class TipoIdentificacionQuery extends \yii\db\ActiveQuery
{

    /**
     * @inheritdoc
     * @return TipoIdentificacion[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TipoIdentificacion|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
