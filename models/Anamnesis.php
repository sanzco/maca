<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "anamnesis".
 *
 * @property integer $anamnesis_id
 * @property integer $cita_id
 * @property string $analisis_ingesta
 * @property integer $apetito_id
 * @property string $cambios_ingesta
 * @property string $alimentos_preferidos
 * @property string $alimentos_rechazados
 * @property string $intolerancias
 * @property integer $volumen_consumido_id
 * @property string $tiempo_empleado
 * @property integer $uso_sal
 *
 * @property Apetito $apetito
 * @property Cita $cita
 * @property VolumenConsumido $volumenConsumido
 * @property IngestaAnamnesis[] $ingestaAnamneses
 * @property TipoCambioIngesta[] $tipoIngestas
 */
class Anamnesis extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'anamnesis';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cita_id', 'apetito_id', 'volumen_consumido_id', 'uso_sal'], 'integer'],
            [['analisis_ingesta'], 'string', 'max' => 1000],
            [['cambios_ingesta'], 'string', 'max' => 1],
            [['alimentos_preferidos', 'alimentos_rechazados', 'intolerancias'], 'string', 'max' => 500],
            [['tiempo_empleado'], 'string', 'max' => 200],
            [['apetito_id'], 'exist', 'skipOnError' => true, 'targetClass' => Apetito::className(), 'targetAttribute' => ['apetito_id' => 'id']],
            [['cita_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cita::className(), 'targetAttribute' => ['cita_id' => 'cita_id']],
            [['volumen_consumido_id'], 'exist', 'skipOnError' => true, 'targetClass' => VolumenConsumido::className(), 'targetAttribute' => ['volumen_consumido_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'anamnesis_id' => Yii::t('app', 'Anamnesis ID'),
            'cita_id' => Yii::t('app', 'Cita ID'),
            'analisis_ingesta' => Yii::t('app', 'Analisis Ingesta'),
            'apetito_id' => Yii::t('app', 'Apetito ID'),
            'cambios_ingesta' => Yii::t('app', 'Cambios en la ingesta'),
            'alimentos_preferidos' => Yii::t('app', 'Alimentos preferidos'),
            'alimentos_rechazados' => Yii::t('app', 'Alimentos rechazados'),
            'intolerancias' => Yii::t('app', 'Intolerancias/Alergias alimentarias'),
            'volumen_consumido_id' => Yii::t('app', 'Percepción Volumen Consumido'),
            'tiempo_empleado' => Yii::t('app', 'Tiempo Empleado para consumo de alimentos'),
            'uso_sal' => Yii::t('app','Utiliza el salero en la mesa?'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApetito()
    {
        return $this->hasOne(Apetito::className(), ['id' => 'apetito_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCita()
    {
        return $this->hasOne(Cita::className(), ['cita_id' => 'cita_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVolumenConsumido()
    {
        return $this->hasOne(VolumenConsumido::className(), ['id' => 'volumen_consumido_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngestaAnamneses()
    {
        return $this->hasMany(IngestaAnamnesis::className(), ['anamnesis_id' => 'anamnesis_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoIngestas()
    {
        return $this->hasMany(TipoCambioIngesta::className(), ['id' => 'tipo_ingesta_id'])->viaTable('ingesta_anamnesis', ['anamnesis_id' => 'anamnesis_id']);
    }
}
