<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "examen".
 *
 * @property integer $examen_id
 * @property integer $cita_id
 * @property string $fecha
 * @property string $valor
 * @property string $interpretacion
 *
 * @property ExamenReferencia $examen
 */
class Examen extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'examen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['examen_id', 'cita_id'], 'integer'],
            [['fecha'], 'safe'],
            [['valor'], 'number'],
            [['interpretacion'], 'string', 'max' => 120],
            [['examen_id'], 'exist', 'skipOnError' => true, 'targetClass' => ExamenReferencia::className(), 'targetAttribute' => ['examen_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'examen_id' => Yii::t('app', 'Examen ID'),
            'cita_id' => Yii::t('app', 'Cita ID'),
            'fecha' => Yii::t('app', 'Fecha'),
            'valor' => Yii::t('app', 'Valor'),
            'interpretacion' => Yii::t('app', 'Interpretacion'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExamen()
    {
        return $this->hasOne(ExamenReferencia::className(), ['id' => 'examen_id']);
    }
    
    public function duplicateResult(){
    	$examen = Examen::findOne(
    	[
		    'cita_id' => $this->cita_id,
    		'examen_id'=>$this->examen_id
    	]); 
    	return $examen;
    }
}
