<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "menu".
 *
 * @property integer $alimento_id
 * @property integer $cita_id
 * @property integer $frecuencia_id
 * @property integer $valor
 * @property integer $observacion
 *
 * @property Frecuencia $frecuencia
 * @property Alimento $alimento
 * @property Cita $cita
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['alimento_id', 'cita_id'], 'required'],
            [['alimento_id', 'cita_id', 'frecuencia_id', 'valor'], 'integer'],
            [['observacion'], 'safe'],
            [['frecuencia_id'], 'exist', 'skipOnError' => true, 'targetClass' => Frecuencia::className(), 'targetAttribute' => ['frecuencia_id' => 'id']],
            [['alimento_id'], 'exist', 'skipOnError' => true, 'targetClass' => Alimento::className(), 'targetAttribute' => ['alimento_id' => 'alimento_id']],
            [['cita_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cita::className(), 'targetAttribute' => ['cita_id' => 'cita_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'alimento_id' => Yii::t('app', 'Alimento ID'),
            'cita_id' => Yii::t('app', 'Cita ID'),
            'frecuencia_id' => Yii::t('app', 'Frecuencia ID'),
            'valor' => Yii::t('app', 'Valor'),
            'observacion' => Yii::t('app', 'Observacion'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFrecuencia()
    {
        return $this->hasOne(Frecuencia::className(), ['id' => 'frecuencia_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlimento()
    {
        return $this->hasOne(Alimento::className(), ['alimento_id' => 'alimento_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCita()
    {
        return $this->hasOne(Cita::className(), ['cita_id' => 'cita_id']);
    }
}
