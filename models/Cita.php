<?php

namespace app\models;

use Yii;
use yii\db\Query;
use yii\data\SqlDataProvider;
/**
 * This is the model class for table "cita".
 *
 * @property integer $cita_id
 * @property string $fecha_hora
 * @property string $examen_fisico
 * @property integer $persona_id
 * @property string $diagnostico
 * @property integer $persona_medico
 *
 * @property Anamnesis[] $anamneses
 * @property Antropometria $antropometria
 * @property Persona $personaMedico
 * @property string $motivo 
 * @property string $objetivos_nutricionales 
 * @property Persona $persona
 * @property Examen[] $examenes
 * @property ExamenReferencia[] $examenes0
 * @property Farmacos[] $farmacos
 * @property Habitos $habitos
 * @property InterpretacionCircunferencia[] $interpretacionCircunferencias
 * @property Circunferencias[] $circunferencias
 * @property Menu[] $menus
 * @property Alimento[] $alimentos
 * @property Recordatorio[] $recordatorios
 */
class Cita extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cita';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cita_id','fecha_hora','plan_nutricional','recomendaciones','f_actividad'], 'safe'],
            [['persona_id', 'persona_medico'], 'integer'],
            [['examen_fisico'], 'string', 'max' => 500],
            [['motivo', 'objetivos_nutricionales'], 'string', 'max' => 5000], 
            [['diagnostico'], 'string', 'max' => 2000],
            [['f_actividad'],'match', 'pattern' => '/^\s*[-+]?[0-9]*[.]?[0-9]+([eE][-+]?[0-9]+)?\s*$/'],
            [['persona_medico'], 'exist', 'skipOnError' => true, 'targetClass' => Persona::className(), 'targetAttribute' => ['persona_medico' => 'id']],
            [['persona_id'], 'exist', 'skipOnError' => true, 'targetClass' => Persona::className(), 'targetAttribute' => ['persona_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cita_id' => Yii::t('app', 'Cita ID'),
            'fecha_hora' => Yii::t('app', 'Fecha Hora'),
            'examen_fisico' => Yii::t('app', 'Examen Fisico'),
            'persona_id' => Yii::t('app', 'Persona ID'),
            'diagnostico' => Yii::t('app', 'Concepto y Diagnostico nutricional'),
            'persona_medico' => Yii::t('app', 'Persona Medico'),
        	'motivo' => Yii::t('app', 'Motivo de Consulta'), 
            'objetivos_nutricionales' => Yii::t('app', 'Objetivos Nutricionales'), 
        	'f_actividad'=>Yii::t('app', 'F. Actividad'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnamnesis()
    {
        return $this->hasOne(Anamnesis::className(), ['cita_id' => 'cita_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAntropometria()
    {
        return $this->hasOne(Antropometria::className(), ['cita_id' => 'cita_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonaMedico()
    {
        return $this->hasOne(Persona::className(), ['id' => 'persona_medico']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersona()
    {
        return $this->hasOne(Persona::className(), ['id' => 'persona_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExamenes()
    {
        return $this->hasMany(Examen::className(), ['cita_id' => 'cita_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExamenes0()
    {
        return $this->hasMany(ExamenReferencia::className(), ['id' => 'examen_id'])->viaTable('examen', ['cita_id' => 'cita_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFarmacos()
    {
        return $this->hasMany(Farmacos::className(), ['cita_id' => 'cita_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHabitos()
    {
    	return $this->hasOne(Habitos::className(), ['cita_id' => 'cita_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterpretacionCircunferencias()
    {
        return $this->hasMany(InterpretacionCircunferencia::className(), ['cita_id' => 'cita_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCircunferencias()
    {
        return $this->hasMany(Circunferencias::className(), ['id' => 'circunferencia_id'])->viaTable('interpretacion_circunferencia', ['cita_id' => 'cita_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenus()
    {
        return $this->hasMany(Menu::className(), ['cita_id' => 'cita_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlimentos()
    {
        return $this->hasMany(Alimento::className(), ['alimento_id' => 'alimento_id'])->viaTable('menu', ['cita_id' => 'cita_id']);
    }
    
	public function getConductas()
   {
       return $this->hasMany(ConductasCompensatorias::className(), ['id' => 'conducta_id'])->viaTable('conducta_cita', ['cita_id' => 'cita_id']);
   }
   
   public function getConductaCitas()
   {
   	 return $this->hasMany(ConductaCita::className(), ['cita_id' => 'cita_id']);
   }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecordatorios()
    {
        return $this->hasMany(Recordatorio::className(), ['cita_id' => 'cita_id']);
    }
    
     /**
    * @return \yii\db\ActiveQuery
    */
   public function getEstadoPsicosocial()
   {
       return $this->hasOne(EstadoPsicosocial::className(), ['cita_id' => 'cita_id']);
   }
    
    
     public function beforeSave($insert)
	{
	    if (parent::beforeSave($insert)) {
	    	if($this->fecha_hora===null){
	       	 	$this->fecha_hora = new \yii\db\Expression('NOW()');
	    	}
	        return true;
	    } else {
	        return false;
	    }
	}
    
	public static function lastDate($identificacion){
		$lastdate = (new \yii\db\Query())
			->select('*')
		    ->from('cita')
		    ->join('INNER JOIN','persona','cita.persona_id = persona.id')
		    ->where(['persona.identificacion' => $identificacion])
		    ->orderBy('fecha_hora DESC')
		    ->limit(1)
		    ->one();
		    return $lastdate;
	}
    
	/*
	 * Consultar la ultima cita sin contar la actual
	 */
	public static function lastDateById($id,$citaId){
		$lastdate = (new \yii\db\Query())
			->select('*')
		    ->from('cita')
		    ->join('INNER JOIN','persona','cita.persona_id = persona.id')
		    ->where(['persona.id' => $id])
		    ->andWhere(['<>','cita.cita_id',$citaId])
		    ->orderBy('fecha_hora DESC')
		    ->limit(1)
		    ->one();
		    return $lastdate;
	}
	
	public static function datesWithoutRecommendations(){
		$medico = Yii::$app->user->id;
		$totalCount = Yii::$app->db
            ->createCommand('SELECT COUNT(*) FROM cita 
            				WHERE plan_nutricional is null or recomendaciones is null and persona_medico =:medico', [':medico' => $medico])
            ->queryScalar();
        $query = new Query;
		$query->select('*')
		      ->from('cita')
		      ->join('INNER JOIN','persona','cita.persona_id = persona.id')
		      ->where('plan_nutricional is null')
		      ->andWhere('recomendaciones is null');
			            
        $dataProvider = new SqlDataProvider([
		    'sql' => $query->createCommand()->sql,
		    'totalCount' => $totalCount,
		    'sort' => [
		        'attributes' => [
		            'fecha_hora' => [
		                'asc' => ['fecha_hora' => SORT_ASC],
		                'desc' => ['fecha_hora' => SORT_DESC],
		                'default' => SORT_DESC,
		                'label' => 'Fecha',
		            ],
		            'apellido' => [
		                'asc' => ['apellido' => SORT_ASC],
		                'desc' => ['apellido' => SORT_DESC],
		                'default' => SORT_DESC,
		                'label' => 'Apellido',
		            ],
		        ],
		        'defaultOrder' => ['fecha_hora'=>SORT_DESC]
		    ],
		    'pagination' => [
		        'pageSize' => 10,
		    ],
		]);
		
		return $dataProvider;
	}
		    
}
