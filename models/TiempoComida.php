<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tiempo_comida".
 *
 * @property integer $tiempo_id
 * @property string $nombre
 *
 * @property Recordatorio[] $recordatorios
 */
class TiempoComida extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tiempo_comida';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tiempo_id', 'nombre'], 'required'],
            [['tiempo_id'], 'integer'],
            [['nombre'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tiempo_id' => Yii::t('app', 'Tiempo ID'),
            'nombre' => Yii::t('app', 'Nombre'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecordatorios()
    {
        return $this->hasMany(Recordatorio::className(), ['tiempo_id' => 'tiempo_id']);
    }
}
