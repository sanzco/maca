<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "circunferencias".
 *
 * @property integer $id
 * @property string $nombre
 *
 * @property InterpretacionCircunferencia[] $interpretacionCircunferencias
 * @property Cita[] $citas
 */
class Circunferencias extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'circunferencias';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['nombre'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nombre' => Yii::t('app', 'Nombre'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterpretacionCircunferencias()
    {
        return $this->hasMany(InterpretacionCircunferencia::className(), ['circunferencia_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCitas()
    {
        return $this->hasMany(Cita::className(), ['cita_id' => 'cita_id'])->viaTable('interpretacion_circunferencia', ['circunferencia_id' => 'id']);
    }
}
