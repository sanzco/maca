<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "frecuencia".
 *
 * @property integer $id
 * @property string $frecuencia
 *
 * @property Menu[] $menus
 */
class Frecuencia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'frecuencia';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['frecuencia'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'frecuencia' => Yii::t('app', 'Frecuencia'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenus()
    {
        return $this->hasMany(Menu::className(), ['frecuencia_id' => 'id']);
    }
}
