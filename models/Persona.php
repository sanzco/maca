<?php

namespace app\models;

use Yii;
use app\models\Antecedentes;
use app\models\TipoAntecedente;

/**
 * This is the model class for table "persona".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $apellido
 * @property string $identificacion
 * @property string $fecha_nacimiento
 * @property string $ocupacion
 * @property string $telefono
 * @property string $email
 * @property integer $tipo_identificacion_id
 * @property integer $tipo_persona_id
 *
 * @property Antecedentes[] $antecedentes
 * @property TipoAntecedente[] $tipoAntecedentes
 * @property TipoIdentificacion $tipoIdentificacion
 * @property TipoPersona $tipoPersona
 */
class Persona extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'persona';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha_nacimiento','genero'], 'safe'],
            [['tipo_identificacion_id', 'tipo_persona_id', 'estado_civil_id'],'integer'],
            [['nombre', 'apellido'],'string','max' => 50],
            [['identificacion'], 'string', 'max' => 20],
            [['ocupacion', 'email'],'string', 'max' => 50],
            [['telefono'], 'string', 'max' => 15],
            [['tipo_identificacion_id'], 'exist', 'skipOnError' => true, 'targetClass' => TipoIdentificacion::className(), 'targetAttribute' => ['tipo_identificacion_id' => 'id']],
            [['tipo_persona_id'], 'exist', 'skipOnError' => true, 'targetClass' => TipoPersona::className(), 'targetAttribute' => ['tipo_persona_id' => 'id']],
            [['fecha_nacimiento','identificacion','nombre','apellido','tipo_identificacion_id','genero'],'required'],
            [['tipo_identificacion_id','id'], 'safe'],
            [['estado_civil_id'], 'exist', 'skipOnError' => true, 'targetClass' => EstadoCivil::className(), 'targetAttribute' => ['estado_civil_id' => 'id']], 
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nombre' => Yii::t('app', 'Nombre'),
            'apellido' => Yii::t('app', 'Apellido'),
            'identificacion' => Yii::t('app', 'Identificacion'),
            'fecha_nacimiento' => Yii::t('app', 'Fecha Nacimiento'),
            'ocupacion' => Yii::t('app', 'Ocupacion'),
            'telefono' => Yii::t('app', 'Telefono'),
            'email' => Yii::t('app', 'Email'),
            'tipo_identificacion_id' => Yii::t('app', 'Tipo Identificacion'),
            'tipo_persona_id' => Yii::t('app', 'Tipo Persona'),
        	'estado_civil_id' => Yii::t('app', 'Estado Civil'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAntecedentes()
    {
        return $this->hasMany(Antecedentes::className(), ['persona_id' => 'id']);
    }

 /**
     * @return \yii\db\ActiveQuery
     */
    public function getCitas()
    {
        return $this->hasMany(Cita::className(), ['persona_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoAntecedentes()
    {
        return $this->hasMany(TipoAntecedente::className(), ['id' => 'tipo_antecedente_id'])->viaTable('antecedentes', ['persona_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoIdentificacion()
    {
        return $this->hasOne(TipoIdentificacion::className(), ['id' => 'tipo_identificacion_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoPersona()
    {
        return $this->hasOne(TipoPersona::className(), ['id' => 'tipo_persona_id']);
    }
    
 /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstadoCivil()
    {
        return $this->hasOne(EstadoCivil::className(), ['id' => 'estado_civil_id']);
    }
    
    public static function findPersonaById($tipoId, $id){
    	 return Persona::find()
       		->where(['identificacion'=> $id, 'tipo_identificacion_id'=>$tipoId])
       		->one();
    }
    
public function beforeSave($insert)
{
    if (parent::beforeSave($insert)) {
    	if($this->tipo_persona_id===null){
       	 	$this->tipo_persona_id = 1;
    	}
    	if($this->historia_clinica === null){
    		$this->historia_clinica = $this->tipoIdentificacion->prefijo.$this->identificacion;
    	}
        return true;
    } else {
        return false;
    }
}

	public function getEdad(){
		list($Y,$m,$d) = explode("-",$this->fecha_nacimiento);
		//Yii::trace(date("md"));
		//Yii::trace($m.$d);
		$edad = 0;
		$meses = 0;
		if(date("md") < $m.$d){
			$edad = date("Y")-$Y-1;
			$meses = ((12-($m-date("m")))/12);
			if($meses == 1){$meses=0.9;}
			$edad = $edad + $meses;
		}else{
			$edad = date("Y")-$Y ;
			$meses =  ((($m-date("m"))*-1))/12;
			$edad = $edad+ $meses;
		}
		//Yii::trace($meses);
    	return round($edad,1);
	}
}
