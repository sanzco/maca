<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "farmacos".
 *
 * @property integer $id
 * @property integer $cita_id
 * @property string $nombre
 * @property string $dosis_frecuencia
 * @property string $objetivo
 *
 * @property Cita $cita
 */
class Farmacos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'farmacos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cita_id'], 'integer'],
            [['prescripcion'], 'safe'],
            [['nombre', 'dosis_frecuencia'], 'string', 'max' => 50],
            [['objetivo'], 'string', 'max' => 500],
            [['cita_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cita::className(), 'targetAttribute' => ['cita_id' => 'cita_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cita_id' => 'Cita ID',
            'nombre' => 'Nombre',
            'dosis_frecuencia' => 'Dosis Frecuencia',
            'objetivo' => 'Objetivo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCita()
    {
        return $this->hasOne(Cita::className(), ['cita_id' => 'cita_id']);
    }
}
