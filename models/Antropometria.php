<?php

namespace app\models;

use Yii;
use app\models\Cita;
/**
 * This is the model class for table "antropometria".
 *
 * @property integer $cita_id
 * @property string $peso
 * @property string $talla
 * @property string $cintura
 * @property string $cadera
 * @property string $grasa_objetivo
 * @property string $carpo
 * @property string $circ_pantorilla
 * @property string $circ_brazo
 * @property string $circ_muscular
 * @property string $interpretacion_imc
 *
 * @property Cita $cita
 */
class Antropometria extends \yii\db\ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'antropometria';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cita_id','peso','talla','cadera','cintura'], 'required'],
            [['cita_id'], 'integer'],
            [['pgraso','plgraso','imc','grasa_actual','peso_ideal_estructura_osea','triceps','interpretacion_imc'], 'safe'],
            [['peso', 'talla', 'cintura', 'cadera', 'grasa_objetivo', 'carpo', 'circ_pantorilla', 'circ_brazo', 'circ_muscular'],'match', 'pattern' => '/^\s*[-+]?[0-9]*[.]?[0-9]+([eE][-+]?[0-9]+)?\s*$/'],
            [['cita_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cita::className(), 'targetAttribute' => ['cita_id' => 'cita_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cita_id' => Yii::t('app', 'Cita ID'),
            'peso' => Yii::t('app', 'Peso (kg)'),
            'talla' => Yii::t('app', 'Talla (m)'),
            'cintura' => Yii::t('app', 'C. Cintura (cm)'),
            'cadera' => Yii::t('app', 'C. Cadera (cm)'),
            'grasa_objetivo' => Yii::t('app', 'Grasa Objetivo (%)'),
            'carpo' => Yii::t('app', 'C. Carpo (cm)'),
            'circ_pantorilla' => Yii::t('app', 'Circ Pantorilla (cm)'),
            'circ_brazo' => Yii::t('app', 'Circ Brazo'),
            'circ_muscular' => Yii::t('app', 'Circ Muscular'),
        	'triceps'=> Yii::t('app', 'P. de Triceps(mm)'),
        	'peso_ideal_estructura_osea' => Yii::t('app', 'Peso Ideal por estructura ósea(Kg)'),
       		'interpretacion_imc' =>  Yii::t('app', 'Interpretación IMC'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCita()
    {
        return $this->hasOne(Cita::className(), ['cita_id' => 'cita_id']);
    }
    
}
