-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.6.12-log - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura para tabla macadb.alimento
DROP TABLE IF EXISTS `alimento`;
CREATE TABLE IF NOT EXISTS `alimento` (
  `alimento_id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `en_lista` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`alimento_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla macadb.alimento: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `alimento` DISABLE KEYS */;
INSERT INTO `alimento` (`alimento_id`, `nombre`, `en_lista`) VALUES
	(1, 'Leche', 1),
	(2, 'Yogurt', 1),
	(3, 'Queso', 1),
	(4, 'Carne de Res', 1);
/*!40000 ALTER TABLE `alimento` ENABLE KEYS */;


-- Volcando estructura para tabla macadb.anamnesis
DROP TABLE IF EXISTS `anamnesis`;
CREATE TABLE IF NOT EXISTS `anamnesis` (
  `anamnesis_id` int(11) NOT NULL AUTO_INCREMENT,
  `cita_id` int(11) DEFAULT NULL,
  `analisis_ingesta` varchar(1000) COLLATE latin1_spanish_ci DEFAULT NULL,
  `apetito_id` tinyint(4) DEFAULT NULL,
  `cambios_ingesta` varchar(1) COLLATE latin1_spanish_ci DEFAULT NULL COMMENT 'Cambios en la ingesta',
  `alimentos_preferidos` varchar(500) COLLATE latin1_spanish_ci DEFAULT NULL COMMENT 'Alimentos preferidos',
  `alimentos_rechazados` varchar(500) COLLATE latin1_spanish_ci DEFAULT NULL COMMENT 'Alimentos rechazados',
  `intolerancias` varchar(500) COLLATE latin1_spanish_ci DEFAULT NULL COMMENT 'Intolerancias/Alergias alimentarias',
  `volumen_consumido_id` tinyint(4) DEFAULT NULL,
  `tiempo_empleado` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `uso_sal` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`anamnesis_id`),
  KEY `FK_ANAMNESIS_CITA` (`cita_id`),
  KEY `FK_ANAMNESIS_APETITO` (`apetito_id`),
  KEY `FK_ANAMNESIS_VOLUMEN` (`volumen_consumido_id`),
  CONSTRAINT `FK_ANAMNESIS_APETITO` FOREIGN KEY (`apetito_id`) REFERENCES `apetito` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_ANAMNESIS_CITA` FOREIGN KEY (`cita_id`) REFERENCES `cita` (`cita_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_ANAMNESIS_VOLUMEN` FOREIGN KEY (`volumen_consumido_id`) REFERENCES `volumen_consumido` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla macadb.anamnesis: ~57 rows (aproximadamente)
/*!40000 ALTER TABLE `anamnesis` DISABLE KEYS */;
INSERT INTO `anamnesis` (`anamnesis_id`, `cita_id`, `analisis_ingesta`, `apetito_id`, `cambios_ingesta`, `alimentos_preferidos`, `alimentos_rechazados`, `intolerancias`, `volumen_consumido_id`, `tiempo_empleado`, `uso_sal`) VALUES
	(1, 1, 'ingesta bien', 1, 'N', 'todos', 'mondongo', 'ninguna', 3, NULL, NULL),
	(2, 121, '', 1, '', '', '', '', 3, NULL, NULL),
	(3, 122, '', 1, '', '', '', '', 3, NULL, NULL),
	(4, 123, '', 1, '', '', '', '', 3, NULL, NULL),
	(5, 125, '', 1, '', '', '', '', 3, NULL, NULL),
	(6, 127, '', 1, '', '', '', '', 3, NULL, NULL),
	(7, 128, '', 1, '', '', '', '', 3, NULL, NULL),
	(8, 129, '', 1, '', '', '', '', 3, NULL, NULL),
	(9, 130, '', 1, '', '', '', '', 3, NULL, NULL),
	(10, 131, '', 1, '', '', '', '', 3, NULL, NULL),
	(11, 219, '', 1, '', '', '', '', 1, '', NULL),
	(12, 220, '', 1, '', '', '', '', 1, '', NULL),
	(13, 221, '', 1, '', '', '', '', 1, '', NULL),
	(14, 222, '', 1, '', '', '', '', 1, '', NULL),
	(15, 223, '', 1, '', '', '', '', 1, '', NULL),
	(16, 224, '', 1, '', '', '', '', 1, '', NULL),
	(17, 225, '', 1, '', '', '', '', 1, '', NULL),
	(18, 226, '', 1, '', '', '', '', 1, '', NULL),
	(19, 227, '', 1, '', '', '', '', 1, '', NULL),
	(20, 228, '', 1, '', '', '', '', 1, '', NULL),
	(21, 229, '', 1, '', '', '', '', 1, '', NULL),
	(22, 230, '', 1, '', '', '', '', 1, '', NULL),
	(23, 241, '', 1, '', '', '', '', 1, '', NULL),
	(24, 242, '', 1, '', '', '', '', 1, '', NULL),
	(25, 243, 'De una vez el analisis de ingesta', 1, '', '', '', '', 1, '', NULL),
	(26, 244, '', 1, '', '', '', '', 1, '', NULL),
	(27, 245, '', 1, '', '', '', '', 1, '', NULL),
	(28, 245, '', 1, '', '', '', '', 1, '', NULL),
	(29, 246, '', 1, '', '', '', '', 1, '', NULL),
	(30, 247, '', 1, '', '', '', '', 1, '', NULL),
	(31, 248, '', 1, '', '', '', '', 1, '', NULL),
	(32, 249, '', 1, '', '', '', '', 1, '', NULL),
	(33, 249, '', 1, '', '', '', '', 1, '', NULL),
	(34, 250, '', 1, '', '', '', '', 1, '', NULL),
	(35, 251, '', 1, '', '', '', '', 1, '', NULL),
	(36, 251, '', 1, '', '', '', '', 1, '', NULL),
	(37, 252, '', 1, '', '', '', '', 1, '', NULL),
	(38, 253, '', 1, '', '', '', '', 1, '', NULL),
	(39, 254, '', 1, '', '', '', '', 1, '', NULL),
	(40, 254, '', 1, '', '', '', '', 1, '', NULL),
	(41, 255, '', 1, '', '', '', '', 1, '', NULL),
	(42, 256, '', 1, '', '', '', '', 1, '', NULL),
	(43, 258, '', 1, '', '', '', '', 1, '', NULL),
	(44, 259, '', 1, '', '', '', '', 1, '', NULL),
	(45, 259, '', 1, '', '', '', '', 1, '', NULL),
	(46, 260, '', 1, '', '', '', '', 1, '', NULL),
	(47, 260, '', 1, '', '', '', '', 1, '', NULL),
	(48, 261, '', 1, '', '', '', '', 1, '', NULL),
	(49, 267, '', 1, '', '', '', '', 1, '', NULL),
	(50, 271, 'no se esto que es ', 3, 'N', 'sopa, changua, maracuya', 'cerdo', 'ninguna', 4, '30minutos', 0),
	(51, 272, '', 1, '', '', '', '', 1, '', NULL),
	(52, 273, '', 1, '', '', '', '', 1, '', NULL),
	(53, 274, '', 1, '', '', '', '', 1, '', NULL),
	(54, 275, '', 1, '', '', '', '', 1, '', NULL),
	(55, 276, '', 1, '', '', '', '', 1, '', NULL),
	(56, 277, '', 1, '', '', '', '', 1, '', NULL),
	(57, 297, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(58, 298, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `anamnesis` ENABLE KEYS */;


-- Volcando estructura para tabla macadb.antecedentes
DROP TABLE IF EXISTS `antecedentes`;
CREATE TABLE IF NOT EXISTS `antecedentes` (
  `tipo_antecedente_id` tinyint(4) NOT NULL,
  `persona_id` int(11) NOT NULL,
  `descripcion` varchar(500) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`tipo_antecedente_id`,`persona_id`),
  KEY `FK_persona` (`persona_id`),
  CONSTRAINT `FK_persona` FOREIGN KEY (`persona_id`) REFERENCES `persona` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_tipo_antecedente` FOREIGN KEY (`tipo_antecedente_id`) REFERENCES `tipo_antecedente` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla macadb.antecedentes: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `antecedentes` DISABLE KEYS */;
INSERT INTO `antecedentes` (`tipo_antecedente_id`, `persona_id`, `descripcion`) VALUES
	(1, 1, 'aca el antecedente patologico'),
	(1, 9, 'Antecedente pato para prueba'),
	(1, 12, 'Ninguno'),
	(2, 9, 'Antecedente quiru para prueba'),
	(2, 12, 'alguno'),
	(3, 9, 'Antecedente fam para prueba'),
	(3, 12, 'otros');
/*!40000 ALTER TABLE `antecedentes` ENABLE KEYS */;


-- Volcando estructura para tabla macadb.antropometria
DROP TABLE IF EXISTS `antropometria`;
CREATE TABLE IF NOT EXISTS `antropometria` (
  `cita_id` int(11) NOT NULL,
  `peso` float DEFAULT NULL,
  `talla` float DEFAULT NULL,
  `cintura` float DEFAULT NULL,
  `cadera` float DEFAULT NULL,
  `grasa_objetivo` float DEFAULT NULL,
  `carpo` float DEFAULT NULL,
  `circ_pantorilla` float DEFAULT NULL,
  `circ_brazo` float DEFAULT NULL,
  `circ_muscular` float DEFAULT NULL,
  `pgraso` float DEFAULT NULL,
  `plgraso` float DEFAULT NULL,
  `imc` float DEFAULT NULL,
  `grasa_actual` float DEFAULT NULL,
  `triceps` float DEFAULT NULL,
  `peso_ideal_estructura_osea` float DEFAULT NULL,
  `interpretacion_imc` varchar(250) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`cita_id`),
  CONSTRAINT `FK_cita` FOREIGN KEY (`cita_id`) REFERENCES `cita` (`cita_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla macadb.antropometria: ~37 rows (aproximadamente)
/*!40000 ALTER TABLE `antropometria` DISABLE KEYS */;
INSERT INTO `antropometria` (`cita_id`, `peso`, `talla`, `cintura`, `cadera`, `grasa_objetivo`, `carpo`, `circ_pantorilla`, `circ_brazo`, `circ_muscular`, `pgraso`, `plgraso`, `imc`, `grasa_actual`, `triceps`, `peso_ideal_estructura_osea`, `interpretacion_imc`) VALUES
	(1, 70, 180, 60, 65, 2, 32, 12, 12, 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(108, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(109, 70, 170, 23, 43, 34, 23, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(121, 120, 170, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(122, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(123, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(124, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(125, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(126, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(127, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(128, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(129, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(130, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(131, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(219, 80, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(220, 79, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(221, 72, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(222, 90, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(223, 78, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(224, 80, 1.92, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(225, 73.9, 1.92, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(226, 80.2, 1.92, 30, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(227, 73.2, 1.73, 65, 69, 3, 90, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(228, 80.3, 1.92, 65.8, 32.2, NULL, 90, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(229, 78.9, 1.92, 65.8, 32.2, 3, 90, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(230, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(239, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(240, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(241, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(242, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(243, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(244, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(245, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(267, 64.9, 1.68, 75, 100, NULL, 14.3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(271, 55, 1.7, 64, 90, 8, 88, NULL, NULL, NULL, 2.75, 52.25, 19.03, 5, NULL, NULL, NULL),
	(297, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(298, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `antropometria` ENABLE KEYS */;


-- Volcando estructura para tabla macadb.apetito
DROP TABLE IF EXISTS `apetito`;
CREATE TABLE IF NOT EXISTS `apetito` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla macadb.apetito: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `apetito` DISABLE KEYS */;
INSERT INTO `apetito` (`id`, `nombre`) VALUES
	(1, 'Moderado'),
	(2, 'Muy Bajo'),
	(3, 'Bajo'),
	(4, 'Aumentado'),
	(5, 'Muy Aumentado');
/*!40000 ALTER TABLE `apetito` ENABLE KEYS */;


-- Volcando estructura para tabla macadb.circunferencias
DROP TABLE IF EXISTS `circunferencias`;
CREATE TABLE IF NOT EXISTS `circunferencias` (
  `id` tinyint(4) NOT NULL,
  `nombre` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `readonly` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla macadb.circunferencias: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `circunferencias` DISABLE KEYS */;
INSERT INTO `circunferencias` (`id`, `nombre`, `readonly`) VALUES
	(1, 'Relación cintura - cadera', 1),
	(2, 'Estructura', 1),
	(3, 'Circunferencia de pantorrila (cm)', 0),
	(4, 'Circunferencia del brazo (mm)', 0),
	(5, 'Circunferencia muscular del brazo(cm)', 1),
	(6, 'Cintura (cm)', 0),
	(7, 'Relación Cintura - talla', 0);
/*!40000 ALTER TABLE `circunferencias` ENABLE KEYS */;


-- Volcando estructura para tabla macadb.cita
DROP TABLE IF EXISTS `cita`;
CREATE TABLE IF NOT EXISTS `cita` (
  `cita_id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_hora` datetime DEFAULT NULL,
  `examen_fisico` varchar(500) COLLATE latin1_spanish_ci DEFAULT NULL,
  `persona_id` int(11) DEFAULT NULL,
  `diagnostico` varchar(2000) COLLATE latin1_spanish_ci DEFAULT NULL,
  `persona_medico` int(11) DEFAULT NULL,
  `motivo` varchar(5000) COLLATE latin1_spanish_ci DEFAULT NULL,
  `objetivos_nutricionales` varchar(5000) COLLATE latin1_spanish_ci DEFAULT NULL,
  `plan_nutricional` varchar(5000) COLLATE latin1_spanish_ci DEFAULT NULL,
  `recomendaciones` varchar(5000) COLLATE latin1_spanish_ci DEFAULT NULL,
  `f_actividad` float DEFAULT NULL,
  PRIMARY KEY (`cita_id`),
  KEY `FK_CITA_PERSONA` (`persona_id`),
  KEY `FK_CITA_MEDICO` (`persona_medico`),
  CONSTRAINT `FK_CITA_MEDICO` FOREIGN KEY (`persona_medico`) REFERENCES `persona` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_CITA_PERSONA` FOREIGN KEY (`persona_id`) REFERENCES `persona` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=300 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla macadb.cita: ~295 rows (aproximadamente)
/*!40000 ALTER TABLE `cita` DISABLE KEYS */;
INSERT INTO `cita` (`cita_id`, `fecha_hora`, `examen_fisico`, `persona_id`, `diagnostico`, `persona_medico`, `motivo`, `objetivos_nutricionales`, `plan_nutricional`, `recomendaciones`, `f_actividad`) VALUES
	(1, '2017-11-25 18:09:33', 'Contextura coporal gruesa', 1, '', 1, NULL, NULL, 'toca plan', 'recom ', NULL),
	(2, '2017-11-29 21:35:52', NULL, 1, NULL, 1, NULL, NULL, 'otro', 'recomendado', NULL),
	(3, '2017-12-03 20:24:39', NULL, 1, NULL, 1, NULL, NULL, 'daddasda', 'dasdadasd', NULL),
	(4, '2017-12-03 21:02:25', NULL, 1, NULL, 1, NULL, NULL, 'rweqweqw', 'eqweqewq', NULL),
	(5, '2017-12-03 21:02:52', NULL, 1, NULL, 1, NULL, NULL, 'ewqeqwe', 'ewqewqeqwe', NULL),
	(6, '2017-12-03 21:10:06', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(7, '2017-12-03 21:42:23', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(8, '2017-12-03 21:42:39', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(9, '2017-12-03 21:43:09', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(10, '2017-12-03 21:43:39', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(11, '2017-12-03 21:44:05', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(12, '2017-12-03 21:45:05', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(13, '2017-12-03 21:58:58', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(14, '2017-12-03 22:17:30', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(15, '2017-12-03 22:21:42', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(16, '2017-12-03 22:22:15', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(17, '2017-12-03 22:23:25', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(18, '2017-12-03 22:24:40', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(19, '2017-12-03 22:25:53', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(20, '2017-12-03 22:26:18', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(21, '2017-12-03 22:26:59', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(22, '2017-12-03 22:28:11', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(23, '2017-12-03 22:30:22', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(24, '2017-12-03 23:03:58', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(25, '2017-12-03 23:04:35', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(26, '2017-12-03 23:05:38', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(27, '2017-12-03 23:09:00', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(28, '2017-12-03 23:09:49', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(29, '2017-12-03 23:13:00', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(30, '2017-12-03 23:14:14', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(31, '2017-12-03 23:14:52', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(32, '2017-12-03 23:17:25', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(33, '2017-12-03 23:17:53', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(34, '2017-12-03 23:35:45', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(35, '2017-12-03 23:35:53', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(36, '2017-12-09 00:18:38', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(37, '2017-12-09 00:32:03', NULL, 1, NULL, 3, NULL, NULL, NULL, NULL, NULL),
	(38, '2017-12-09 00:32:25', NULL, 1, NULL, 6, NULL, NULL, NULL, NULL, NULL),
	(39, '2017-12-09 00:39:46', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(40, '2017-12-09 00:43:20', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(41, '2017-12-09 21:35:07', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(42, '2017-12-09 21:54:04', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(43, '2017-12-09 21:55:34', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(44, '2017-12-09 22:05:01', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(45, '2017-12-09 22:08:30', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(46, '2017-12-09 22:10:16', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(47, '2017-12-09 22:11:16', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(48, '2017-12-09 22:13:24', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(49, '2017-12-09 22:14:25', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(50, '2017-12-09 22:15:16', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(51, '2017-12-09 22:15:51', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(52, '2017-12-09 22:17:09', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(53, '2017-12-09 22:18:37', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(54, '2017-12-09 22:19:01', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(55, '2017-12-09 22:28:43', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(56, '2017-12-09 22:29:26', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(57, '2017-12-09 22:29:56', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(58, '2017-12-09 22:33:08', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(59, '2017-12-09 22:34:27', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(60, '2017-12-09 22:34:42', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(61, '2017-12-09 22:35:14', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(62, '2017-12-09 22:35:59', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(63, '2017-12-09 22:42:24', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(64, '2017-12-09 22:43:35', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(65, '2017-12-09 22:43:50', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(66, '2017-12-09 22:44:55', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(67, '2017-12-09 22:45:21', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(68, '2017-12-09 22:46:15', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(69, '2017-12-09 22:53:42', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(70, '2017-12-09 22:55:20', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(71, '2017-12-09 22:55:34', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(72, '2017-12-09 22:55:45', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(73, '2017-12-09 22:56:56', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(74, '2017-12-09 22:57:20', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(75, '2017-12-09 22:58:01', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(76, '2017-12-09 22:58:24', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(77, '2017-12-09 22:58:30', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(78, '2017-12-09 23:01:36', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(79, '2017-12-09 23:04:05', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(80, '2017-12-09 23:04:37', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(81, '2017-12-09 23:04:53', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(82, '2017-12-09 23:05:28', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(83, '2017-12-09 23:08:28', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(84, '2017-12-09 23:12:55', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(85, '2017-12-09 23:14:09', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(86, '2017-12-09 23:14:34', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(87, '2017-12-09 23:14:49', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(88, '2017-12-09 23:15:29', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(89, '2017-12-09 23:15:57', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(90, '2017-12-09 23:17:34', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(91, '2017-12-09 23:18:26', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(92, '2017-12-09 23:22:55', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(93, '2017-12-09 23:23:46', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(94, '2017-12-09 23:24:49', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(95, '2017-12-09 23:25:04', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(96, '2017-12-09 23:26:05', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(97, '2017-12-09 23:30:03', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(98, '2017-12-09 23:31:33', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(99, '2017-12-09 23:32:00', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(100, '2017-12-09 23:32:38', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(101, '2017-12-09 23:39:38', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(102, '2017-12-09 23:40:29', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(103, '2017-12-09 23:41:56', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(104, '2017-12-09 23:51:50', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(105, '2017-12-10 00:00:07', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(106, '2017-12-10 00:02:56', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(107, '2017-12-10 00:09:24', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(108, '2017-12-10 00:15:01', 'swswswsw', 1, 'AAAAAAAAA', 1, NULL, NULL, NULL, NULL, NULL),
	(109, '2017-12-10 01:11:59', 'Examen malito', 1, 'este es el diagnostico', 1, NULL, NULL, NULL, NULL, NULL),
	(110, '2017-12-10 01:40:06', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(111, '2017-12-10 01:41:50', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(112, '2017-12-10 01:42:57', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(113, '2017-12-10 01:43:28', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(114, '2017-12-10 01:44:16', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(115, '2017-12-11 21:45:23', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(116, '2017-12-11 22:20:58', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(117, '2017-12-13 22:35:16', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(118, '2017-12-19 22:59:59', NULL, 6, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(119, '2017-12-19 23:00:50', NULL, 6, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(120, '2017-12-19 23:09:04', NULL, 8, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(121, '2017-12-20 22:12:32', '', 9, '', 1, NULL, NULL, NULL, NULL, NULL),
	(122, '2017-12-20 22:18:36', '', 9, '', 1, NULL, NULL, NULL, NULL, NULL),
	(123, '2017-12-20 22:32:26', '', 9, '', 1, NULL, NULL, NULL, NULL, NULL),
	(124, '2017-12-20 22:36:34', '', 9, '', 1, NULL, NULL, NULL, NULL, NULL),
	(125, '2017-12-20 22:37:38', '', 9, '', 1, NULL, NULL, NULL, NULL, NULL),
	(126, '2017-12-20 22:40:08', '', 9, '', 1, NULL, NULL, NULL, NULL, NULL),
	(127, '2017-12-20 22:41:29', '', 9, '', 1, NULL, NULL, NULL, NULL, NULL),
	(128, '2017-12-20 22:44:08', '', 9, '', 1, NULL, NULL, NULL, NULL, NULL),
	(129, '2017-12-20 22:47:45', '', 9, '', 1, NULL, NULL, NULL, NULL, NULL),
	(130, '2017-12-20 22:50:34', '', 9, '', 1, NULL, NULL, NULL, NULL, NULL),
	(131, '2017-12-20 22:52:49', '', 9, '', 1, NULL, NULL, NULL, NULL, NULL),
	(132, '2017-12-26 21:59:53', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(133, '2017-12-26 22:09:00', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(134, '2017-12-26 22:11:34', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(135, '2017-12-26 22:12:48', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(136, '2017-12-26 22:14:35', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(137, '2017-12-26 23:40:58', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(138, '2017-12-26 23:41:36', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(139, '2017-12-26 23:41:51', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(140, '2017-12-26 23:42:06', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(141, '2017-12-26 23:45:08', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(142, '2017-12-26 23:45:28', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(143, '2017-12-26 23:47:17', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(144, '2017-12-26 23:53:00', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(145, '2017-12-26 23:54:11', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(146, '2017-12-26 23:55:42', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(147, '2017-12-26 23:58:09', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(148, '2017-12-27 00:00:39', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(149, '2017-12-27 00:05:27', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(150, '2017-12-27 00:06:01', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(151, '2017-12-28 19:31:53', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(152, '2017-12-28 19:39:59', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(153, '2017-12-28 19:40:26', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(154, '2017-12-28 19:41:59', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(155, '2017-12-28 19:44:15', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(156, '2017-12-28 19:49:39', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(157, '2017-12-28 19:50:19', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(158, '2017-12-28 19:54:29', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(159, '2017-12-28 19:56:03', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(160, '2017-12-28 19:57:12', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(161, '2017-12-28 20:19:23', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(162, '2017-12-28 20:19:46', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(163, '2017-12-28 20:20:09', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(164, '2017-12-28 20:20:54', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(165, '2017-12-28 20:22:04', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(166, '2017-12-28 20:23:33', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(167, '2017-12-28 20:24:04', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(168, '2017-12-28 20:24:25', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(169, '2017-12-28 20:28:51', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(170, '2017-12-28 20:29:19', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(171, '2017-12-28 20:30:44', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(172, '2017-12-28 23:14:24', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(173, '2017-12-28 23:15:55', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(174, '2017-12-28 23:16:35', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(175, '2017-12-28 23:17:16', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(176, '2017-12-28 23:18:28', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(177, '2017-12-28 23:22:16', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(178, '2017-12-28 23:24:04', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(179, '2017-12-28 23:26:23', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(180, '2017-12-28 23:28:07', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(181, '2017-12-28 23:33:22', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(182, '2017-12-28 23:34:46', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(183, '2017-12-28 23:35:12', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(184, '2017-12-28 23:35:21', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(185, '2017-12-28 23:35:52', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(186, '2017-12-28 23:39:23', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(187, '2017-12-28 23:40:59', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(188, '2017-12-28 23:45:41', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(189, '2017-12-28 23:46:05', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(190, '2017-12-28 23:46:54', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(191, '2017-12-29 09:43:11', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(192, '2017-12-29 09:45:00', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(193, '2017-12-29 17:57:14', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(194, '2017-12-29 18:39:04', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(195, '2017-12-29 18:41:10', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(196, '2017-12-29 18:44:02', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(197, '2017-12-29 18:44:26', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(198, '2017-12-29 18:44:57', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(199, '2017-12-29 18:45:32', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(200, '2017-12-29 18:45:59', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(201, '2017-12-29 18:46:13', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(202, '2017-12-29 18:46:29', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(203, '2017-12-29 18:47:04', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(204, '2017-12-29 18:47:46', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(205, '2017-12-29 18:48:25', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(206, '2017-12-29 18:49:25', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(207, '2017-12-29 18:50:05', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(208, '2017-12-29 18:50:15', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(209, '2017-12-29 18:50:44', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(210, '2017-12-29 18:50:53', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(211, '2017-12-29 19:04:03', NULL, 1, NULL, 1, 'motivos aca', 'objetivos por aca', NULL, NULL, NULL),
	(212, '2018-01-02 07:52:03', NULL, 1, NULL, 1, '', '', NULL, NULL, NULL),
	(213, '2018-01-07 22:35:25', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(218, '2018-01-07 22:48:33', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(219, '2018-01-07 22:49:06', '', 10, '', 1, 'Estos son los motivos ', 'estos son los objetivos', NULL, NULL, NULL),
	(220, '2018-01-07 22:57:23', '', 10, '', 1, '', '', NULL, NULL, NULL),
	(221, '2018-01-07 23:10:53', '', 10, '', 1, '', '', NULL, NULL, NULL),
	(222, '2018-01-07 23:14:16', '', 1, '', 1, '', '', NULL, NULL, NULL),
	(223, '2018-01-07 23:16:37', '', 10, '', 1, '', '', NULL, NULL, NULL),
	(224, '2018-01-07 23:18:22', '', 9, '', 1, '', '', NULL, NULL, NULL),
	(225, '2018-01-07 23:19:48', '', 1, '', 1, '', '', NULL, NULL, NULL),
	(226, '2018-01-07 23:20:41', '', 1, '', 1, '', '', NULL, NULL, NULL),
	(227, '2018-01-07 23:27:11', '', 9, '', 1, '', '', NULL, NULL, NULL),
	(228, '2018-01-07 23:31:16', '', 1, '', 1, '', '', NULL, NULL, NULL),
	(229, '2018-01-07 23:42:16', '', 1, '', 1, '', '', NULL, NULL, NULL),
	(230, '2018-01-07 23:46:03', '', 9, '', 1, '', '', NULL, NULL, NULL),
	(231, '2018-01-07 23:53:23', NULL, 1, NULL, 1, '', '', NULL, NULL, NULL),
	(232, '2018-01-08 00:37:18', NULL, 1, NULL, 1, '', '', NULL, NULL, NULL),
	(233, '2018-01-08 00:47:12', NULL, 1, NULL, 1, '', '', NULL, NULL, NULL),
	(234, '2018-01-08 01:22:28', NULL, 1, NULL, 1, '', '', NULL, NULL, NULL),
	(235, '2018-01-08 01:24:57', NULL, 1, NULL, 1, '', '', NULL, NULL, NULL),
	(236, '2018-01-08 01:28:48', NULL, 1, NULL, 1, '', '', NULL, NULL, NULL),
	(237, '2018-01-08 13:24:29', NULL, 1, NULL, 1, '', '', NULL, NULL, NULL),
	(238, '2018-01-08 13:25:30', NULL, 1, NULL, 1, '', '', NULL, NULL, NULL),
	(239, '2018-01-08 15:53:26', '', 9, '', 1, '', '', NULL, NULL, NULL),
	(240, '2018-01-08 16:31:46', '', 1, '', 1, '', '', NULL, NULL, NULL),
	(241, '2018-01-08 16:35:39', '', 1, '', 1, '', '', NULL, NULL, NULL),
	(242, '2018-01-08 16:40:02', '', 1, '', 1, '', '', NULL, NULL, NULL),
	(243, '2018-01-08 16:42:59', 'Texto del examen fisico', 9, '', 1, '', '', NULL, NULL, NULL),
	(244, '2018-01-08 16:45:34', '', 1, '', 1, '', '', NULL, NULL, NULL),
	(245, '2018-01-08 17:25:49', '', 1, '', 1, '', '', NULL, NULL, NULL),
	(246, '2018-01-08 17:39:01', '', 1, '', 1, '', '', NULL, NULL, NULL),
	(247, '2018-01-08 17:43:06', '', 1, '', 1, '', '', NULL, NULL, NULL),
	(248, '2018-01-08 17:46:14', '', 1, '', 1, '', '', NULL, NULL, NULL),
	(249, '2018-01-08 17:47:14', '', 1, '', 1, '', '', NULL, NULL, NULL),
	(250, '2018-01-08 17:51:47', '', 1, '', 1, '', '', NULL, NULL, NULL),
	(251, '2018-01-08 17:55:02', '', 1, '', 1, '', '', NULL, NULL, NULL),
	(252, '2018-01-08 18:04:20', '', 1, '', 1, '', '', NULL, NULL, NULL),
	(253, '2018-01-08 18:10:37', '', 1, '', 1, '', '', NULL, NULL, NULL),
	(254, '2018-01-08 18:13:18', '', 1, '', 1, '', 'solo con objetivos', NULL, NULL, NULL),
	(255, '2018-01-08 18:38:14', '', 9, '', 1, '', '', NULL, NULL, NULL),
	(256, '2018-01-08 18:48:58', '', 9, '', 1, '', '', NULL, NULL, NULL),
	(257, '2018-01-08 18:55:50', NULL, 9, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(258, '2018-01-08 20:50:43', '', 1, '', 1, '', '', NULL, NULL, NULL),
	(259, '2018-01-08 21:00:06', '', 1, '', 1, '', '', NULL, NULL, NULL),
	(260, '2018-01-08 21:01:44', '', 1, '', 1, '', '', NULL, NULL, NULL),
	(261, '2018-01-08 21:03:59', '', 1, '', 1, '', '', NULL, NULL, NULL),
	(262, '2018-01-08 21:20:53', NULL, 1, NULL, 1, '', '', NULL, NULL, NULL),
	(263, '2018-01-13 18:32:24', NULL, 1, NULL, 1, '', '', NULL, NULL, NULL),
	(264, '2018-01-13 22:30:12', NULL, 9, NULL, 1, '', '', NULL, NULL, NULL),
	(265, '2018-01-13 22:46:49', NULL, 11, NULL, 1, '', '', 'edadad', 'eqwdqwdqwd', NULL),
	(266, '2018-01-14 19:43:35', NULL, 1, NULL, 1, 'son motivos', 'son objetivos', NULL, NULL, NULL),
	(267, '2018-01-15 20:41:50', '', 1, '', 1, '', '', NULL, NULL, NULL),
	(268, '2018-01-16 21:24:15', NULL, 1, NULL, 1, '', '', NULL, NULL, NULL),
	(269, '2018-01-16 22:01:06', NULL, 1, NULL, 1, '', '', NULL, NULL, NULL),
	(270, '2018-01-16 22:29:49', NULL, 1, NULL, 1, '', '', NULL, NULL, NULL),
	(271, '2018-01-17 22:06:49', 'Responde bien a los estimulos', 12, 'Presenta una buena intención de seguir la dieta.', 1, 'Quiere bajar de peso', 'bajar 3 kilos', 'El plan', 'Reco', NULL),
	(272, '2018-01-17 23:01:33', '', 10, '', 1, '', '', NULL, NULL, NULL),
	(273, '2018-01-17 23:11:20', '', 10, '', 1, '', '', NULL, NULL, NULL),
	(274, '2018-01-17 23:13:49', '', 10, '', 1, '', '', NULL, NULL, NULL),
	(275, '2018-01-20 18:33:46', '', 1, '', 1, '', '', NULL, NULL, NULL),
	(276, '2018-01-20 18:39:39', '', 1, '', 1, '', '', NULL, NULL, NULL),
	(277, '2018-01-20 18:41:44', '', 9, '', 1, '', '', NULL, NULL, NULL),
	(278, '2018-01-20 18:59:11', NULL, 9, NULL, 1, '', '', NULL, NULL, NULL),
	(279, '2018-01-22 21:25:10', NULL, 9, NULL, 1, '', '', NULL, NULL, NULL),
	(280, '2018-01-29 20:06:30', NULL, 1, NULL, 1, '', '', NULL, NULL, NULL),
	(281, '2018-01-29 20:19:25', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(282, '2018-02-03 08:26:24', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(283, '2018-02-03 09:03:01', NULL, 13, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(284, '2018-02-03 09:09:13', NULL, 14, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(285, '2018-02-03 09:13:47', NULL, 15, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(286, '2018-02-03 09:19:24', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(287, '2018-02-03 09:22:14', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(288, '2018-02-03 09:24:28', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(289, '2018-02-03 09:24:37', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(290, '2018-02-03 09:25:07', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(291, '2018-02-03 09:25:16', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(292, '2018-02-03 09:25:46', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(293, '2018-02-03 09:26:09', NULL, 1, NULL, 1, '', '', NULL, NULL, NULL),
	(294, '2018-02-03 09:26:52', NULL, 9, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(295, '2018-02-03 09:27:14', NULL, 9, NULL, 1, NULL, NULL, NULL, NULL, NULL),
	(296, '2018-02-03 09:31:46', NULL, 9, NULL, 1, 'motivo ', 'objetivo', NULL, NULL, NULL),
	(297, '2018-02-03 10:29:27', NULL, 1, NULL, 1, '', '', NULL, NULL, NULL),
	(298, '2018-02-03 11:30:14', NULL, 1, NULL, 1, '', '', NULL, NULL, NULL),
	(299, '2018-02-03 11:53:20', NULL, 1, NULL, 1, '', '', NULL, NULL, NULL);
/*!40000 ALTER TABLE `cita` ENABLE KEYS */;


-- Volcando estructura para tabla macadb.conductas_compensatorias
DROP TABLE IF EXISTS `conductas_compensatorias`;
CREATE TABLE IF NOT EXISTS `conductas_compensatorias` (
  `id` tinyint(4) NOT NULL,
  `conducta` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla macadb.conductas_compensatorias: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `conductas_compensatorias` DISABLE KEYS */;
INSERT INTO `conductas_compensatorias` (`id`, `conducta`) VALUES
	(1, 'Vómitos'),
	(2, 'Laxantes'),
	(3, 'Diuréticos'),
	(4, 'Fármaco para adelgazar'),
	(5, 'Consumo excesivo de agua');
/*!40000 ALTER TABLE `conductas_compensatorias` ENABLE KEYS */;


-- Volcando estructura para tabla macadb.conducta_cita
DROP TABLE IF EXISTS `conducta_cita`;
CREATE TABLE IF NOT EXISTS `conducta_cita` (
  `cita_id` int(11) NOT NULL,
  `conducta_id` tinyint(4) NOT NULL,
  `posee_conducta` tinyint(4) DEFAULT NULL,
  `frecuencia` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`cita_id`,`conducta_id`),
  KEY `FK_conducta_conducta` (`conducta_id`),
  CONSTRAINT `FK_conducta_cita` FOREIGN KEY (`cita_id`) REFERENCES `cita` (`cita_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_conducta_conducta` FOREIGN KEY (`conducta_id`) REFERENCES `conductas_compensatorias` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla macadb.conducta_cita: ~45 rows (aproximadamente)
/*!40000 ALTER TABLE `conducta_cita` DISABLE KEYS */;
INSERT INTO `conducta_cita` (`cita_id`, `conducta_id`, `posee_conducta`, `frecuencia`) VALUES
	(261, 1, 1, 'carreta3'),
	(261, 2, 0, ''),
	(261, 3, 1, 'carreta2'),
	(261, 4, 0, ''),
	(261, 5, 1, 'carreta'),
	(267, 1, 0, ''),
	(267, 2, 0, ''),
	(267, 3, 0, ''),
	(267, 4, 0, ''),
	(267, 5, 0, ''),
	(271, 1, 0, ''),
	(271, 2, 0, ''),
	(271, 3, 0, ''),
	(271, 4, 0, ''),
	(271, 5, 1, 'litro diario'),
	(272, 1, 0, ''),
	(272, 2, 0, ''),
	(272, 3, 0, ''),
	(272, 4, 0, ''),
	(272, 5, 0, ''),
	(273, 1, 0, ''),
	(273, 2, 0, ''),
	(273, 3, 0, ''),
	(273, 4, 0, ''),
	(273, 5, 0, ''),
	(274, 1, 0, ''),
	(274, 2, 0, ''),
	(274, 3, 0, ''),
	(274, 4, 0, ''),
	(274, 5, 0, ''),
	(275, 1, 0, ''),
	(275, 2, 0, ''),
	(275, 3, 0, ''),
	(275, 4, 0, ''),
	(275, 5, 0, ''),
	(276, 1, 0, ''),
	(276, 2, 0, ''),
	(276, 3, 0, ''),
	(276, 4, 0, ''),
	(276, 5, 0, ''),
	(277, 1, 0, ''),
	(277, 2, 0, ''),
	(277, 3, 0, ''),
	(277, 4, 0, ''),
	(277, 5, 0, '');
/*!40000 ALTER TABLE `conducta_cita` ENABLE KEYS */;


-- Volcando estructura para tabla macadb.estado_civil
DROP TABLE IF EXISTS `estado_civil`;
CREATE TABLE IF NOT EXISTS `estado_civil` (
  `id` tinyint(4) NOT NULL,
  `nombre` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla macadb.estado_civil: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `estado_civil` DISABLE KEYS */;
INSERT INTO `estado_civil` (`id`, `nombre`) VALUES
	(1, 'Soltero'),
	(2, 'Casado'),
	(3, 'Divorciado');
/*!40000 ALTER TABLE `estado_civil` ENABLE KEYS */;


-- Volcando estructura para tabla macadb.estado_psicosocial
DROP TABLE IF EXISTS `estado_psicosocial`;
CREATE TABLE IF NOT EXISTS `estado_psicosocial` (
  `cita_id` int(11) NOT NULL,
  `quien_vive` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL COMMENT 'Quien vive con usted?',
  `relaciones_amistad` int(11) DEFAULT NULL COMMENT '¿Tiene relaciones de amistad?',
  PRIMARY KEY (`cita_id`),
  CONSTRAINT `FK_psicosocial_cita` FOREIGN KEY (`cita_id`) REFERENCES `cita` (`cita_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla macadb.estado_psicosocial: ~14 rows (aproximadamente)
/*!40000 ALTER TABLE `estado_psicosocial` DISABLE KEYS */;
INSERT INTO `estado_psicosocial` (`cita_id`, `quien_vive`, `relaciones_amistad`) VALUES
	(1, 'Esposa', 1),
	(258, '', NULL),
	(259, '', NULL),
	(261, 'FLORIDA', 0),
	(267, '', NULL),
	(271, 'Esposo', 1),
	(272, '', NULL),
	(273, '', NULL),
	(274, '', NULL),
	(275, '', NULL),
	(276, '', NULL),
	(277, '', NULL),
	(297, NULL, NULL),
	(298, NULL, NULL);
/*!40000 ALTER TABLE `estado_psicosocial` ENABLE KEYS */;


-- Volcando estructura para tabla macadb.examen
DROP TABLE IF EXISTS `examen`;
CREATE TABLE IF NOT EXISTS `examen` (
  `examen_id` int(11) NOT NULL,
  `cita_id` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `valor` decimal(10,0) DEFAULT NULL,
  `interpretacion` varchar(120) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`examen_id`,`cita_id`),
  KEY `FK_examen_cita` (`cita_id`),
  CONSTRAINT `FK_examen_cita` FOREIGN KEY (`cita_id`) REFERENCES `cita` (`cita_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla macadb.examen: ~20 rows (aproximadamente)
/*!40000 ALTER TABLE `examen` DISABLE KEYS */;
INSERT INTO `examen` (`examen_id`, `cita_id`, `fecha`, `valor`, `interpretacion`) VALUES
	(0, 298, NULL, NULL, NULL),
	(1, 1, '2017-12-11', 30, 'Alto'),
	(1, 131, NULL, 20, 'Nuevo'),
	(1, 219, NULL, NULL, ''),
	(1, 220, NULL, NULL, ''),
	(1, 221, NULL, NULL, ''),
	(1, 222, NULL, NULL, ''),
	(1, 223, NULL, NULL, ''),
	(1, 224, NULL, NULL, ''),
	(1, 225, NULL, NULL, ''),
	(1, 226, NULL, NULL, ''),
	(1, 227, NULL, NULL, ''),
	(1, 228, NULL, NULL, ''),
	(1, 229, NULL, NULL, ''),
	(1, 239, NULL, 12, 'Nivel bajo, '),
	(1, 240, NULL, 12, 'bajo'),
	(1, 242, NULL, 1, 'bajo cita 1'),
	(2, 130, NULL, 898, 'MAs'),
	(2, 131, NULL, 900, 'mas nuevo'),
	(2, 239, NULL, 4, 'lo tiene alto'),
	(2, 240, NULL, 14, 'alto'),
	(2, 242, NULL, 2, 'medio cita 1'),
	(2, 243, '2018-01-13', 12, 'muy alto'),
	(3, 271, '2018-01-03', 3, 'No se');
/*!40000 ALTER TABLE `examen` ENABLE KEYS */;


-- Volcando estructura para tabla macadb.examen_referencia
DROP TABLE IF EXISTS `examen_referencia`;
CREATE TABLE IF NOT EXISTS `examen_referencia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `valor_referencia_bajo` decimal(10,0) DEFAULT NULL,
  `valor_referencia_superior` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla macadb.examen_referencia: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `examen_referencia` DISABLE KEYS */;
INSERT INTO `examen_referencia` (`id`, `nombre`, `valor_referencia_bajo`, `valor_referencia_superior`) VALUES
	(1, 'Glucosa', 12, 50),
	(2, 'Sacarosa', 1, 3),
	(3, 'HPC', 2, 4);
/*!40000 ALTER TABLE `examen_referencia` ENABLE KEYS */;


-- Volcando estructura para tabla macadb.farmacos
DROP TABLE IF EXISTS `farmacos`;
CREATE TABLE IF NOT EXISTS `farmacos` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `cita_id` int(11) NOT NULL DEFAULT '0',
  `nombre` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `dosis_frecuencia` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `objetivo` varchar(500) COLLATE latin1_spanish_ci DEFAULT NULL,
  `prescripcion` varchar(1) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_farmaco_cita` (`cita_id`),
  CONSTRAINT `FK_farmaco_cita` FOREIGN KEY (`cita_id`) REFERENCES `cita` (`cita_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla macadb.farmacos: ~8 rows (aproximadamente)
/*!40000 ALTER TABLE `farmacos` DISABLE KEYS */;
INSERT INTO `farmacos` (`id`, `cita_id`, `nombre`, `dosis_frecuencia`, `objetivo`, `prescripcion`) VALUES
	(1, 1, 'Complejo B', '1 Semanal', 'Recuperación muscular', NULL),
	(2, 1, 'Proteina', '1 Diaria', 'Ganancia musculaar', NULL),
	(3, 256, 'Certralina', '1 dia', 'Mejorar atencion', NULL),
	(4, 256, 'Sinuptap', '1 semana', 'otros', NULL),
	(5, 261, 'SANTIAGO', 'ZAPATA', 'SANTIAGO ZAPATA', NULL),
	(6, 271, 'traumel', '1', 'homeopatico', '0'),
	(7, 271, 'vitamina c', '3', 'resfriados', '1'),
	(8, 274, 'Complejo B', '3', 'Mejorar atencion', '1'),
	(9, 298, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `farmacos` ENABLE KEYS */;


-- Volcando estructura para tabla macadb.frecuencia
DROP TABLE IF EXISTS `frecuencia`;
CREATE TABLE IF NOT EXISTS `frecuencia` (
  `id` tinyint(4) NOT NULL,
  `frecuencia` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla macadb.frecuencia: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `frecuencia` DISABLE KEYS */;
INSERT INTO `frecuencia` (`id`, `frecuencia`) VALUES
	(1, 'Diario'),
	(2, 'Semanal'),
	(3, 'Mensual'),
	(4, 'Nunca');
/*!40000 ALTER TABLE `frecuencia` ENABLE KEYS */;


-- Volcando estructura para tabla macadb.habitos
DROP TABLE IF EXISTS `habitos`;
CREATE TABLE IF NOT EXISTS `habitos` (
  `cita_id` int(11) NOT NULL,
  `lugar_consumo` varchar(500) COLLATE latin1_spanish_ci DEFAULT NULL COMMENT 'Lugar donde consume los alimentos',
  `compania` int(11) DEFAULT NULL,
  `quien_prepara` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL COMMENT 'Quién prepara los alimentos para ud?',
  `licor` varchar(250) COLLATE latin1_spanish_ci DEFAULT NULL COMMENT 'Consume licor',
  `cigarrillo` varchar(250) COLLATE latin1_spanish_ci DEFAULT NULL COMMENT 'Fuma',
  `actividad_fisica` varchar(500) COLLATE latin1_spanish_ci DEFAULT NULL COMMENT 'Actividad Fisica',
  `deporte` varchar(250) COLLATE latin1_spanish_ci DEFAULT NULL,
  `habito_intestinal` varchar(500) COLLATE latin1_spanish_ci DEFAULT NULL COMMENT 'Habito Intestinal',
  `horas_sueno` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL COMMENT 'Horas de sueño',
  PRIMARY KEY (`cita_id`),
  CONSTRAINT `FK_habitos_cita` FOREIGN KEY (`cita_id`) REFERENCES `cita` (`cita_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla macadb.habitos: ~51 rows (aproximadamente)
/*!40000 ALTER TABLE `habitos` DISABLE KEYS */;
INSERT INTO `habitos` (`cita_id`, `lugar_consumo`, `compania`, `quien_prepara`, `licor`, `cigarrillo`, `actividad_fisica`, `deporte`, `habito_intestinal`, `horas_sueno`) VALUES
	(1, 'Mi casa', NULL, 'esposa', '1', '0', 'Bicicleta', NULL, 'dede', '8'),
	(121, '', NULL, '', '0', '0', '', NULL, '', NULL),
	(122, '', NULL, '', '0', '0', '', NULL, '', NULL),
	(123, '', NULL, '', '0', '0', '', NULL, '', NULL),
	(125, '', NULL, '', '0', '0', '', NULL, '', NULL),
	(127, '', NULL, '', '0', '0', '', NULL, '', NULL),
	(128, '', NULL, '', '0', '0', '', NULL, '', NULL),
	(129, '', NULL, '', '0', '0', '', NULL, '', NULL),
	(130, '', NULL, '', '0', '0', '', NULL, '', NULL),
	(131, '', NULL, '', '0', '0', '', NULL, '', NULL),
	(220, '', NULL, '', '', '', '', NULL, '', NULL),
	(221, '', NULL, '', '', '', '', NULL, '', NULL),
	(222, '', NULL, '', '', '', '', NULL, '', NULL),
	(223, '', NULL, '', '', '', '', NULL, '', NULL),
	(224, '', NULL, '', '', '', '', NULL, '', NULL),
	(225, '', NULL, '', '', '', '', NULL, '', NULL),
	(226, '', NULL, '', '', '', '', NULL, '', NULL),
	(227, '', NULL, '', '', '', '', NULL, '', NULL),
	(228, '', NULL, '', '', '', '', NULL, '', NULL),
	(229, '', NULL, '', '', '', '', NULL, '', NULL),
	(230, '', NULL, '', '', '', '', NULL, '', NULL),
	(241, '', NULL, '', '', '', '', NULL, '', NULL),
	(242, '', NULL, '', '', '', '', NULL, '', NULL),
	(243, '', NULL, '', '', '', '', NULL, '', NULL),
	(244, '', NULL, '', '', '', '', NULL, '', NULL),
	(245, '', NULL, '', '', '', '', NULL, '', NULL),
	(246, '', NULL, '', '', '', '', NULL, '', NULL),
	(247, '', NULL, '', '', '', '', NULL, '', NULL),
	(248, '', NULL, '', '', '', '', NULL, '', NULL),
	(249, '', NULL, '', '', '', '', NULL, '', NULL),
	(250, '', NULL, '', '', '', '', NULL, '', NULL),
	(251, '', NULL, '', '', '', '', NULL, '', NULL),
	(252, '', NULL, '', '', '', '', NULL, '', NULL),
	(253, '', NULL, '', '', '', '', NULL, '', NULL),
	(254, '', NULL, '', '', '', '', NULL, '', NULL),
	(255, '', NULL, '', '', '', '', NULL, '', NULL),
	(256, '', NULL, '', '', '', '', NULL, '', NULL),
	(258, '', NULL, '', '', '', '', NULL, '', NULL),
	(259, '', NULL, '', '', '', '', NULL, '', NULL),
	(260, '', NULL, '', '', '', '', NULL, '', NULL),
	(261, '', NULL, '', '', '', '', NULL, '', NULL),
	(267, '', NULL, '', '', '', '', NULL, '', ''),
	(271, 'Casa, trabajo', 1, 'esposo', 'Si, socialmente', 'no', 'si, gimnasio 3 veces x semana', 'Gimnasio', 'regular', '7'),
	(272, '', NULL, '', '', '', '', NULL, '', ''),
	(273, '', NULL, '', '', '', '', NULL, '', ''),
	(274, '', NULL, '', '', '', '', NULL, '', ''),
	(275, '', NULL, '', '', '', '', NULL, '', ''),
	(276, '', NULL, '', '', '', '', NULL, '', ''),
	(277, '', NULL, '', '', '', '', 'Tennis', '', ''),
	(297, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(298, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `habitos` ENABLE KEYS */;


-- Volcando estructura para tabla macadb.ingesta_anamnesis
DROP TABLE IF EXISTS `ingesta_anamnesis`;
CREATE TABLE IF NOT EXISTS `ingesta_anamnesis` (
  `anamnesis_id` int(11) NOT NULL,
  `tipo_ingesta_id` int(11) NOT NULL,
  `texto_descriptivo` varchar(250) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`anamnesis_id`,`tipo_ingesta_id`),
  KEY `FK_tipo_ingesta` (`tipo_ingesta_id`),
  CONSTRAINT `FK_anamnesis` FOREIGN KEY (`anamnesis_id`) REFERENCES `anamnesis` (`anamnesis_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_tipo_ingesta` FOREIGN KEY (`tipo_ingesta_id`) REFERENCES `tipo_cambio_ingesta` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla macadb.ingesta_anamnesis: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `ingesta_anamnesis` DISABLE KEYS */;
INSERT INTO `ingesta_anamnesis` (`anamnesis_id`, `tipo_ingesta_id`, `texto_descriptivo`) VALUES
	(1, 2, 'Normal'),
	(56, 1, 'mastica'),
	(56, 2, 'degluye');
/*!40000 ALTER TABLE `ingesta_anamnesis` ENABLE KEYS */;


-- Volcando estructura para tabla macadb.interpretacion_circunferencia
DROP TABLE IF EXISTS `interpretacion_circunferencia`;
CREATE TABLE IF NOT EXISTS `interpretacion_circunferencia` (
  `cita_id` int(11) NOT NULL,
  `circunferencia_id` tinyint(4) NOT NULL,
  `valor` float DEFAULT NULL,
  `interpretacion` varchar(500) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`cita_id`,`circunferencia_id`),
  KEY `FK_interpretacion_circ` (`circunferencia_id`),
  CONSTRAINT `FK_cita_intepretacion` FOREIGN KEY (`cita_id`) REFERENCES `cita` (`cita_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_interpretacion_circ` FOREIGN KEY (`circunferencia_id`) REFERENCES `circunferencias` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla macadb.interpretacion_circunferencia: ~45 rows (aproximadamente)
/*!40000 ALTER TABLE `interpretacion_circunferencia` DISABLE KEYS */;
INSERT INTO `interpretacion_circunferencia` (`cita_id`, `circunferencia_id`, `valor`, `interpretacion`) VALUES
	(1, 1, 30, 'Ninguna'),
	(229, 1, 2, 'es la relacion - cadera'),
	(229, 2, 2, 'texto estructura'),
	(229, 3, 23, 'text c pantorrilla'),
	(229, 4, 24, 'text brazo'),
	(229, 5, 25, 'text musc'),
	(230, 1, NULL, ''),
	(230, 2, NULL, ''),
	(230, 3, NULL, ''),
	(230, 4, NULL, ''),
	(230, 5, NULL, ''),
	(239, 1, NULL, ''),
	(239, 2, NULL, ''),
	(239, 3, NULL, ''),
	(239, 4, NULL, ''),
	(239, 5, NULL, ''),
	(240, 1, NULL, ''),
	(240, 2, NULL, ''),
	(240, 3, NULL, ''),
	(240, 4, NULL, ''),
	(240, 5, NULL, ''),
	(241, 1, NULL, ''),
	(241, 2, NULL, ''),
	(241, 3, NULL, ''),
	(241, 4, NULL, ''),
	(241, 5, NULL, ''),
	(242, 1, NULL, ''),
	(242, 2, NULL, ''),
	(242, 3, NULL, ''),
	(242, 4, NULL, ''),
	(242, 5, NULL, ''),
	(243, 1, NULL, ''),
	(243, 2, NULL, ''),
	(243, 3, NULL, ''),
	(243, 4, NULL, ''),
	(243, 5, NULL, ''),
	(244, 1, NULL, ''),
	(244, 2, NULL, ''),
	(244, 3, NULL, ''),
	(244, 4, NULL, ''),
	(244, 5, NULL, ''),
	(245, 1, NULL, ''),
	(245, 2, NULL, ''),
	(245, 3, NULL, ''),
	(245, 4, NULL, ''),
	(245, 5, NULL, ''),
	(267, 1, 0.75, ''),
	(267, 2, 11.75, 'Grande'),
	(271, 1, 0.71, ''),
	(271, 2, 1.82, 'Grande'),
	(271, 3, 20, 'Medio'),
	(271, 4, 18, 'Medio');
/*!40000 ALTER TABLE `interpretacion_circunferencia` ENABLE KEYS */;


-- Volcando estructura para tabla macadb.menu
DROP TABLE IF EXISTS `menu`;
CREATE TABLE IF NOT EXISTS `menu` (
  `alimento_id` tinyint(4) NOT NULL,
  `cita_id` int(11) NOT NULL,
  `frecuencia_id` tinyint(4) DEFAULT NULL,
  `valor` tinyint(4) DEFAULT NULL,
  `observacion` varchar(250) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`alimento_id`,`cita_id`),
  KEY `FK_menu_cita` (`cita_id`),
  KEY `FK_menu_frecuencia` (`frecuencia_id`),
  CONSTRAINT `FK_menu_alimento` FOREIGN KEY (`alimento_id`) REFERENCES `alimento` (`alimento_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_menu_cita` FOREIGN KEY (`cita_id`) REFERENCES `cita` (`cita_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_menu_frecuencia` FOREIGN KEY (`frecuencia_id`) REFERENCES `frecuencia` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla macadb.menu: ~8 rows (aproximadamente)
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` (`alimento_id`, `cita_id`, `frecuencia_id`, `valor`, `observacion`) VALUES
	(1, 273, 1, 1, NULL),
	(1, 274, 1, 1, 'dddd'),
	(2, 273, 2, 2, NULL),
	(2, 274, 3, 2, 'otra'),
	(3, 1, 1, 1, 'una rebanada'),
	(3, 273, 2, 3, NULL),
	(3, 274, 2, 3, 'trwa'),
	(4, 1, 2, 4, NULL),
	(4, 273, 1, 4, NULL),
	(4, 274, 3, 4, 'cuatro');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;


-- Volcando estructura para tabla macadb.perfil
DROP TABLE IF EXISTS `perfil`;
CREATE TABLE IF NOT EXISTS `perfil` (
  `id` tinyint(4) NOT NULL,
  `nombre` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla macadb.perfil: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `perfil` DISABLE KEYS */;
INSERT INTO `perfil` (`id`, `nombre`) VALUES
	(1, 'Administrador'),
	(2, 'Medico'),
	(3, 'Reporter');
/*!40000 ALTER TABLE `perfil` ENABLE KEYS */;


-- Volcando estructura para tabla macadb.perfil_usuario
DROP TABLE IF EXISTS `perfil_usuario`;
CREATE TABLE IF NOT EXISTS `perfil_usuario` (
  `perfil_id` tinyint(4) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  PRIMARY KEY (`perfil_id`,`usuario_id`),
  KEY `FK_perfil_usuario_usuario` (`usuario_id`),
  CONSTRAINT `FK_perfil_usuario_perfil` FOREIGN KEY (`perfil_id`) REFERENCES `perfil` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_perfil_usuario_usuario` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`persona_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla macadb.perfil_usuario: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `perfil_usuario` DISABLE KEYS */;
INSERT INTO `perfil_usuario` (`perfil_id`, `usuario_id`) VALUES
	(2, 1),
	(3, 1),
	(2, 7);
/*!40000 ALTER TABLE `perfil_usuario` ENABLE KEYS */;


-- Volcando estructura para tabla macadb.persona
DROP TABLE IF EXISTS `persona`;
CREATE TABLE IF NOT EXISTS `persona` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `apellido` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `identificacion` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `genero` varchar(1) COLLATE latin1_spanish_ci DEFAULT NULL,
  `ocupacion` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `telefono` varchar(15) COLLATE latin1_spanish_ci DEFAULT NULL,
  `email` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `historia_clinica` varchar(20) COLLATE latin1_spanish_ci DEFAULT NULL,
  `tipo_identificacion_id` tinyint(4) DEFAULT NULL,
  `tipo_persona_id` tinyint(4) DEFAULT NULL,
  `estado_civil_id` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_persona_tipoid` (`tipo_identificacion_id`),
  KEY `FK_persona_tipopersona` (`tipo_persona_id`),
  KEY `FK_persona_estado_civil` (`estado_civil_id`),
  CONSTRAINT `FK_persona_estado_civil` FOREIGN KEY (`estado_civil_id`) REFERENCES `estado_civil` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_persona_tipoid` FOREIGN KEY (`tipo_identificacion_id`) REFERENCES `tipo_identificacion` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_persona_tipopersona` FOREIGN KEY (`tipo_persona_id`) REFERENCES `tipo_persona` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla macadb.persona: ~15 rows (aproximadamente)
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
INSERT INTO `persona` (`id`, `nombre`, `apellido`, `identificacion`, `fecha_nacimiento`, `genero`, `ocupacion`, `telefono`, `email`, `historia_clinica`, `tipo_identificacion_id`, `tipo_persona_id`, `estado_civil_id`) VALUES
	(1, 'Santiago', 'Zapata', '16078838', '1984-02-24', 'M', 'Ingeniero de sistemas', '3012', 'sanzco@gmail.com', 'CC16078838', 1, 3, 2),
	(2, 'Juan ', 'Gomez', '20039', '2017-12-08', NULL, NULL, '32312', 'sanzco@gmail.com', NULL, 1, 1, 1),
	(3, 'dadasd', 'eqweqeq', '313', '2017-12-15', NULL, NULL, '31313', 's2@m.com', NULL, 1, 1, 1),
	(4, 'qqqq', 'qqqq', '12232', '2017-12-14', NULL, NULL, '21212', 'sanzco@gmail.com', NULL, 1, 2, 1),
	(5, 'wq', 'wq', '21212', '2017-12-14', NULL, NULL, '2121', '2121@kmo.com', NULL, 1, 2, 1),
	(6, 'Andres', 'Gomez', '9090', '2017-12-13', NULL, '', '', 'sa@mxo.com', NULL, 1, 2, 1),
	(7, 'Camilo', 'Bravo', '12420', '2017-12-16', NULL, NULL, '2121', 'po@ko.com', NULL, 1, 2, 1),
	(8, 'Otros', 'nombre', '9090', '2017-12-08', NULL, 'Medico', '1', 'jo@j.com', NULL, 1, 1, 1),
	(9, 'Mateo', 'Gomez', '123', '1947-12-09', 'M', 'Piloto', '', '', 'CC123', 1, 1, 1),
	(10, 'Camilo', 'Medina', '123224', '2018-01-12', 'M', 'Geologo', '313112', 'lope@mc.com', 'CC123224', 1, 1, 2),
	(11, 'Andrea', 'Gomez', '321123', '2012-11-07', 'F', 'Maestra', '323131', 'anjs@oio.com', 'CC321123', 1, 1, 2),
	(12, 'Andrea', 'Salas', '63529193', '1982-06-09', 'F', 'Ingeniera', '39092903', 'ajo@ijo.com', 'CC63529193', 1, 1, 2),
	(13, 'Juaquin', 'Gomez', '1212', '1941-02-14', 'M', '', '', '', 'CC1212', 1, 1, 3),
	(14, 'Juaquin', 'Gomez', '1212', '1941-02-14', 'M', '', '', '', 'CC1212', 1, 1, 3),
	(15, 'Juaquin', 'Gomez', '1212', '1941-02-14', 'M', '', '', '', 'CC1212', 1, 1, 3);
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;


-- Volcando estructura para tabla macadb.recordatorio
DROP TABLE IF EXISTS `recordatorio`;
CREATE TABLE IF NOT EXISTS `recordatorio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cita_id` int(11) DEFAULT NULL,
  `hora` varchar(5) COLLATE latin1_spanish_ci DEFAULT NULL,
  `tiempo_id` tinyint(4) DEFAULT NULL,
  `preparacion` varchar(500) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_recordatorio_cita` (`cita_id`),
  KEY `FK_tiempo` (`tiempo_id`),
  CONSTRAINT `FK_recordatorio_cita` FOREIGN KEY (`cita_id`) REFERENCES `cita` (`cita_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_tiempo` FOREIGN KEY (`tiempo_id`) REFERENCES `tiempo_comida` (`tiempo_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla macadb.recordatorio: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `recordatorio` DISABLE KEYS */;
INSERT INTO `recordatorio` (`id`, `cita_id`, `hora`, `tiempo_id`, `preparacion`) VALUES
	(1, 1, '20:40', 2, 'Al Vapor'),
	(2, 254, '00:00', 1, 'omelete'),
	(3, 254, '00:00', 1, 'omelete'),
	(4, 255, NULL, 1, ''),
	(5, 271, '00:00', 1, 'cereal'),
	(6, 298, NULL, NULL, NULL);
/*!40000 ALTER TABLE `recordatorio` ENABLE KEYS */;


-- Volcando estructura para tabla macadb.recordatorio_preparacion
DROP TABLE IF EXISTS `recordatorio_preparacion`;
CREATE TABLE IF NOT EXISTS `recordatorio_preparacion` (
  `recordatorio_id` int(11) DEFAULT NULL,
  `alimento` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `cantidad` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  KEY `FK_recordatorio` (`recordatorio_id`),
  CONSTRAINT `FK_recordatorio` FOREIGN KEY (`recordatorio_id`) REFERENCES `recordatorio` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla macadb.recordatorio_preparacion: ~9 rows (aproximadamente)
/*!40000 ALTER TABLE `recordatorio_preparacion` DISABLE KEYS */;
INSERT INTO `recordatorio_preparacion` (`recordatorio_id`, `alimento`, `cantidad`) VALUES
	(1, 'huevos', '1'),
	(1, 'queso', '1'),
	(2, 'huevos', '1'),
	(2, 'queso', '1 rebanada'),
	(3, 'huevos', '1'),
	(3, 'queso', '1 rebanada'),
	(4, '', ''),
	(5, 'fresa', '3 unidad'),
	(5, 'leche', '1 taza'),
	(5, 'cereal', '4 cucharadas');
/*!40000 ALTER TABLE `recordatorio_preparacion` ENABLE KEYS */;


-- Volcando estructura para tabla macadb.tiempo_comida
DROP TABLE IF EXISTS `tiempo_comida`;
CREATE TABLE IF NOT EXISTS `tiempo_comida` (
  `tiempo_id` tinyint(4) NOT NULL,
  `nombre` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`tiempo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla macadb.tiempo_comida: ~9 rows (aproximadamente)
/*!40000 ALTER TABLE `tiempo_comida` DISABLE KEYS */;
INSERT INTO `tiempo_comida` (`tiempo_id`, `nombre`) VALUES
	(1, 'Desayuno'),
	(2, 'Almuerzo'),
	(3, 'Media Mañana'),
	(4, 'Media Tarde'),
	(5, 'Cena'),
	(6, 'Pre Entreno'),
	(7, 'Post Entreno'),
	(8, 'Durante Entrenamiento'),
	(9, 'Merienda');
/*!40000 ALTER TABLE `tiempo_comida` ENABLE KEYS */;


-- Volcando estructura para tabla macadb.tipo_antecedente
DROP TABLE IF EXISTS `tipo_antecedente`;
CREATE TABLE IF NOT EXISTS `tipo_antecedente` (
  `id` tinyint(4) NOT NULL,
  `nombre` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla macadb.tipo_antecedente: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `tipo_antecedente` DISABLE KEYS */;
INSERT INTO `tipo_antecedente` (`id`, `nombre`) VALUES
	(1, 'Antecedentes Patológicos'),
	(2, 'Antecedentes Quirúrgicos'),
	(3, 'Antecedentes Familiares');
/*!40000 ALTER TABLE `tipo_antecedente` ENABLE KEYS */;


-- Volcando estructura para tabla macadb.tipo_cambio_ingesta
DROP TABLE IF EXISTS `tipo_cambio_ingesta`;
CREATE TABLE IF NOT EXISTS `tipo_cambio_ingesta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla macadb.tipo_cambio_ingesta: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `tipo_cambio_ingesta` DISABLE KEYS */;
INSERT INTO `tipo_cambio_ingesta` (`id`, `nombre`) VALUES
	(1, 'Masticación'),
	(2, 'Deglución');
/*!40000 ALTER TABLE `tipo_cambio_ingesta` ENABLE KEYS */;


-- Volcando estructura para tabla macadb.tipo_cita
DROP TABLE IF EXISTS `tipo_cita`;
CREATE TABLE IF NOT EXISTS `tipo_cita` (
  `id` tinyint(4) NOT NULL,
  `nombre` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla macadb.tipo_cita: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `tipo_cita` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_cita` ENABLE KEYS */;


-- Volcando estructura para tabla macadb.tipo_comida
DROP TABLE IF EXISTS `tipo_comida`;
CREATE TABLE IF NOT EXISTS `tipo_comida` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE latin1_spanish_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla macadb.tipo_comida: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `tipo_comida` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_comida` ENABLE KEYS */;


-- Volcando estructura para tabla macadb.tipo_examen
DROP TABLE IF EXISTS `tipo_examen`;
CREATE TABLE IF NOT EXISTS `tipo_examen` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `nombre_tipo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla macadb.tipo_examen: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `tipo_examen` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_examen` ENABLE KEYS */;


-- Volcando estructura para tabla macadb.tipo_identificacion
DROP TABLE IF EXISTS `tipo_identificacion`;
CREATE TABLE IF NOT EXISTS `tipo_identificacion` (
  `id` tinyint(4) NOT NULL,
  `nombre` varchar(20) COLLATE latin1_spanish_ci DEFAULT NULL,
  `prefijo` char(2) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla macadb.tipo_identificacion: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `tipo_identificacion` DISABLE KEYS */;
INSERT INTO `tipo_identificacion` (`id`, `nombre`, `prefijo`) VALUES
	(1, 'Cedula Ciudadania', 'CC'),
	(2, 'Cedula Extranjeria', 'CE'),
	(3, 'Tarjeta de Identidad', 'TI'),
	(4, 'Registro Civil', 'RC');
/*!40000 ALTER TABLE `tipo_identificacion` ENABLE KEYS */;


-- Volcando estructura para tabla macadb.tipo_persona
DROP TABLE IF EXISTS `tipo_persona`;
CREATE TABLE IF NOT EXISTS `tipo_persona` (
  `id` tinyint(4) NOT NULL,
  `nombre` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla macadb.tipo_persona: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `tipo_persona` DISABLE KEYS */;
INSERT INTO `tipo_persona` (`id`, `nombre`) VALUES
	(1, 'Paciente'),
	(2, 'Medico'),
	(3, 'Tecnico');
/*!40000 ALTER TABLE `tipo_persona` ENABLE KEYS */;


-- Volcando estructura para tabla macadb.usuario
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
  `persona_id` int(11) NOT NULL,
  `username` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `password` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estado` char(1) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`persona_id`),
  UNIQUE KEY `username` (`username`),
  CONSTRAINT `FK_usuario_persona` FOREIGN KEY (`persona_id`) REFERENCES `persona` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla macadb.usuario: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` (`persona_id`, `username`, `password`, `estado`) VALUES
	(1, 'szapata', '$2y$13$YeUTwk/9gtuysY55KngPF.7e.dFU.ZbsfFVmkdGcpjlDzRuPai9A6', 'A'),
	(2, 'xsaza', '$2y$13$w2/Yl6RzyFDUw2.JN8lO8eIgdEXwXvcirmPN0Egc2.lQSkSyjxhdy', 'A'),
	(3, 'a1', '', 'A'),
	(4, 'wqw', '321321321', 'A'),
	(6, 'aa', '$2y$13$gOEy3/AQV/b1mil2dD7D1u3arCohqMmRoxS3wk..Rz7vKIg4F2mOy', 'A'),
	(7, 'camib', '$2y$13$le/gP4T5gpYvVOE9Zl6dOOYGtHFfZtf83zcDna7l/4aCXNXZAWmIm', 'A');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;


-- Volcando estructura para tabla macadb.volumen_consumido
DROP TABLE IF EXISTS `volumen_consumido`;
CREATE TABLE IF NOT EXISTS `volumen_consumido` (
  `id` tinyint(4) NOT NULL,
  `nombre` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla macadb.volumen_consumido: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `volumen_consumido` DISABLE KEYS */;
INSERT INTO `volumen_consumido` (`id`, `nombre`) VALUES
	(1, 'Muy Escaso'),
	(2, 'Escaso'),
	(3, 'Normal'),
	(4, 'Abundante'),
	(5, 'Muy Abundante');
/*!40000 ALTER TABLE `volumen_consumido` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
