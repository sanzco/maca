<?php /* @var $this Controller */ ?>
<?php $this->beginContent('@app/views/layouts/leftmain.php'); 
use kartik\sidenav\SideNav;
use yii\helpers\Url;
?>

<div class="span-5 last">
	<div id="sidebar" class="left_bar">
	<?php
		echo SideNav::widget([
	'type' => SideNav::TYPE_INFO,
	'headingOptions' => ['style'=>'background-color: #6ccad5;color:#fff'],
	'heading' => 'Administración',
	'items' => [
		[
			'url' => Url::to(['/usuario/index']),
			'label' => 'Usuarios',
			'icon' => 'user'
		],
		[
			'label' => 'Alimentos',
			'icon' => 'shopping-cart',
			'url' =>Url::to(['/alimento/index'])
		],
		[
			'label' => 'Examenes',
			'icon' => 'file',
			'url' =>Url::to(['/examen/index'])
		],
		
		
	],
]);
	?>
	</div><!-- sidebar -->
</div>

<div class="span-19 right_content" >
	<div id="content">
		<?php echo $content; ?>
	</div><!-- content -->
</div>
<?php $this->endContent(); ?>