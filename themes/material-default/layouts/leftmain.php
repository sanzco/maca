<?php

use yii\helpers\Html;
use yii\widgets\Menu;
use yii\widgets\Breadcrumbs;
use app\models\Usuario;

/**
 * @var $this \yii\base\View
 * @var $content string
 */
// $this->registerAssetBundle('app');
?>
<?php $this->beginPage(); ?>

<!DOCTYPE html>
<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  
  <title><?php echo Html::encode($this->title); ?></title>
  <?php $this->head(); ?>
	
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
  
  <!-- CSS  -->
  <link href="<?php echo $this->theme->baseUrl ?>/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="<?php echo $this->theme->baseUrl ?>/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
  <?php $this->beginBody() ?>
  <nav class="light-blue lighten-1" role="navigation">
    <div class="container">
      <div class="nav-wrapper"><a id="logo-container" href="#" class="brand-logo"><img alt="Logo Maca" style="width: 125px; height: 60px;padding-top: 2px" src="<?php echo $this->theme->baseUrl ?>/images/maca_letra_tx.png"> </a>
	  		<?php
						$user = Usuario::findOne(Yii::$app->user->id);
	  			$isAdmin = false;
	  			if(!empty($user)){
	  				foreach($user->perfiles as $perfil ){
	  					if($perfil->id == '1'){
	  						$isAdmin = true;
	  						break;
	  					}
	  				}
	  			}	
	  		
	  				echo Menu::widget([
						    'options' => ['id' => "nav-mobile", 'class' => 'right side-nav'],
						    'items' => [
						        ['label' => 'Historia', 'url' => ['site/index']],
						        ['label' => 'Recomendaciones', 'url' => ['cita/recomendaciones']],
						        ['label' => 'Admin', 'url' => ['admin/index'], 'visible' => !Yii::$app->user->isGuest],
						        ['label' => 'Logout', 'url'=>['site/logout'], 'visible' => !Yii::$app->user->isGuest],
						    ],
						]);
					?>
          <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
      </div>
    </div>
  </nav>
  

  <div class="container">
    <div class="section">
          <?php echo $content; ?>
      </div>
  </div>

  <div class="container">
    <div class="section">

      <!--   Icon Section   -->
      
      <center><img alt="Logo Maca" style="filter:alpha(opacity=40);opacity:.40;" src="<?php echo $this->theme->baseUrl ?>/images/logo_maca.png">
      </center>
       <!--   <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center light-blue-text"><i class="mdi-image-flash-on"></i></h2> 
            <h5 class="center">Speeds up development</h5>

            <p class="light">We did most of the heavy lifting for you to provide a default stylings that incorporate our custom components. Additionally, we refined animations and transitions to provide a smoother experience for developers.</p>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center light-blue-text"><i class="mdi-social-group"></i></h2>
            <h5 class="center">User Experience Focused</h5>

            <p class="light">By utilizing elements and principles of Material Design, we were able to create a framework that incorporates components and animations that provide more feedback to users. Additionally, a single underlying responsive system across all platforms allow for a more unified user experience.</p>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center light-blue-text"><i class="mdi-action-settings"></i></h2>
            <h5 class="center">Easy to work with</h5>

            <p class="light">We have provided detailed documentation as well as specific code examples to help new users get started. We are also always open to feedback and can answer any questions a user may have about Materialize.</p>
          </div>
        </div>
        -->
      
    </div>
    <br><br>

    <div class="section">

    </div>
  </div>

  <footer class="orange">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">MACA Centro Integral de Nutrición</h5>
          <p class="grey-text lighten-4">
	Somos el primer Centro Integral de Nutrición en el Eje Cafetero, el cual contribuye a la promoción de la salud, 
	prevención y tratamiento de enfermedades, brindando acompañamiento y abordaje profesional por Nutricionistas, Psicólogos y Gastrónomos.
</p>
        </div>
        
        
        <div class="col l6 s12" style="padding-left: 80px">
          <h5 class="white-text">Servicios</h5>
          <ul>
            <li><a class="white-text" href="#!">Nutrición Deportiva</a></li>
            <li><a class="white-text" href="#!">Trastornos de la Conducta Alimentaria</a></li>
            <li><a class="white-text" href="#!">Consulta Nutricional Especializada</a></li>
          </ul>
        </div>
        
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      By Santiago Zapata
      </div>
    </div>
  </footer>  


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="<?php echo $this->theme->baseUrl ?>/js/materialize.js"></script>
  <script src="<?php echo $this->theme->baseUrl ?>/js/init.js"></script>
  
  <?php $this->endBody() ?>
  </body>
</html>
<?php $this->endPage(); ?>